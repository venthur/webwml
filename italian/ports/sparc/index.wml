#use wml::debian::template title="Port su SPARC" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="f9d5abd797e762089776545824869e3e44bd2c42" maintainer="Luca Monducci"

<h1>Debian SPARC</h1>

<ul>
    <li><a href="#intro">Introduzione</a></li>
    <li><a href="#status">Stato attuale</a></li>
    <li><a href="#sparc64bit">Nota sul supporto SPARC 64-bit</a>
    <ul>
	<li><a href="#kernelsun4u">Compilare il kernel per sun4u</a></li>
    </ul></li>
    <li><a href="#errata">Problemi</a></li>
    <li><a href="#who">Chi siamo? Come posso partecipare?</a></li>
    <li><a href="#links">Dove si possono trovare altre informazioni?</a></li>
</ul>


<h2 id="intro">Introduzione</h2>

<p>Con queste pagine si vuole aiutare gli utenti e gli sviluppatori Debian
a usare Debian GNU/Linux sull'architettura SPARC. In queste pagine è
possibile trovare informazioni sullo stato attuale, sui problemi noti,
delle informazioni sulle persone che si occupano del port Debian e dei
riferimenti a ulteriori informazioni.</p>


<h2 id="status">Stato attuale</h2>

<p>
Il supporto per le macchine precedenti le UltraSPARC è stato abbandonato
insieme al termine del servizio di Debian Etch (consultare
<a href="https://wiki.debian.org/Sparc32">https://wiki.debian.org/Sparc32</a>).
Dopo ciò, il port 32-bit aveva bisogno di una CPU UltraSPARC ed eseguiva un
kernel a 64-bit.
</p>

<p>
Questo port a 32-bit è stato abbandonato insieme al termine del servizio di
Debian Wheezy.
</p>

<p>
Al momento non esiste un port ufficiale Debian per SPARC, invece esiste
un port per SPARC completamente a 64-bit e questo port è pienamente
supportato dal team Debian Ports.
</p>


<h2 id="sparc64bit">Nota sul supporto SPARC 64 bit</h2>

<p>Il port Debian SPARC, come accennato in precedenza, supporta le
architetture sun4u (<q>Ultra</q>) e sun4v (CPU Niagara). Usa un kernel
a 64 bit (compilato con gcc 3.3 o una versione successiva), ma molte
delle applicazioni sono eseguite a 32 bit; questo metodo è chiamato
<q>spazio utente a 32-bit</q>.</p>

<p>La realizzazione del port Debian SPARC 64 (conosciuto anche come
<q>UltraLinux</q>) non è attualmente ideato come un'opera di port
completo come gli altri, più propriamente ha l'intenzione di essere una
<em>aggiunta</em> al port su SPARC.</p>

<p>In pratica non c'è nessun interesse nell'aver tutte le applicazioni
eseguite in modalità a 64 bit. L'uso della modalità a 64 bit per tutto
implica un notevole sovraccarico (sia di memoria che di disco) e spesso
senza apportare vantaggi. Solo alcune applicazioni possono veramente trarre
dei benefici dalla modalità a 64 bit e su queste si concentra il lavoro di
port.</p>

<h3 id="kernelsun4u">Compilazione del kernel per sun4u</h3>

<p>Per compilare un kernel Linux per Sun4u, è necessario usare i sorgenti
del kernel 2.2 o una versione successiva.</p>

<p>È fortemente raccomandato usare il pacchetto <tt>kernel-package</tt>
in modo da semplificare l'installazione e la gestione dei kernel, si può
compilare un kernel già configurato con un unico comando (da root):</p>

<pre>
  make-kpkg --subarch=sun4u --arch_in_name --revision=custom.1 kernel_image
</pre>


<h2 id="errata">Problemi</h2>

<p>Alcuni dei problemi noti con istruzioni su come correggerli o aggirarli
possono essere trovati nella pagina dei <a href="problems">Problemi</a>.</p>


<h2 id="who">Chi siamo? Come si può contribuire?</h2>

<p>Il port Debian SPARC è un lavoro distribuito, proprio come lo è Debian.
Parecchie persone hanno aiutato la realizzazione sia del port che della
documentazione, è disponibile un breve elenco nei <a href="credits">\
ringraziamenti</a>.</p>

<p>Se volete contribuire iscrivetevi, nel modo <a href="#links">descritto
sotto</a>, alla lista di messaggi &lt;debian-sparc@lists.debian.org&gt;.</p>

<p>Gli sviluppatori ufficiali che vogliono fare il port dei loro pacchetti
per poi farne l'upload dovrebbero leggere le linee guida per il port nella
<a href="$(DOC)/developers-reference/">Developers Reference</a> e vedere la
<a href="porting">pagina del port SPARC</a>.</p>


<h2 id="links">Dove si possono trovare altre informazioni?</h2>

<p>
Una pagina di Debian Wiki è dedicata al
<a href="https://wiki.debian.org/Sparc64">port Debian Sparc64</a>.
</p>


<p>Il miglior posto dove fare domande specifiche sul port Debian su SPARC
è la lista di messaggi <a href="https://lists.debian.org/debian-sparc/">\
&lt;debian-sparc@lists.debian.org&gt;</a>. Gli
<a href="https://lists.debian.org/debian-sparc/">archivi</a> della lista di
messaggi sono consultabili via web.</p>

<p>Per iscriversi alla lista di messaggi inviare un messaggio a
<a href="mailto:debian-sparc-request@lists.debian.org">\
debian-sparc-request@lists.debian.org</a> con la parola
&ldquo;subscribe&rdquo; nell'oggetto e non nel corpo del messaggio.
In alternativa ci si può iscrivere via web utilizzando la
<a href="https://lists.debian.org/debian-sparc/">pagina di iscrizione
alla lista di messaggi</a>.</p>

<p>Domande relative al kernel dovrebbero essere rivolte sulla lista
&lt;sparclinux@vger.rutgers.edu&gt;, per iscriversi inviare un messaggio
con <q>subscribe sparclinux</q> nel corpo del messaggio a
<a href="mailto:majordomo@vger.rutgers.edu">majordomo@vger.rutgers.edu</a>.
Ovviamente c'è anche una lista per Red Hat.</p>
