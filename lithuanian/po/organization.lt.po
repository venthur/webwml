msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2009-02-22 15:31+0300\n"
"Last-Translator: Aleksandr Charkov <alcharkov@gmail.com>\n"
"Language-Team: unknown\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "dabartinis"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "narys"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "vedėjas"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:41
#, fuzzy
msgid "Stable Release Manager"
msgstr "Išleidimo Valdytojai"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "vedlys"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "pirmininkas"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asistentas"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "sekretorius"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Pareigūnai"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribucija"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
#, fuzzy
msgid "Publicity team"
msgstr "Viešieji_ryšiai"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Palaikymas ir Infrastruktūra"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Lyderis"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Techninis Komitetas"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Sekretorius"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Plėtojimo Projektai"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP Archyvai"

#: ../../english/intro/organization.data:114
#, fuzzy
msgid "FTP Masters"
msgstr "FTP Meistras"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP Asistentai"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Vadovavimas Išleidimui"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Išleidimo komanda"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Kokybės Užtikrinimas"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Įdiegimo sistemos komanda"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Išleidimo pastabos"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "CD atvaizdai"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produkcija"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testuojama"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
#, fuzzy
msgid "Autobuilding infrastructure"
msgstr "Palaikymas ir Infrastruktūra"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:176
#, fuzzy
msgid "Buildd administration"
msgstr "Buildd Administracija"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentacija"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Reikalaujantis darbo ir Laukiamas paketų sąrašas"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Ryšiai su žiniasklaida"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Tinkalalapiai "

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Debian Planeta"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Moterys Debian projekte"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Įvykiai"

#: ../../english/intro/organization.data:264
#, fuzzy
msgid "DebConf Committee"
msgstr "Techninis Komitetas"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Partnerio Programa"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Kompiuterinės įrangos aukojimo koordinatorius"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Klaidų stebėjimo sistema"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Pašto konferencijų administracija ir Pašto konferencijų archyvai"

#: ../../english/intro/organization.data:329
#, fuzzy
msgid "New Members Front Desk"
msgstr "Naujo plėtojo registracinis stalas"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Debian sąskaitos valdytojai"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Raktinės valdytojai (PGP ir GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Saugumo komanda"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Politika"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Sistemos administracija"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Tai yra adresas, kurį naudoti jei iškilus problemoms su Debian mašinoms, "
"įskaitant slaptažodžio problemas arba jie jums reikia įdiegto paketo."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Jeigu jūs turit problemų su kompiuterine įranga naudojant Debian, prašom "
"žiūrėti <a href=\"https://db.debian.org/machines.cgi\"> Debian Mašinos</a> "
"puslapį, jame turi būti mašinos administratoriaus informacija."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "LDAP plėtotojų direktorijos administratorius"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Veidrodžiai"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "DNS prižiūrėtojas"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Paketų stebėjimo sistema"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Alioth administratoriai"

#~ msgid "Individual Packages"
#~ msgstr "Individualūs paketai"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian vaikams nuo 1 iki 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian medicinos praktikai ir jų tyrimams"

#~ msgid "Debian for education"
#~ msgstr "Debian švietimui"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian teisėje"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian neįgaliems žmonėms"

#, fuzzy
#~| msgid "Debian for medical practice and research"
#~ msgid "Debian for science and related research"
#~ msgstr "Debian medicinos praktikai ir jų tyrimams"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian švietimui"

#~ msgid "Live System Team"
#~ msgstr "Live sistemos komanda"

#~ msgid "Auditor"
#~ msgstr "Revizorius"

#~ msgid "Publicity"
#~ msgstr "Viešieji_ryšiai"

#, fuzzy
#~| msgid "Debian Maintainer Keyring Team"
#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Debian plėtotųjų raktinės komanda"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Papildomos Debian distribucijos"

#~| msgid "Release Manager for ``stable''"
#~ msgid "Release Team for ``stable''"
#~ msgstr "``stable'' Išleidimo komanda"

#~ msgid "Vendors"
#~ msgstr "Pardavėjai"

#~ msgid "Handhelds"
#~ msgstr "Kišeniniai kompiuteriai"

#~| msgid "Security Testing Team"
#~ msgid "Marketing Team"
#~ msgstr "Prekybos Komanda"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Administratoriai, atsakingi už tam tikra architektūrą gali būti "
#~ "pasiekiami <genericemail arch@buildd.debian.org>, pavyzdžiui "
#~ "<genericemail i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Individualių buildd administratorių vardus galite rasti taip pat čia href="
#~ "\"http://www.buildd.net\">http://www.buildd.net</a>. Pasirinkite "
#~ "architektūrą ir distributyvą kad pažiūrėtumėte galimus buildd ir jų "
#~ "administratorius."

#~ msgid "Key Signing Coordination"
#~ msgstr "Rakto pasirašymo koordinatorius"

#~ msgid "Accountant"
#~ msgstr "Sąskaitininkas"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Universali operacinė sistema jūsų darbastalyje"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian pelno nesiekiančioms organizacijoms"

#~ msgid ""
#~ "This is not yet an official Debian internal project but it has announced "
#~ "the intention to be integrated."
#~ msgstr ""
#~ "Tai dar ne oficialus vidinis Debian projektas, bet jis paskelbė siekti "
#~ "integraciją."

#~ msgid "Debian Multimedia Distribution"
#~ msgstr "Debian Multimedijos Distributyvas"

#~ msgid "Debian GNU/Linux for Enterprise Computing"
#~ msgstr "Debian GNU/Linux Verslui"

#~ msgid "Mailing List Archives"
#~ msgstr "Pašto konferencijos archyvai"

#~ msgid "APT Team"
#~ msgstr "APT komanda"

#~ msgid "Release Assistants"
#~ msgstr "Išleidimo Asistentai"

#~ msgid "Security Audit Project"
#~ msgstr "Saugumo revizijos projektas"

#~ msgid "Testing Security Team"
#~ msgstr "Testing Saugumo komanda"

#~ msgid "Alioth administrators"
#~ msgstr "Alioth administratoriai"

#~ msgid "User support"
#~ msgstr "Vartotojų palaikymas"

#~ msgid "Embedded systems"
#~ msgstr "Įdiegtos sistemos"

#~ msgid "Firewalls"
#~ msgstr "Saugos siena"

#~ msgid "Laptops"
#~ msgstr "Nešiojami kompiuteriai"

#~ msgid "Special Configurations"
#~ msgstr "Specialios konfigūracijos"

#~ msgid "Ports"
#~ msgstr "Pernešimai"

#~ msgid "CD Vendors Page"
#~ msgstr "CD Pardavėjų puslapis"

#~ msgid "Consultants Page"
#~ msgstr "Konsultantų puslapis"
