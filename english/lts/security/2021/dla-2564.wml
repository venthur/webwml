<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Alex Birnberg discovered a cross-site scripting (XSS) vulnerability in
the Horde Application Framework, more precisely its Text Filter API.
An attacker could take control of a user's mailbox by sending a
crafted e-mail.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-26929">CVE-2021-26929</a>

    <p>An XSS issue was discovered in Horde Groupware Webmail Edition
    (where the Horde_Text_Filter library is used). The attacker can
    send a plain text e-mail message, with JavaScript encoded as a
    link or email that is mishandled by preProcess in Text2html.php,
    because bespoke use of \x00\x00\x00 and \x01\x01\x01 interferes
    with XSS defenses.</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
2.3.5-1+deb9u1.</p>

<p>We recommend that you upgrade your php-horde-text-filter packages.</p>

<p>For the detailed security status of php-horde-text-filter please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/php-horde-text-filter">https://security-tracker.debian.org/tracker/php-horde-text-filter</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2564.data"
# $Id: $
