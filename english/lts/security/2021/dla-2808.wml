<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>There were a couple of vulnerabilites found in src:python3.5, the
Python interpreter v3.5, and are as follows:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3733">CVE-2021-3733</a>

    <p>The ReDoS-vulnerable regex has quadratic worst-case complexity
    and it allows cause a denial of service when identifying
    crafted invalid RFCs. This ReDoS issue is on the client side
    and needs remote attackers to control the HTTP server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3737">CVE-2021-3737</a>

    <p>HTTP client can get stuck infinitely reading len(line) < 64k
    lines after receiving a <q>100 Continue</q> HTTP response. This
    could lead to the client being a bandwidth sink for anyone
    in control of a server.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.5.3-1+deb9u5.</p>

<p>We recommend that you upgrade your python3.5 packages.</p>

<p>For the detailed security status of python3.5 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python3.5">https://security-tracker.debian.org/tracker/python3.5</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2808.data"
# $Id: $
