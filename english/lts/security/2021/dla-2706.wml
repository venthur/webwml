<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been found in the Apache HTTP server, which
could result in denial of service. In addition the implementation of
the MergeSlashes option could result in unexpected behaviour.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.4.25-3+deb9u10.</p>

<p>We recommend that you upgrade your apache2 packages.</p>

<p>For the detailed security status of apache2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/apache2">https://security-tracker.debian.org/tracker/apache2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2706.data"
# $Id: $
