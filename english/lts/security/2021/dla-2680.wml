<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jamie Landeg-Jones and Manfred Paul discovered a buffer overflow vulnerability
in NGINX, a small, powerful, scalable web/proxy server.</p>

<p>NGINX has a buffer overflow for years that exceed four digits, as demonstrated
by a file with a modification date in 1969 that causes an integer overflow (or
a false modification date far in the future), when encountered by the autoindex
module.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.10.3-1+deb9u7.</p>

<p>We recommend that you upgrade your nginx packages.</p>

<p>For the detailed security status of nginx please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nginx">https://security-tracker.debian.org/tracker/nginx</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2680.data"
# $Id: $
