<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that <a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a> in the Oniguruma regular
expressions library, notably used in PHP mbstring, was a false-positive. In
consequence the patch for <a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a> was reverted. For reference, the
original advisory text follows.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a>

    <p>In Oniguruma an attacker able to supply a regular expression for
    compilation may be able to overflow a buffer by one byte in
    concat_opt_exact_str in src/regcomp.c</p></li>

</ul>

<p>For Debian 9 stretch, this problem has been fixed in version
6.1.3-2+deb9u2.</p>

<p>We recommend that you upgrade your libonig packages.</p>

<p>For the detailed security status of libonig please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libonig">https://security-tracker.debian.org/tracker/libonig</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2431-2.data"
# $Id: $
