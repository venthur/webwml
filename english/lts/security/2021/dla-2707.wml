<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>One security issue has been discovered in sogo.</p>

<p>SOGo does not validate the signatures of any SAML assertions it receives.
Any actor with network access to the deployment could impersonate users when
SAML is the authentication method.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.2.6-2+deb9u1.</p>

<p>We recommend that you upgrade your sogo packages.</p>

<p>ATTENTION! If you are using SAML authentication, use sogo-tool to immediately
delete users sessions and force all users to visit the login page:</p>

    <p>sogo-tool -v expire-sessions 1</p>
    <p>systemctl restart memcached</p>

<p>For the detailed security status of sogo please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sogo">https://security-tracker.debian.org/tracker/sogo</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2707.data"
# $Id: $
