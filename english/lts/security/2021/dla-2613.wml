<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>node-underscore and libjs-underscore are vulnerable to Arbitrary Code
Execution via the template function, particulary when a variable
property is passed as an argument as it is not sanitized.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1.8.3~dfsg-1+deb9u1.</p>

<p>We recommend that you upgrade your underscore packages.</p>

<p>For the detailed security status of underscore please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/underscore">https://security-tracker.debian.org/tracker/underscore</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2613.data"
# $Id: $
