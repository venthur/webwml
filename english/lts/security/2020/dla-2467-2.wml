<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2020-27783">CVE-2020-27783</a>, released as DLA 2467-1, was incomplete as
the &lt;math/svg&gt; component was still affected by the vulnerability.  This
update includes an additional patch that completes the fix.  Note that a
package with version 3.7.1-1+deb9u2 was uploaded, but before the
publication of the advisory a regression was discovered, which was
immediately corrected prior to publication of this advisory.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.7.1-1+deb9u3.</p>

<p>We recommend that you upgrade your lxml packages.</p>

<p>For the detailed security status of lxml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lxml">https://security-tracker.debian.org/tracker/lxml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2467-2.data"
# $Id: $
