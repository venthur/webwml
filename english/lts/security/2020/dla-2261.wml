<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It has been discovered, that a vulnerability in php5, a server-side,
HTML-embedded scripting language, could lead to exhausted disk space on
the server. When using overly long filenames or field names, a memory
limit could be hit which results in stopping the upload but not cleaning
up behind.</p>

<p>Further the embedded version of <q>file</q> is vulnerable to <a href="https://security-tracker.debian.org/tracker/CVE-2019-18218">CVE-2019-18218</a>.
As it can not be exploited the same in php5 as in file, this issue is not
handled as an own CVE but just as a bug, that has been fixed here
(restrict the number of CDF_VECTOR elements to prevent a heap-based
buffer overflow (4-byte out-of-bounds write)).</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
5.6.40+dfsg-0+deb8u12.</p>

<p>We recommend that you upgrade your php5 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2261.data"
# $Id: $
