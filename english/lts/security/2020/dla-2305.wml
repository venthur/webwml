<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Use-after-free in libtransmission/variant.c in Transmission before
3.00 allows remote attackers to cause a denial of service (crash)
or possibly execute arbitrary code via a crafted torrent file.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.92-2+deb9u2.</p>

<p>We recommend that you upgrade your transmission packages.</p>

<p>For the detailed security status of transmission please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/transmission">https://security-tracker.debian.org/tracker/transmission</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2305.data"
# $Id: $
