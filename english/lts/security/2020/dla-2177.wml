<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Felix Wilhelm of Google Project Zero discovered a flaw in git, a fast,
scalable, distributed revision control system. With a crafted URL that
contains a newline, the credential helper machinery can be fooled to
return credential information for a wrong host.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.1.4-2.1+deb8u9.</p>

<p>We recommend that you upgrade your git packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2177.data"
# $Id: $
