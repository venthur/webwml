<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<p>WARNING: The fix for <a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">CVE-2020-1938</a> may disrupt services that rely on a
working AJP configuration. The option secretRequired defaults to true
now. You should define a secret in your server.xml or you can revert
back by setting secretRequired to false.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-17563">CVE-2019-17563</a>

    <p>When using FORM authentication with Apache Tomcat there was a narrow
    window where an attacker could perform a session fixation attack.
    The window was considered too narrow for an exploit to be practical
    but, erring on the side of caution, this issue has been treated as a
    security vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1935">CVE-2020-1935</a>

    <p>In Apache Tomcat the HTTP header parsing code used an approach to
    end-of-line parsing that allowed some invalid HTTP headers to be
    parsed as valid. This led to a possibility of HTTP Request Smuggling
    if Tomcat was located behind a reverse proxy that incorrectly
    handled the invalid Transfer-Encoding header in a particular manner.
    Such a reverse proxy is considered unlikely.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1938">CVE-2020-1938</a>

    <p>When using the Apache JServ Protocol (AJP), care must be taken when
    trusting incoming connections to Apache Tomcat. Tomcat treats AJP
    connections as having higher trust than, for example, a similar HTTP
    connection. If such connections are available to an attacker, they
    can be exploited in ways that may be surprising. Previously Tomcat
    shipped with an AJP Connector enabled by default that listened on
    all configured IP addresses. It was expected (and recommended in the
    security guide) that this Connector would be disabled if not
    required.
    .
    Note that Debian already disabled the AJP connector by default.
    Mitigation is only required if the AJP port was made accessible to
    untrusted users.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9484">CVE-2020-9484</a>

    <p>When using Apache Tomcat and an attacker is able to control the
    contents and name of a file on the server; and b) the server is
    configured to use the PersistenceManager with a FileStore; and c)
    the PersistenceManager is configured with
    sessionAttributeValueClassNameFilter="null" (the default unless a
    SecurityManager is used) or a sufficiently lax filter to allow the
    attacker provided object to be deserialized; and d) the attacker
    knows the relative file path from the storage location used by
    FileStore to the file the attacker has control over; then, using a
    specifically crafted request, the attacker will be able to trigger
    remote code execution via deserialization of the file under their
    control. Note that all of conditions a) to d) must be true for the
    attack to succeed.</p>


<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
8.0.14-1+deb8u17.</p>

<p>We recommend that you upgrade your tomcat8 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2209.data"
# $Id: $
