<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were two issues in
<a href="https://www.djangoproject.com/">Django</a>, the Python web development
framework:</p>

<ul>
<li>
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-13254">
        CVE-2020-13254: Potential data leakage via malformed memcached keys.
    </a>

    <p>In cases where a memcached backend does not perform key validation,
    passing malformed cache keys could result in a key collision, and potential
    data leakage. In order to avoid this vulnerability, key validation is added
    to the memcached cache backends.</p>
</li>
<li>
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-13596">
        CVE-2020-13596: Possible XSS via admin ForeignKeyRawIdWidget.
    </a>

    <p>Query parameters to the admin ForeignKeyRawIdWidget were not
    properly URL encoded, posing an XSS attack vector.
    ForeignKeyRawIdWidget now ensures query parameters are correctly
    URL encoded.</p>
</li>
</ul>

<p>For more information, please see
<a href="https://www.djangoproject.com/weblog/2020/jun/03/security-releases/">upstream's
own announcment</a>.</p>

<p>This upload also addresses test failures introduced in
<tt>1.7.11-1+deb8u3</tt> and <tt>1.7.11-1+deb8u8</tt> via the fixes for
<a href="https://security-tracker.debian.org/tracker/CVE-2019-19844">CVE-2018-7537</a>
and <a href="https://security-tracker.debian.org/tracker/CVE-2019-19844">CVE-2019-19844</a>
respectfully.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.7.11-1+deb8u9.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2233.data"
# $Id: $
