<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Fuzzing found two further file-format specific issues in libarchive, a
read-only segfault in 7z, and an infinite loop in ISO9660.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1000019">CVE-2019-1000019</a>

    <p>Out-of-bounds Read vulnerability in 7zip decompression, that can
    result in a crash (denial of service, CWE-125)</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1000020">CVE-2019-1000020</a>

    <p>Vulnerability in ISO9660 parser that can result in DoS by infinite
    loop (CWE-835)</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.1.2-11+deb8u7.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1668.data"
# $Id: $
