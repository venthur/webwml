<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were found in icedtea-web, an
implementation of the Java Network Launching Protocol (JNLP).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10181">CVE-2019-10181</a>

     <p>It was found that in icedtea-web executable code could be injected
     in a JAR file without compromising the signature verification. An
     attacker could use this flaw to inject code in a trusted JAR. The
     code would be executed inside the sandbox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10182">CVE-2019-10182</a>

     <p>It was found that icedtea-web did not properly sanitize paths from
     &lt;jar/&gt; elements in JNLP files. An attacker could trick a victim
     into running a specially crafted application and use this flaw to
     upload arbitrary files to arbitrary locations in the context of the
     user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10185">CVE-2019-10185</a>

    <p>It was found that icedtea-web was vulnerable to a zip-slip attack
    during auto-extraction of a JAR file. An attacker could use this
    flaw to write files to arbitrary locations. This could also be used
    to replace the main running application and, possibly, break out of
    the sandbox.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.5.3-1+deb8u1.</p>

<p>We recommend that you upgrade your icedtea-web packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1914.data"
# $Id: $
