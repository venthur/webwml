<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16993">CVE-2019-16993</a>

  <p>In phpBB, includes/acp/acp_bbcodes.php had improper verification of a
  CSRF token on the BBCode page in the Administration Control Panel. An
  actual CSRF attack was possible if an attacker also managed to retrieve
  the session id of a reauthenticated administrator prior to targeting
  them.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13776">CVE-2019-13776</a>

  <p>phpBB allowed the stealing of an Administration Control Panel session id
  by leveraging CSRF in the Remote Avatar feature. The CSRF Token Hijacking
  lead to stored XSS.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.0.12-5+deb8u4.</p>

<p>We recommend that you upgrade your phpbb3 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1942.data"
# $Id: $
