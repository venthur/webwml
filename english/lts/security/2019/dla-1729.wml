<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several vulnerabilities have been found in wireshark, a network traffic
analyzer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-9209">CVE-2019-9209</a>:

      <p>Preventing the crash of the ASN.1 BER and related dissectors by
      avoiding a buffer overflow associated with excessive digits in
      time values.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9349">CVE-2017-9349</a>:

      <p>Fixing an infinite loop in the DICOM dissector by validating
      a length value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9344">CVE-2017-9344</a>:

      <p>Avoid a divide by zero, by validating an interval value in the
      Bluetooth L2CAP dissector.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.12.1+g01b65bf-4+deb8u18.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1729.data"
# $Id: $
