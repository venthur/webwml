<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Wireshark, a network
traffic analyzer. An attacker could cause a denial of service (infinite loop or
application crash) via packet injection or a crafted capture file. Improper URL
handling in Wireshark could also allow remote code execution. A double-click
will no longer automatically open the URL in pcap(ng) files and instead copy it
to the clipboard where it can be inspected and pasted to the browser's address
bar.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
2.6.20-0+deb9u3.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2967.data"
# $Id: $
