<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libxfont, an X11 font rasterisation library.
By creating symlinks, a local attacker can open (but not read) local files
as user root. This might create unwanted actions with special files like
/dev/watchdog.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1:2.0.1-3+deb9u2.</p>

<p>We recommend that you upgrade your libxfont packages.</p>

<p>For the detailed security status of libxfont please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxfont">https://security-tracker.debian.org/tracker/libxfont</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2901.data"
# $Id: $
