<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in tinyxml, a C++ XML parsing library.</p>

<p>Crafted XML messages could lead to an infinite loop in
TiXmlParsingData::Stamp(), which results in a denial of service.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.6.2-4+deb9u1.</p>

<p>We recommend that you upgrade your tinyxml packages.</p>

<p>For the detailed security status of tinyxml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tinyxml">https://security-tracker.debian.org/tracker/tinyxml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2988.data"
# $Id: $
