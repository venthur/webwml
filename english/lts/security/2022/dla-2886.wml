<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in the Simple Linux Utility
for Resource Management (SLURM), a cluster resource management and job
scheduling system, which could result in denial of service,
information disclosure or privilege escalation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12838">CVE-2019-12838</a>

    <p>SchedMD Slurm allows SQL Injection.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12693">CVE-2020-12693</a>

    <p>In the rare case where Message Aggregation is enabled, Slurm
    allows Authentication Bypass via an Alternate Path or Channel. A
    race condition allows a user to launch a process as an arbitrary
    user.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27745">CVE-2020-27745</a>

    <p>RPC Buffer Overflow in the PMIx MPI plugin.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31215">CVE-2021-31215</a>

    <p>SchedMD Slurm allows remote code execution as SlurmUser because
    use of a PrologSlurmctld or EpilogSlurmctld script leads to
    environment mishandling.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
16.05.9-1+deb9u5.</p>

<p>We recommend that you upgrade your slurm-llnl packages.</p>

<p>For the detailed security status of slurm-llnl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/slurm-llnl">https://security-tracker.debian.org/tracker/slurm-llnl</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2886.data"
# $Id: $
