<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in fribidi, a free Implementation of the
Unicode BiDi algorithm. The issues are related to stack-buffer-overflow,
heap-buffer-overflow, and a SEGV.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25308">CVE-2022-25308</a>

      <p>stack-buffer-overflow issue in main()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25309">CVE-2022-25309</a>

      <p>heap-buffer-overflow issue in fribidi_cap_rtl_to_unicode()</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25310">CVE-2022-25310</a>

      <p>SEGV issue in fribidi_remove_bidi_marks()</p>


<p>For Debian 9 stretch, these problems have been fixed in version
0.19.7-1+deb9u2.</p>

<p>We recommend that you upgrade your fribidi packages.</p>

<p>For the detailed security status of fribidi please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/fribidi">https://security-tracker.debian.org/tracker/fribidi</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2974.data"
# $Id: $
