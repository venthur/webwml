<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in the NVIDIA binary
driver and libraries that provide optimized hardware acceleration which may
lead to denial of service, information disclosure or data corruption.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
390.144-1~deb9u1.</p>

<p>We recommend that you upgrade your nvidia-graphics-drivers packages.</p>

<p>For the detailed security status of nvidia-graphics-drivers please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/nvidia-graphics-drivers">https://security-tracker.debian.org/tracker/nvidia-graphics-drivers</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2888.data"
# $Id: $
