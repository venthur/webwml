<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple out-of-bounds error were discovered in qtsvg-opensource-src.
The highest threat from <a href="https://security-tracker.debian.org/tracker/CVE-2021-3481">CVE-2021-3481</a> (at least) is to data
confidentiality the application availability.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
5.7.1~20161021-2.1+deb9u1.</p>

<p>We recommend that you upgrade your qtsvg-opensource-src packages.</p>

<p>For the detailed security status of qtsvg-opensource-src please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/qtsvg-opensource-src">https://security-tracker.debian.org/tracker/qtsvg-opensource-src</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2885.data"
# $Id: $
