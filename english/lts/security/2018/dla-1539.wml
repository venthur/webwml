<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Samba, a SMB/CIFS file,
print, and login server for Unix. The Common Vulnerabilities and
Exposures project identifies the following issues:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10858">CVE-2018-10858</a>

    <p>Svyatoslav Phirsov discovered that insufficient input validation in
    libsmbclient allowed a malicious Samba server to write to the
    client's heap memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10919">CVE-2018-10919</a>

    <p>Phillip Kuhrt discovered that Samba when acting as an Active Domain
    controller disclosed some sensitive attributes.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.2.14+dfsg-0+deb8u10.</p>

<p>We recommend that you upgrade your samba packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1539.data"
# $Id: $
