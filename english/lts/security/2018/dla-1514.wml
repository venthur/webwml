<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Nick Roessler from the University of Pennsylvania has found a buffer overflow
in texlive-bin, the executables for TexLive, the popular distribution of TeX
document production system.</p>

<p>This buffer overflow can be used for arbitrary code execution by crafting a
special type1 font (.pfb) and provide it to users running pdf(la)tex, dvips or
luatex in a way that the font is loaded.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2014.20140926.35254-6+deb8u1.</p>

<p>We recommend that you upgrade your texlive-bin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1514.data"
# $Id: $
