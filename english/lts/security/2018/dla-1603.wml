<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several issues were found in suricata, an intrusion detection and
prevention tool.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7177">CVE-2017-7177</a>

    <p>Suricata has an IPv4 defragmentation evasion issue caused by lack
    of a check for the IP protocol during fragment matching.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15377">CVE-2017-15377</a>

    <p>It was possible to trigger lots of redundant checks on the content
    of crafted network traffic with a certain signature, because of
    DetectEngineContentInspection in detect-engine-content-inspection.c.
    The search engine doesn't stop when it should after no match is
    found; instead, it stops only upon reaching inspection-recursion    limit (3000 by default).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6794">CVE-2018-6794</a>

    <p>Suricata is prone to an HTTP detection bypass vulnerability in
    detect.c and stream-tcp.c. If a malicious server breaks a normal
    TCP flow and sends data before the 3-way handshake is complete,
    then the data sent by the malicious server will be accepted by web
    clients such as a web browser or Linux CLI utilities, but ignored
    by Suricata IDS signatures. This mostly affects IDS signatures for
    the HTTP protocol and TCP stream content; signatures for TCP packets
    will inspect such network traffic as usual.</p>

<p>TEMP-0856648-2BC2C9 (no CVE assigned yet)</p>

    <p>Out of bounds read in app-layer-dns-common.c.
    On a zero size A or AAAA record, 4 or 16 bytes would still be read.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.0.7-2+deb8u3.</p>

<p>We recommend that you upgrade your suricata packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1603.data"
# $Id: $
