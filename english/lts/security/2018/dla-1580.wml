<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>systemd was found to suffer from multiple security vulnerabilities
ranging from denial of service attacks to possible root privilege
escalation.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-1049">CVE-2018-1049</a>

    <p>A race condition exists between .mount and .automount units such
    that automount requests from kernel may not be serviced by systemd
    resulting in kernel holding the mountpoint and any processes that
    try to use said mount will hang. A race condition like this may
    lead to denial of service, until mount points are unmounted.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15686">CVE-2018-15686</a>

    <p>A vulnerability in unit_deserialize of systemd allows an attacker
    to supply arbitrary state across systemd re-execution via
    NotifyAccess. This can be used to improperly influence systemd
    execution and possibly lead to root privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-15688">CVE-2018-15688</a>

    <p>A buffer overflow vulnerability in the dhcp6 client of systemd
    allows a malicious dhcp6 server to overwrite heap memory in
    systemd-networkd, which is not enabled by default in Debian.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
215-17+deb8u8.</p>

<p>We recommend that you upgrade your systemd packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1580.data"
# $Id: $
