<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Daniel J. Bernstein, Joachim Breitner, Daniel Genkin, Leon Groot
Bruinderink, Nadia Heninger, Tanja Lange, Christine van Vredendaal and
Yuval Yarom discovered that gnupg is prone to a local side-channel
attack allowing full key recovery for RSA-1024.</p>

<p>See <a href="https://eprint.iacr.org/2017/627">https://eprint.iacr.org/2017/627</a> for details.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.4.12-7+deb7u9.</p>

<p>We recommend that you upgrade your gnupg packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1080.data"
# $Id: $
