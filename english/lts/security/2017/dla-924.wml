<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two security vulnerabilities have been discovered in the Tomcat
servlet and JSP engine.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5647">CVE-2017-5647</a>

   <p>A bug in the handling of the pipelined requests when send file was
   used resulted in the pipelined request being lost when send file
   processing of the previous request completed. This could result in
   responses appearing to be sent for the wrong request.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-5648">CVE-2017-5648</a>

   <p>It was noticed that some calls to application listeners did not use
   the appropriate facade object. When running an untrusted application
   under a SecurityManager, it was therefore possible for that
   untrusted application to retain a reference to the request or
   response object and thereby access and/or modify information
   associated with another web application.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
7.0.28-4+deb7u12.</p>

<p>We recommend that you upgrade your tomcat7 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-924.data"
# $Id: $
