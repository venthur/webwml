<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Felix Wilhelm reported that several buffer handling functions in
libxml2, a library providing support to read, modify and write XML and
HTML files, don't check for integer overflows, resulting in
out-of-bounds memory writes if specially crafted, multi-gigabyte XML
files are processed. An attacker can take advantage of this flaw for
denial of service or execution of arbitrary code.</p>

<p>For the oldstable distribution (buster), this problem has been fixed
in version 2.9.4+dfsg1-7+deb10u4.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 2.9.10+dfsg-6.7+deb11u2.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5142.data"
# $Id: $
