<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>The following vulnerabilities have been discovered in the webkit2gtk
web engine:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30934">CVE-2021-30934</a>

    <p>Dani Biro discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30936">CVE-2021-30936</a>

    <p>Chijin Zhou discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30951">CVE-2021-30951</a>

    <p>Pangu discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30952">CVE-2021-30952</a>

    <p>WeBin discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30953">CVE-2021-30953</a>

    <p>VRIJ discovered that processing maliciously crafted web content
    may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30954">CVE-2021-30954</a>

    <p>Kunlun Lab discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-30984">CVE-2021-30984</a>

    <p>Kunlun Lab discovered that processing maliciously crafted web
    content may lead to arbitrary code execution.</p></li>

</ul>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2.34.4-1~deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2.34.4-1~deb11u1.</p>

<p>We recommend that you upgrade your webkit2gtk packages.</p>

<p>For the detailed security status of webkit2gtk please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5060.data"
# $Id: $
