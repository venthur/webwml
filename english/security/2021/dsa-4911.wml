<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the chromium web browser.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21227">CVE-2021-21227</a>

    <p>Gengming Liu discovered a data validation issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21228">CVE-2021-21228</a>

    <p>Rob Wu discovered a policy enforcement error.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21229">CVE-2021-21229</a>

    <p>Mohit Raj discovered a user interface error in the file downloader.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21230">CVE-2021-21230</a>

    <p>Manfred Paul discovered use of an incorrect type.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21231">CVE-2021-21231</a>

    <p>Sergei Glazunov discovered a data validation issue in the v8 javascript
    library.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21232">CVE-2021-21232</a>

    <p>Abdulrahman Alqabandi discovered a use-after-free issue in the developer
    tools.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21233">CVE-2021-21233</a>

    <p>Omair discovered a buffer overflow issue in the ANGLE library.</p></li>

</ul>

<p>For the stable distribution (buster), these problems have been fixed in
version 90.0.4430.93-1~deb10u1.</p>

<p>We recommend that you upgrade your chromium packages.</p>

<p>For the detailed security status of chromium please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4911.data"
# $Id: $
