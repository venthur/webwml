#use wml::debian::template title="Security Information" GEN_TIME="yes" MAINPAGE="true"
#use wml::debian::toc
#use wml::debian::recent_list_security
#include "$(ENGLISHDIR)/releases/info"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
<li><a href="#keeping-secure">Keeping your Debian System secure</a></li>
<li><a href="#DSAS">Recent Advisories</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian takes security very seriously. We handle all security problems brought to our attention and ensure that they are corrected within a reasonable timeframe.</p>
</aside>

<p>
Experience has shown that <q>security through obscurity</q> never works. Therefore, public disclosure allows for quicker and better solutions of security problems. In that respect, this page addresses Debian's status regarding various known security holes, which could potentially affect the Debian operating system.
</p>

<p>
The Debian project coordinates many security advisories with other free software vendors, and as a result, these advisories are published the same day a vulnerability is made public. In addition, we have our own <a href="audit/">Security Audit</a> team. They review the archive and look for unfixed security bugs.
</p>
  
# "reasonable timeframe" might be too vague, but we don't have 
# accurate statistics. For older (out of date) information and data
# please read:
# https://www.debian.org/News/2004/20040406  [ Year 2004 data ]
# and (older)
# https://people.debian.org/~jfs/debconf3/security/ [ Year 2003 data ]
# https://lists.debian.org/debian-security/2001/12/msg00257.html [ Year 2001]
# If anyone wants to do up-to-date analysis please contact me (jfs)
# and I will provide scripts, data and database schemas.


<p>
Debian also participates in security standardization efforts:
</p>

<ul>
  <li>The <a href="#DSAS">Debian Security Advisories</a> are <a href="cve-compatibility">CVE-Compatible</a> (review the <a href="crossreferences">cross references</a>).</li>
  <li>Debian is represented in the Board of the <a href="https://oval.cisecurity.org/">Open Vulnerability Assessment Language</a> project.</li>
</ul>

<h2><a id="keeping-secure">Keeping your Debian System secure</a></h2>


<p>
In order to receive the latest Debian security advisories, please subscribe to the <a href="https://lists.debian.org/debian-security-announce/">debian-security-announce</a> mailing list.
</p>

<p>
On top of that, you can use <a href="https://packages.debian.org/stable/admin/apt">APT</a> to easily get the latest security updates. To keep your Debian operating system up-to-date with security patches, please add the following line to your <code>/etc/apt/sources.list</code> file:
</p>

<pre>
deb&nbsp;http://security.debian.org/debian-security&nbsp;<current_release_security_name>&nbsp;main&nbsp;contrib&nbsp;non-free
</pre>

<p>
After saving the changes, run the following two commands to download and install the pending updates:
</p>

<pre>
apt-get update &amp;&amp; apt-get upgrade
</pre> 

<p>
The security archive is signed with the usual <a href="https://ftp-master.debian.org/keys.html">Debian archive keys</a>.
</p>

<p>
For more information about security issues in Debian, please refer to our FAQ and our documentation:
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="faq">Security FAQ</a></button> <button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="../doc/user-manuals#securing">Securing Debian</a></button></p>


<aside class="light">
  <span class="fa fa-rss fa-5x"></span>
</aside>

<h2><a id="DSAS">Recent Advisories</a></h2>

<p>These web pages include a condensed archive of security advisories posted to
the <a href="https://lists.debian.org/debian-security-announce/">\
debian-security-announce</a> list.

<p>
<:= get_recent_security_list( '1m', '6', '.', '$(ENGLISHDIR)/security' ) :>
</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (titles only)" href="dsa">
<link rel="alternate" type="application/rss+xml"
 title="Debian Security Advisories (summaries)" href="dsa-long">
:#rss#}

<p>
The latest Debian security advisories are available as <a href="dsa">RDF files</a>. We also offer a slightly <a href="dsa-long">longer version</a> of the files which includes the first paragraph of the corresponding advisory. That way you can easily spot what the advisory is about.
</p>

#include "$(ENGLISHDIR)/security/index.include"
<p>Older security advisories are also available:
<:= get_past_sec_list(); :>

<p>Debian distributions are not vulnerable to all security problems. The
<a href="https://security-tracker.debian.org/">Debian Security Tracker</a> 
collects all information about the vulnerability status of Debian packages.
It can be searched by CVE name or by package.</p>
