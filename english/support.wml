#use wml::debian::template title="User Support" MAINPAGE="true"
#use wml::debian::recent_list

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">


<ul class="toc">
  <li><a href="#irc">IRC (real time Support)</a></li>
  <li><a href="#mail_lists">Mailing Lists</a></li>
  <li><a href="#usenet">Usenet Newsgroups</a></li>
  <li><a href="#forums">Debian User Forums</a></li>
  <li><a href="#maintainers">How to contact Package Maintainers</a></li>
  <li><a href="#bts">Bug Tracking System</a></li>
  <li><a href="#release">Known Problems</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debian support is offered by a group of volunteers. If this community-driven support doesn't fulfil your needs and you can't find the answer in our <a href="doc/">documentation</a>, you may hire a <a href="consultants/">consultant</a> to maintain or add additional functionality your Debian system.</p>
</aside>

<h2><a id="irc">IRC (real time Support)</a></h2>

<p>
<a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) is a great way to chat with people from all over the world in real time. It's a text-based chat system for instant messaging. On IRC you can enter chat rooms (so-called channels) or you can directly chat with individual persons via private messages.
</p>

<p>
IRC channels dedicated to Debian can be found on <a href="https://www.oftc.net/">OFTC</a>. For a full list of Debian channels, please refer to our <a href="https://wiki.debian.org/IRC">Wiki</a>. You can also use a <a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">search engine</a> to check for Debian-related channels.
</p>

<h3>IRC Clients</h3>

<p>
To connect to the IRC network, you can either use OFTC's <a href="https://www.oftc.net/WebChat/">WebChat</a> in your preferred web browser or install a client on your computer. There are lots of different clients out there, some with a graphical interface, some for the console. Some popular IRC clients have been packaged for Debian, for example:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (text mode)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (text mode)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul> 

<p>
The Debian Wiki offers a more comprehensive <a href="https://wiki.debian.org/IrcClients">list of IRC clients</a> which are available as Debian packages.
</p>

<h3>Connect to the Network</h3>

<p>
Once you have the client installed, you need to tell it to connect
to the server. In most clients, you can do that by typing:
</p>

<pre>
/server irc.debian.org
</pre>

<p>The hostname irc.debian.org is an alias for irc.oftc.net. In some clients (such as irssi) you will need to type this instead:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
# <p>Once you are connected, join channel <code>#debian-foo</code> by typing</p>
# <pre>/join #debian</pre>
# for support in your language.
# <p>For support in English, read on</p>

<h3>Join a Channel</h3>

<p>Once you are connected, join channel <code>#debian</code> by typing this command:</p>

<pre>
/join #debian
</pre>

<p>Note: graphical clients like HexChat or Konversation often have a button
or a menu entry for connecting to servers and joining channels.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Read our IRC FAQ</a></button></p>

<h2><a id="mail_lists">Mailing Lists</a></h2>

<p>
More than thousand active <a href="intro/people.en.html#devcont">developers</a> spread around the world work on Debian in their spare time—and in their own timezones. Therefore we communicate primarily through e-mail. Similarly, most of the conversation between Debian developers and users happens on different <a href="MailingLists/">mailing lists</a>:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
  <li>For user support in English, please contact the <a href="https://lists.debian.org/debian-user/">debian-user</a> mailing list.</li>
  <li>For user support in other languages, please check the <a href="https://lists.debian.org/users.html">index</a> of other user mailing lists.</li>
</ul>

<p>
Of course, there are plenty of other mailing lists, dedicated to some aspect of the Linux ecosystem and not Debian-specific. Please use your favorite search engine to find the most suitable list for your purpose.
</p>

<h2><a id="usenet">Usenet Newsgroups</a></h2>

<p>
A lot of our <a href="#mail_lists">mailing lists</a> can be browsed as newsgroups, in the <kbd>linux.debian.*</kbd> hierarchy. For easy access to the newsgroups, you can use a web interface such as <a href="https://groups.google.com/forum/">Google Groups</a>.
</p>

<h2><a id="forums">Debian User Forums</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p>
<a href="https://forums.debian.net">Debian User Forums</a> is a web portal 
where thousands of other users discuss Debian-related topics, ask questions,
and help each other by answering them. You can read all boards without
having to register. If you want to participate in the discussion and publish
your own postings, please register and log in.
</p>

<h2><a id="maintainers">How to contact Package Maintainers</a></h2>

<p>
Basically, there are two common ways to get in touch with a maintainer of a Debian package:
</p>

<ul>
  <li>If you'd like to report a bug, simply file a <a href="bts">bug report</a>; the maintainer automatically receives a copy of your bug report.</li>
  <li>If you simply want to send an email to the maintainer, use the special mail aliases set up for each package:<br>
      &lt;<em>package_name</em>&gt;@packages.debian.org</li>
</ul>

<toc-add-entry name="doc" href="doc/">Documentation</toc-add-entry>

<p>An important part of any operating system is documentation, the
technical manuals that describe the operation and use of programs. As part
of its efforts to create a high-quality free operating system, the Debian
Project is making every effort to provide all of its users with proper
documentation in an easily accessible form.</p>

<p>See the <a href="doc/">documentation page</a> for a list of Debian
manuals and other documentation, including the Installation Guide, the
Debian FAQ, and other users' and developers' manuals.</p>


<toc-add-entry name="consultants" href="consultants/">Consultants</toc-add-entry>

<h2><a id="bts">Bug Tracking System</a></h2>

<p>
The Debian distribution has its own <a href="Bugs/">bug tracker</a> with bugs reported by users and developers. Every bug has a unique number and is kept on file until it is marked as resolved. There are two different ways to report a bug:
</p>

<ul>
  <li>The recommended way is to use the Debian package <em>reportbug</em>.</li>
  <li>Alternatively, you can send an email as described on this <a href="Bugs/Reporting">page</a>.</li>
</ul>

<h2><a id="release">Known Problems</a></h2>

<p>Limitations and severe problems of the current stable distribution
(if any) are described on <a href="releases/stable/">the release page</a>.</p>

<p>Please pay particular attention to the <a href="releases/stable/releasenotes">release
notes</a> and the <a href="releases/stable/errata">errata</a>.</p>
