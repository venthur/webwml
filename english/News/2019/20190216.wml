<define-tag pagetitle>Updated Debian 9: 9.8 released</define-tag>
<define-tag release_date>2019-02-16</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>9</define-tag>
<define-tag codename>stretch</define-tag>
<define-tag revision>9.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>The Debian project is pleased to announce the eighth update of its
stable distribution Debian <release> (codename <q><codename></q>). 
This point release mainly adds corrections for security issues,
along with a few adjustments for serious problems.  Security advisories
have already been published separately and are referenced where available.</p>

<p>Please note that the point release does not constitute a new version of Debian
<release> but only updates some of the packages included.  There is
no need to throw away old <q><codename></q> media. After installation,
packages can be upgraded to the current versions using an up-to-date Debian
mirror.</p>

<p>Those who frequently install updates from security.debian.org won't have
to update many packages, and most such updates are
included in the point release.</p>

<p>New installation images will be available soon at the regular locations.</p>

<p>Upgrading an existing installation to this revision can be achieved by
pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Miscellaneous Bugfixes</h2>

<p>This stable update adds a few important corrections to the following packages:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction arc "Fix directory traversal bugs [CVE-2015-9275], arcdie crash when called with more than 1 variable argument and version 1 arc header reading">
<correction astroml-addons "Fix Python 3 dependencies">
<correction base-files "Update for the point release">
<correction c3p0 "Fix XML External Entity vulnerability [CVE-2018-20433]">
<correction ca-certificates-java "Fix temporary jvm-*.cfg generation on armhf">
<correction chkrootkit "Fix regular expression for filtering out dhcpd and dhclient as false positives from the packet sniffer test">
<correction compactheader "Update to work with newer Thunderbird versions">
<correction courier "Fix @piddir@ substitution">
<correction cups "Security fixes [CVE-2017-18248 CVE-2018-4700]">
<correction debian-edu-config "Fix configuration of personal web pages; re-enable offline installation of a combi server including diskless workstation support; enable Chromium homepage setting at installation time and via LDAP">
<correction debian-installer "Rebuild for the point release">
<correction debian-installer-netboot-images "Rebuild against proposed-updates">
<correction debian-security-support "Update support status of various packages">
<correction dnspython "Fix error when parsing nsec3 bitmap from text">
<correction egg "Skip emacsen-install for unsupported xemacs21">
<correction erlang "Do not install Erlang mode for XEmacs">
<correction espeakup "debian/espeakup.service: Fix compatibility with older versions of systemd">
<correction freerdp "Fix security issues [CVE-2018-8786 CVE-2018-8787 CVE-2018-8788]; add CredSSP v3 and RDP proto v6 support">
<correction ganeti-os-noop "Fix size detection for non-block devices">
<correction glibc "Fix several security isses [CVE-2017-15670 CVE-2017-15671 CVE-2017-15804 CVE-2017-1000408 CVE-2017-1000409 CVE-2017-16997 CVE-2017-18269 CVE-2018-11236 CVE-2018-11237]; avoid segmentation faults on CPUs with AVX512-F; fix a use after free in pthread_create(); check for postgresql in NSS check; fix pthread_cond_wait() in the pshared case on non-x86.">
<correction gnulib "vasnprintf: Fix heap memory overrun bug [CVE-2018-17942]">
<correction gnupg2 "Avoid crash when importing without a TTY">
<correction graphite-api "Fix RequiresMountsFor spelling in systemd service">
<correction grokmirror "Add missing dependency on python-pkg-resources">
<correction gvrng "Fix permissions problem that prevented starting gvrng; generate correct Python dependencies">
<correction ibus "Fix multi-arch installation by removing the gir package's Python dependency">
<correction icinga2 "Fix timestamps being stored as local time in PostgreSQL">
<correction intel-microcode "Add accumulated fixes for Westmere EP (signature 0x206c2) [Intel SA-00161 CVE-2018-3615 CVE-2018-3620 CVE-2018-3646 Intel SA-00115 CVE-2018-3639 CVE-2018-3640 Intel SA-0088 CVE-2017-5753 CVE-2017-5754]">
<correction isort "Fix Python dependencies">
<correction jdupes "Fix potential crash on ARM">
<correction kmodpy "Remove incorrect Multi-Arch: same from python-kmodpy">
<correction libapache2-mod-perl2 "Don't allow &lt;Perl&gt; sections in user controlled configuration [CVE-2011-2767]">
<correction libb2 "Detect if the system can use AVX before actually using it">
<correction libdatetime-timezone-perl "Update included data">
<correction libemail-address-list-perl "Fix DoS vulnerability [CVE-2018-18898]">
<correction libemail-address-perl "Fix DoS vulnerabilities [CVE-2015-7686 CVE-2018-12558]">
<correction libgpod "python-gpod: Add missing dependency on python-gobject-2">
<correction libssh "Fix broken server-side keyboard-interactive authentication">
<correction linux "New upstream release; new upstream version; fix build failures on arm64 and mips*; libceph: fix CEPH_FEATURE_CEPHX_V2 check in calc_signature()">
<correction linux-igd "Make the init script require $network">
<correction lttng-modules "Fix build on linux-rt 4.9 kernels and kernels &gt;= 4.9.0-3">
<correction mistral "Fix <q>std.ssh action may disclose presence of arbitrary files</q> [CVE-2018-16849]">
<correction monkeysign "Fix security issue [CVE-2018-12020]; actually send multiple emails instead of a single one">
<correction mpqc "Also install sc-libtool">
<correction nvidia-graphics-drivers "New upstream release">
<correction nvidia-modprobe "New upstream release">
<correction nvidia-persistenced "New upstream release">
<correction nvidia-settings "New upstream release">
<correction nvidia-xconfig "New upstream release">
<correction openni2 "Fix armhf baseline violation and armel FTBFS caused by NEON usage">
<correction openvpn "Fix NCP behaviour on TLS reconnect, causing <q>AEAD Decrypt error: cipher final failed</q> errors">
<correction parsedatetime "Add support for Python 3">
<correction pdns "Fix security issues [CVE-2018-1046 CVE-2018-10851]; fix MySQL queries with stored procedures; fix LDAP, Lua, OpenDBX backends not finding domains">
<correction pdns-recursor "Fix security issues [CVE-2018-10851 CVE-2018-14626 CVE-2018-14644]">
<correction photocollage "Add missing dependency on gir1.2-gtk-3.0">
<correction postfix "New upstream stable release; avoid postconf failures when postfix-instance-generator runs during boot">
<correction postgresql-9.6 "New upstream release">
<correction postgrey "No change rebuild">
<correction pylint-django "Fix Python 3 dependencies">
<correction python-acme "Backport newer version for tls-sni-01 deprecation">
<correction python-arpy "Fix Python 3 dependencies">
<correction python-certbot "Backport newer version for tls-sni-01 deprecation">
<correction python-certbot-apache "Update for deprecation of tls-sni-01">
<correction python-certbot-nginx "Update for deprecation of tls-sni-01">
<correction python-hypothesis "Fix (inverted) dependencies of python3-hypothesis and python-hypothesis-doc">
<correction python-josepy "New package, required by Certbot">
<correction pyzo "Add missing dependency on python3-pkg-resources">
<correction r-cran-readxl "Fix crash bugs [CVE-2018-20450 CVE-2018-20452]">
<correction rtkit "Move dbus and polkit from Recommends to Depends">
<correction ruby-rack "Fix a possible cross-site scripting vulnerability [CVE-2018-16471]">
<correction samba "New upstream release; s3:ntlm_auth: fix memory leak in manage_gensec_request(); ignore nmbd start errors when there is no non-loopback interface or no local IPv4 non-loopback interface; fix CVE-2018-14629 regression on a non-CNAME record">
<correction sl-modem "Support Linux versions &gt; 3">
<correction sogo-connector "Update to work with newer Thunderbird versions">
<correction sox "Really apply fixes for CVE-2014-8145">
<correction ssh-agent-filter "Fix two-byte out-of-bounds stack write">
<correction supercollider "Disable support for XEmacs and Emacs &lt;=23">
<correction sympa "Remove /etc/sympa/sympa.conf-smime.in from conffiles; use full path for head command in Sympa configuration file">
<correction twitter-bootstrap3 "Fix multiple security vulnerabilities [CVE-2018-14040 CVE-2018-14041 CVE-2018-14042]">
<correction tzdata "New upstream release">
<correction uglifyjs "Fix manpage contents">
<correction uriparser "Fix multiple security vulnerabilties [CVE-2018-19198 CVE-2018-19199 CVE-2018-19200]">
<correction vm "Drop support for xemacs21">
<correction vulture "Add missing dependency on python3-pkg-resources">
<correction wayland "Fix possible integer overflow [CVE-2017-16612]">
<correction wicd "Always depend on net-tools, rather than alternatives">
<correction wvstreams "Work around stack corruption">
<correction xapian-core "Fix leaks of freelist blocks in corner cases, which then get reported as <q>DatabaseCorruptError</q> by Database::check()">
<correction xkeycaps "Prevent segfault in commands.c when more than 8 keysyms per key are present">
<correction yosys "Fix <q>ModuleNotFoundError: No module named 'smtio'</q>">
<correction z3 "Remove incorrect Multi-Arch: same from python-z3">
</table>

<h2>Security Updates</h2>


<p>This revision adds the following security updates to the stable release.
The Security Team has already released an advisory for each of these
updates:</p>

<table border=0>
<tr><th>Advisory ID</th>  <th>Package</th></tr>
<dsa 2018 4330 chromium-browser>
<dsa 2018 4333 icecast2>
<dsa 2018 4334 mupdf>
<dsa 2018 4335 nginx>
<dsa 2018 4336 ghostscript>
<dsa 2018 4337 thunderbird>
<dsa 2018 4338 qemu>
<dsa 2018 4339 ceph>
<dsa 2018 4340 chromium-browser>
<dsa 2018 4342 chromium-browser>
<dsa 2018 4343 liblivemedia>
<dsa 2018 4344 roundcube>
<dsa 2018 4345 samba>
<dsa 2018 4346 ghostscript>
<dsa 2018 4347 perl>
<dsa 2018 4348 openssl>
<dsa 2018 4349 tiff>
<dsa 2018 4350 policykit-1>
<dsa 2018 4351 libphp-phpmailer>
<dsa 2018 4353 php7.0>
<dsa 2018 4354 firefox-esr>
<dsa 2018 4355 openssl1.0>
<dsa 2018 4356 netatalk>
<dsa 2018 4357 libapache-mod-jk>
<dsa 2018 4358 ruby-sanitize>
<dsa 2018 4359 wireshark>
<dsa 2018 4360 libarchive>
<dsa 2018 4361 libextractor>
<dsa 2019 4362 thunderbird>
<dsa 2019 4363 python-django>
<dsa 2019 4364 ruby-loofah>
<dsa 2019 4365 tmpreaper>
<dsa 2019 4366 vlc>
<dsa 2019 4367 systemd>
<dsa 2019 4368 zeromq3>
<dsa 2019 4369 xen>
<dsa 2019 4370 drupal7>
<dsa 2019 4372 ghostscript>
<dsa 2019 4375 spice>
<dsa 2019 4376 firefox-esr>
<dsa 2019 4377 rssh>
<dsa 2019 4378 php-pear>
<dsa 2019 4381 libreoffice>
<dsa 2019 4382 rssh>
<dsa 2019 4383 libvncserver>
<dsa 2019 4384 libgd2>
<dsa 2019 4386 curl>
<dsa 2019 4387 openssh>
</table>


<h2>Removed packages</h2>

<p>The following packages were removed due to circumstances beyond our control:</p>

<table border=0>
<tr><th>Package</th>               <th>Reason</th></tr>
<correction adblock-plus "Incompatible with newer firefox-esr versions">
<correction calendar-exchange-provider " incompatible with newer Thunderbird versions">
<correction cookie-monster "Incompatible with newer firefox-esr versions">
<correction corebird "Broken by Twitter API changes">
<correction debian-buttons "Incompatible with newer firefox-esr versions">
<correction debian-parl "Depends on broken / removed Firefox plugins">
<correction firefox-branding-iceweasel "Incompatible with newer firefox-esr versions">
<correction firefox-kwallet5 "Incompatible with newer firefox-esr versions">
<correction flashblock "Incompatible with newer firefox-esr versions">
<correction flickrbackup "Incompatible with current Flickr API">
<correction imap-acl-extension "Incompatible with newer firefox-esr versions">
<correction libwww-topica-perl "Useless due to Topica site closure">
<correction mozilla-dom-inspector "Incompatible with newer firefox-esr versions">
<correction mozilla-noscript "Incompatible with newer firefox-esr versions">
<correction mozilla-password-editor "Incompatible with newer firefox-esr versions">
<correction mozvoikko "Incompatible with newer firefox-esr versions">
<correction personaplus "Incompatible with newer firefox-esr versions">
<correction python-formalchemy "Unusable, fails to import in Python">
<correction refcontrol "Incompatible with newer firefox-esr versions">
<correction requestpolicy "Incompatible with newer firefox-esr versions">
<correction spice-xpi "Incompatible with newer firefox-esr versions">
<correction toggle-proxy "Incompatible with newer firefox-esr versions">
<correction y-u-no-validate "Incompatible with newer firefox-esr versions">

</table>

<h2>Debian Installer</h2>
<p>The installer has been updated to include the fixes incorporated
into stable by the point release.</p>

<h2>URLs</h2>

<p>The complete lists of packages that have changed with this revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>The current stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Proposed updates to the stable distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>stable distribution information (release notes, errata etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Security announcements and information:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely
free operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a>, send mail to
&lt;press@debian.org&gt;, or contact the stable release team at
&lt;debian-release@lists.debian.org&gt;.</p>
