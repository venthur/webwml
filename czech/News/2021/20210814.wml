<define-tag pagetitle>Debian 11 <q>Bullseye</q> vydán</define-tag>
<define-tag release_date>2021-08-14</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="eb179e1fd34e48a5b08dc775a8c1521aee2a6efa" maintainer="Miroslav Kuře"


<p>
Projekt Debian po 2 letech, 1 měsíci a 9 dnech vývoje s hrdostí
představuje novou stabilní verzi 11, kódovým názvem
<q>Bullseye</q>. Díky spojenému úsilí
týmů <a href="https://security-team.debian.org/">Debian Security</a>
a <a href="https://wiki.debian.org/LTS">Debian Long Term Support</a>
bude tato verze podporována 5 let.
</p>

<p>
Debian 11 <q>Bullseye</q> obsahuje nejrůznější desktopové aplikace a
desktopová prostředí, mezi jinými například:
</p>
<ul>
<li>Gnome 3.38,</li>
<li>KDE Plasma 5.20,</li>
<li>LXDE 11,</li>
<li>LXQt 0.16,</li>
<li>MATE 1.24,</li>
<li>Xfce 4.16.</li>
</ul>

<p>
Z celkového počtu 59 551 balíků v tomto vydání je 11 294 balíků zcela
nových, 9 519 balíků bylo odstraněno jako zastaralé, 42 821 balíků
bylo aktualizováno a 5 434 balíků zůstalo beze změny.
</p>

<p>
<q>Bullseye</q> je naše první vydání, které obsahuje linuxové jádro s
podporou pro souborový systém exFAT. To znamená, že již není potřeba
používat implementaci souborového systému v uživatelském prostoru,
kterou poskytoval balík exfat-fuse. Nástroje pro vytváření a kontrolu
souborových systémů exFAT se nachází v balíku exfatprogs.
</p>

<p>
Většina moderních tiskáren je schopno tisknout a scanovat bez nutnosti
použití specifického ovladače (často nesvobodného) od výrobce
zařízení.
<q>Bullseye</q> přináší balík ipp-usb, který používá neutrální
protokol IPP-přes-USB podporovaný mnoha moderními tiskárnami. To
umožňuje se k USB zařízení chovat jako k síťovému zařízení. Oficiální
SANE backend pro bezovladačová zařízení je poskytován přes sane-escl v
balíku libsane1, který používá protokol eSCL.
</p>

<p>
Systemd v <q>Bullseye</q> ve výchozím nastavení zapíná perzistentní
žurnál. To umožňuje uživatelům, kteří nevyužívají speciální vlastnosti
jiných logovacích daemonů tyto tradiční nástroje odinstalovat a přejít
na kompletní logování jen pomocí Systemd.
</p>

<p>
Tým Debian Med se podílí na boji proti COVID-19 balíčkováním softwaru
pro výzkum a sekvencování viru a pro boj s pandemií pomocí nástrojů
používaných v epidemiologii. Tato práce bude pokračovat se zaměřením
na nástroje pro strojové učení v obou oblastech. Práce týmu s nástroji
kontroly kvality a kontinuální integrace je kritická pro konzistentní
a reprodukovatelné výsledky vyžadované ve vědeckém výzkumu.

Varianta Debianu Debian Med obsahuje množství aplikací kriticky
závislých na výkonu, které nyní mohou těžit z projektu SIMD
Everywhere. Pro instalaci balíků spravovaných týmem Debian Med si
nainstalujte metabalíky nazvané med-*, které se momentálně nachází ve
verzi 3.6.x.
</p>

<p>
Čínština, japonština, korejština a mnoho dalších jazyků nyní mají
novou vstupní metodu Fcitx 5, která nahrazuje populární Fcitx 4
z <q>Busteru</q>. Tato nová verze má mnohem lepší podporu Waylandu.
</p>

<p>
Debian 11 <q>Bullseye</q> obsahuje množství aktualizovaných
softwarových balíků (přes 72% oproti předchozímu vydání), mezi kterými
nechybí třeba:
</p>
<ul>
<li>Apache 2.4.48</li>
<li>BIND DNS Server 9.16</li>
<li>Calligra 3.2</li>
<li>Cryptsetup 2.3</li>
<li>Emacs 27.1</li>
<li>GIMP 2.10.22</li>
<li>GNU Compiler Collection 10.2</li>
<li>GnuPG 2.2.20</li>
<li>Inkscape 1.0.2</li>
<li>LibreOffice 7.0</li>
<li>Linux řady 5.10</li>
<li>MariaDB 10.5</li>
<li>OpenSSH 8.4p1</li>
<li>Perl 5.32</li>
<li>PHP 7.4</li>
<li>PostgreSQL 13</li>
<li>Python 3, 3.9.1</li>
<li>Rustc 1.48</li>
<li>Samba 4.13</li>
<li>Vim 8.2</li>
<li>a více než 59 000 dalších softwarových balíků připravených k
okamžitému použití, sestavených z téměř 30 000 zdrojových balíků.</li>
</ul>

<p>
S tímto bohatým výběrem softwaru a tradiční podporou širokého spektra
počítačových architektur Debian opět potvrzuje svůj záměr být
univerzálním operačním systémem. Je vhodný pro nejrůznější způsoby
nasazení: od desktopů k netbookům, od vývojářských stanic ke klastrům
a databázovým, webovým či datovým serverům. Aby <q>Bullseye</q> splnil
vysoká očekávání, která uživatelé stabilního vydání předpokládají,
byly použity dodatečné způsoby zajištění kvality jako například
automatické testování instalací a aktualizací všech balíků v archivu.
</p>

<p>
Celkově je podporováno devět architektur:
64 bitové PC / Intel EM64T / x86-64 (<code>amd64</code>),
32 bitové PC / Intel IA-32 (<code>i386</code>),
64 bitové little-endian Motorola/IBM PowerPC (<code>ppc64el</code>),
64 bitové IBM S/390 (<code>s390x</code>),
ARM (<code>armel</code> resp. <code>armhf</code> pro starší resp.
novější 32 bitový hardware a <code>arm64</code> pro 64 bitovou
architekturu <q>AArch64</q>)
a MIPS (<code>mipsel</code> pro 32 bitový little-endian hardware
a <code>mips64el</code> pro 64 bitový little-endian hardware).
</p>


<h3>Chcete Debian vyzkoušet?</h3>

<p>
Chcete-li Debian 11 <q>Bullseye</q> vyzkoušet jednoduše bez nutnosti
cokoliv instalovat, můžete využít některý
z <a href="$(HOME)/CD/live/">živých (live) obrazů</a>, který zavede
celý operační systém do operační paměti počítače, kde si ho můžete
vyzkoušet bez obav z rozbití stávajícího systému.
</p>

<p>
Živé obrazy jsou k dispozici na architekturách <code>amd64</code>
a <code>i386</code> ve formě obrazů pro DVD, USB paměti a zavádění ze
sítě. Uživatel si může vyzkoušet různá desktopová prostředí: GNOME,
KDE Plasma, LXDE, LXQt, MATE a Xfce. Bullseye má i standardní živý
obraz základního Debianu bez grafického uživatelského rozhraní.
</p>

<p>
Jestliže se vám bude nový operační systém líbit, máte možnost si ho
nainstalovat na disk počítače přímo z živého obrazu. Živé obrazy
obsahují jak standardní instalační systém Debianu, tak i nezávislý
instalátor Calamares. Více informací naleznete
v <a href="$(HOME)/releases/bullseye/releasenotes">poznámkách k
vydání</a> a na stránkách Debianu v
části <a href="$(HOME)/CD/live/">živé obrazy</a>.
</p>

<p>
Jestliže chcete Debian rovnou nainstalovat, můžete si vybrat z
nejrůznějších instalačních médií, jako jsou Blu-ray, DVD, CD, USB
paměti, nebo třeba síť. Při instalaci je možné volit z několika
desktopových prostředí &mdash; Cinnamon, GNOME, KDE Plasma Desktop a
Aplikace, LXDE, LXQt, MATE nebo Xfce. Navíc jsou k
dispozici <q>vícearchitekturní</q> CD a DVD, která umožňují instalaci
na různých architekturách z jediného média. Také si třeba můžete
vytvořit instalační USB klíčenku (více se dozvíte
v <a href="$(HOME)/releases/bullseye/installmanual">instalační
příručce</a>).
</p>

<p>
V instalačním systému Debianu se událo mnohé, ale zmiňme hlavně lepší
podporu hardwaru.
</p>

<p>
V některých případech se stále může stát, že po úspěšné instalaci bude
mít nový systém problémy se zobrazováním - pro tyto případy existuje
<a
href="https://www.debian.org/releases/bullseye/amd64/ch06s04#completing-installed-system">několik
tipů</a>, které mohou pomoci s přihlášením. Tamtéž je zmíněna možnost
použít nástroj isenkram, který umí automaticky rozpoznat a
doinstalovat chybějící firmware, ale jeho použití je třeba zvážit,
protože to pravděpodobně bude znamenat instalaci balíků ze sekce
non-free.
</p>

<p>
Kromě toho byly vylepšeny i <a
href="https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/">obrazy
instalačního systému s nesvobodným firmwarem</a>, který nyní předvídá
nutnost použití firmware v instalovaném systému (např. firmware pro
grafické karty AMD nebo Nvidia, případně novější generace audio
hardware od Intelu).
</p>

<p>
Pro cloudové uživatele nabízí Debian přímou podporu mnoha známých
cloudových platforem. Oficiální obrazy s Debianem se dají jednoduše
zvolit z nabídky u konkrétních poskytovatelů. Debian také poskytuje
předpřipravené <code>amd64</code> a <code>arm64</code>
<a href="https://cloud.debian.org/images/openstack/current/">
obrazy pro OpenStack</a>, které lze použít pro lokální cloud.
</p>

<p>
Debian je nyní možné instalovat v 76 jazycích, z nichž většina funguje
jak v textovém, tak v grafickém rozhraní.
</p>

<p>
Obrazy instalačních médií si můžete již nyní stáhnout přes
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (doporučená možnost),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a> nebo
<a href="$(HOME)/CD/http-ftp/">HTTP</a> (vizte
<a href="$(HOME)/CD/">Debian na CD</a>). V brzké době
bude <q>Bullseye</q> dostupný i
u <a href="$(HOME)/CD/vendors">prodejců</a> na DVD, CD a Blu-ray
discích.
</p>


<h3>Aktualizace Debianu</h3>

<p>
Přechod na Debian 11 z předchozího vydání (Debian 10 <q>Buster</q>) je
ve většině případů řešen automaticky nástrojem pro správu balíků APT.
Aktualizace Debianu může být jako vždy provedena bezbolestně, na místě
a bez zbytečné odstávky systému, ovšem důrazně doporučujeme přečíst
si <a href="$(HOME)/releases/bullseye/releasenotes">poznámky k
vydání</a>
a <a href="$(HOME)/releases/bullseye/installmanual">instalační
příručku</a> a předejít tak případným problémům.
</p>

<p>
Například repozitář s bezpečnostními aktualizacemi se nyní jmenuje
bullseye-security a uživatelé by měli při aktualizaci příslušně
opravit své sources.list, jak popisuje <a
href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#security-archive">příslušná
kapitola poznámek k vydání</a>.
</p>

<p>
Aktualizujete-li vzdáleně, upozorňujeme na část <a
href="$(HOME)/releases/bullseye/amd64/release-notes/ch-information#ssh-not-available">Během
aktualizace není možné navazovat nová SSH spojení</a>.
</p>

<p>
V nejbližších týdnech po vydání se budou poznámky k vydání dále
vylepšovat a překládat do dalších jazyků.
</p>


<h2>O Debianu</h2>

<p>
Debian je svobodný operační systém vyvíjený tisícovkami dobrovolníků z
celého světa spolupracujících prostřednictvím Internetu. Hlavními
silnými stránkami projektu Debian jsou dobrovolnická základna,
dodržování společenské smlouvy Debianu (Debian Social Contract) a
odhodlání poskytovat nejlepší možný operační systém. Vydání nové verze
Debianu je v tomto směru dalším důležitým krokem.
</p>


<h2>Kontaktní informace</h2>

<p>
Pro další informace prosím navštivte webové stránky Debianu
na <a href="$(HOME)/">https://www.debian.org/</a> nebo zašlete email
na &lt;press@debian.org&gt;.
</p>
