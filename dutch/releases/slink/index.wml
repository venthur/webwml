#use wml::debian::template title="Informatie over Debian 2.1 (slink)" BARETITLE=yes
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/slink/formats_and_architectures.wmh"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="8da95139c3595d47371ba8d288784086ae2ebacd"

<:

$about_lang_setting = "If you have set your browser's localization
properly, you can use the above link to get the right HTML version
automatically -- see <A href=\"$base_url/intro/cn\">content negotiation</A>.
Otherwise, pick the exact architecture, language, and format you want
from the table below.\n";

 :>


<ul>
	<li><a href="#release-notes">Notities bij de release</a>
	<li><a href="#new-inst">Nieuwe installaties</a>
	<li><a href="#errata">Errata</a>
	<li><a href="#unofficial-updates">Niet-officiële updates</a>
	<li><a href="#acquiring">Debian 2.1 verkrijgen</a>
      </ul>

<p>

<strong>Debian 2.1 is vervangen.</strong>

<p>

Omdat er <a href="../">recentere releases</a> uitgebracht werden, is release 2.1
vervangen. Om historische redenen worden deze pagina's behouden.
U moet beseffen dat Debian 2.1 niet langer onderhouden wordt.

<p>

De volgende architecturen worden ondersteund in Debian 2.1:

<ul>
<: foreach $arch (@arches) {
      print "<li> " . $arches{$arch} . "\n";
   } :>
</ul>


<h2><a name="release-notes"></a>Notities bij de release</h2>

<p>

Raadpleeg de Notities bij de release voor uw architectuur om te weten wat nieuw
is in Debian 2.1. De Notities bij de release bevatten ook instructies voor
gebruikers die vanuit eerdere releases opwaarderen.

<ul>
<: &permute_as_list('release-notes/', 'Release Notes'); :>
</ul>

<p>
<: print $about_lang_setting; :>
</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architectuur</strong></th>
  <th align="left"><strong>Bestandsformaat</strong></th>
  <th align="left"><strong>Talen</strong></th>
</tr>
<: &permute_as_matrix('release-notes', 'english', 'croatian', 'czech',
		      'japanese', 'portuguese', 'russian');
:>
</table>
</div>

<p>

Voor de architectuur i386 bestaat een <a href="i386/reports">gedetailleerd
rapport</a> waarin beschreven wordt welke pakketten sinds de laatste twee
releases veranderd zijn.


<h2><a name="new-inst"></a>Nieuwe installaties</h2>

<p>

Installatie-instructies, samen met downloadbare bestanden, zijn onderverdeeld
volgens architectuur:
<ul>
<:= &permute_as_list('install', 'Install Manual'); :>
</ul>

<p>
<: print $about_lang_setting; :>
</p>


<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architectuur</strong></th>
  <th align="left"><strong>Bestandsformaat</strong></th>
  <th align="left"><strong>Talen</strong></th>
</tr>
<:= &permute_as_matrix('install', 'english', 'croatian', 'czech',
		      'finnish', 'french',
		      'japanese', 'portuguese', 'russian', 'spanish');
:>
</table>
</div>

Merk op dat ook Duitse documentatie beschikbaar is, maar enkel voor de architectuur i386; u kunt deze raadplegen als
<a href="i386/install.de.txt">ASCII</a>,
<a href="i386/install.de.pdf">PDF</a>, of
<a href="i386/install.de.html">HTML</a>.

<p>

Er is veel aandacht besteed aan de Debian-installatiehandleiding om
niet-i386-distributies te ondersteunen. Voor sommige architecturen (in het
bijzonder <:=$arches{'alpha'}:>) is nog wat werk te doen -- raadpleeg het
document zelf voor informatie over hoe u een bijdrage kunt leveren.

<p>

Deze webpagina's zullen bijgewerkt worden met recentere versies van de
Installatiehandleiding voor slink wanneer deze beschikbaar worden. Indien u
wilt bijdragen met aanpassingen, raadpleeg dan die sectie en de handleiding; u
kunt ook de <a href="source/">SGML-broncode</a> ophalen -- patches zijn de
beste manier om commentaar aan te leveren. Medewerkers en gebruikers die zich
afvragen wat exact nieuw is, zouden het
<a href="source/ChangeLog">ChangeLog</a>-bestand moeten raadplegen.


<h2><a name="errata"></a>Errata</h2>

<p>

Soms, in geval van kritieke problemen of beveiligingsupdates, wordt de
uitgebrachte distributie (Slink in dit geval) bijgewerkt. Over  het algemeen
worden deze aangeduid als tussenreleases. De huidige tussenrelease is Debian
2.1r5. Het
<a href="http://archive.debian.org/debian/dists/slink/ChangeLog">ChangeLog</a>-bestand vindt u op elke spiegelserver van het Debian archief.

<p>

Reparaties aan de vrijgegeven stabiele distributie ondergaan vaak een
uitgebreide testperiode voordat ze in het archief worden geaccepteerd. Deze
reparaties zijn echter beschikbaar in de map
<a href="http://archive.debian.org/debian/dists/slink-proposed-updates/">
dists/slink-proposed-updates</a> op elke spiegelserver van het Debian archief.
Indien u <tt>apt</tt> gebruikt om uw pakketten te updaten, kunt u de updates
uit <q>proposed updates</q> installeren door in het bestand
<tt>/etc/apt/sources.list</tt> de volgende regel toe te voegen:
<pre>
  deb http://archive.debian.org dists/slink-proposed-updates/
</pre>
Voer nadien de volgende commando's uit:
<kbd>apt-get update; apt-get upgrade</kbd>.

<p>

Slink is gecertificeerd voor gebruik met de 2.0.x-serie van de Linux kernel.
Indien u de Linux 2.2.x kernel wilt gebruiken met slink, raadpleeg dan de <a
href="running-kernel-2.2">lijst met bekende problemen</a>.


<h2><a name="unofficial-updates"></a>Niet-officiële updates</h2>

<p>

Uitgebrachte versies van Debian worden enkel bijgewerkt in het geval van
kritieke fouten of beveiligingsproblemen. Voor het gemak van de gebruiker is er
echter bepaalde bijgewerkte software die onofficieel beschikbaar wordt gesteld
door andere gebruikers en ontwikkelaars. Het materiaal uit deze sectie wordt
niet officieel ondersteund door Debian.


<h3>Geïnternationaliseerde opstartdiskettes</h3>

<p>

Niet-officiële Portugese opstartdiskettes zijn beschikbaar op ftp://ftp2.escelsanet.com.br/debian/.


<h3>Nooddiskette</h3>

<p>

Gebruikers van Adaptec 2940 SCSI-kaarten en andere SCSI-controllers met de
aic7xxx chipset zullen wellicht problemen ondervinden met de standaard
opstartdiskettes. Een vriendelijke gebruiker heeft enkele experimenten gedaan
waarmee de problemen van veel gebruikers opgelost kunnen worden. Er zijn twee
alternatieve nooddiskettes voor de i386-architectuur beschikbaar op
ftp://kalle.csb.ki.se/pub/. Op die locatie zijn ook vervangkernels te vinden,
welke u kunt gebruiken om de bestaande kernels op de opstartdiskettes gewoon te
vervangen. U heeft wel de diskette met stuurprogramma's uit de standaardlocatie
nodig.


<p>

Een alternatieve en nieuwere collectie nood- en stuurprogrammadiskettes voor
Adaptec-gebruikers is te vinden op https://www.debian.org/~adric/aic7xxx/.



<h3>Gnome</h3>

<p>

Indien u de meest recente versie van Gnome wenst te gebruiken in de stabiele
release, raadpleeg dan de informatie over de GNOME-update voor Debian 2.1
(http://www.gnome.org/start/debian-readme.html).


<h3>APT</h3>

<p>

Een bijgewerkte versie van <code>apt</code> is beschikbaar in Debian sinds
versie 2.1r3. Het voordeel van deze bijgewerkte versie is voornamelijk dat deze
in staat is zelf de installatie vanaf verschillende cd's af te handelen. Dit
maakt de detectieoptie <code>dpkg-multicd</code> uit <code>dselect</code>
overbodig. Uw 2.1-cd bevat mogelijk een oudere versie van <code>apt</code> en
dus wilt u misschien opwaarderen naar de versie die nu in Slink aanwezig is.



<h2><a name="acquiring"></a>Debian 2.1 verkrijgen</h2>

<p>

Debian is elektronisch verkrijgbaar of via cd-leveranciers.

<h3>Debian op cd aankopen</h3>

<p>

We houden een <a href="../../CD/vendors/">lijst van cd-verkopers</a> bij
welke cd's verkopen met Debian 2.1.


<h3>Debian downloaden van het internet</h3>

<p>

We houden een <a href="../../distrib/ftplist">lijst van sites</a> bij
welke de distributie spiegelen.


<!-- Keep this comment at the end of the file
Local variables:
mode: sgml
sgml-indent-data:nil
sgml-doctype:"../.doctype"
End:
-->
