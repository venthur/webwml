# translation of countries.po to Dutch
# Templates files for webwml modules
# Copyright (C) 2003,2004 Software in the Public Interest, Inc.
#
# Frans Pop <aragorn@tiscali.nl>, 2006.
# Frans Pop <elendil@planet.nl>, 2006, 2007, 2008, 2010.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2011, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: countries.nl.po\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2021-06-09 23:18+0200\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#: ../../english/template/debian/countries.wml:111
msgid "United Arab Emirates"
msgstr "Verenigde Arabische Emiraten"

#: ../../english/template/debian/countries.wml:114
msgid "Albania"
msgstr "Albanië"

#: ../../english/template/debian/countries.wml:117
msgid "Armenia"
msgstr "Armenië"

#: ../../english/template/debian/countries.wml:120
msgid "Argentina"
msgstr "Argentinië"

#: ../../english/template/debian/countries.wml:123
msgid "Austria"
msgstr "Oostenrijk"

#: ../../english/template/debian/countries.wml:126
msgid "Australia"
msgstr "Australië"

#: ../../english/template/debian/countries.wml:129
msgid "Bosnia and Herzegovina"
msgstr "Bosnië en Herzegovina"

#: ../../english/template/debian/countries.wml:132
msgid "Bangladesh"
msgstr "Bangladesh"

#: ../../english/template/debian/countries.wml:135
msgid "Belgium"
msgstr "België"

#: ../../english/template/debian/countries.wml:138
msgid "Bulgaria"
msgstr "Bulgarije"

#: ../../english/template/debian/countries.wml:141
msgid "Brazil"
msgstr "Brazilië"

#: ../../english/template/debian/countries.wml:144
msgid "Bahamas"
msgstr "Bahamas"

#: ../../english/template/debian/countries.wml:147
msgid "Belarus"
msgstr "Wit-Rusland"

#: ../../english/template/debian/countries.wml:150
msgid "Canada"
msgstr "Canada"

#: ../../english/template/debian/countries.wml:153
msgid "Switzerland"
msgstr "Zwitserland"

#: ../../english/template/debian/countries.wml:156
msgid "Chile"
msgstr "Chili"

#: ../../english/template/debian/countries.wml:159
msgid "China"
msgstr "China"

#: ../../english/template/debian/countries.wml:162
msgid "Colombia"
msgstr "Colombia"

#: ../../english/template/debian/countries.wml:165
msgid "Costa Rica"
msgstr "Costa Rica"

#: ../../english/template/debian/countries.wml:168
msgid "Czech Republic"
msgstr "Tjechië"

#: ../../english/template/debian/countries.wml:171
msgid "Germany"
msgstr "Duitsland"

#: ../../english/template/debian/countries.wml:174
msgid "Denmark"
msgstr "Denemarken"

#: ../../english/template/debian/countries.wml:177
msgid "Dominican Republic"
msgstr "Dominicaanse Republiek"

#: ../../english/template/debian/countries.wml:180
msgid "Algeria"
msgstr "Algerije"

#: ../../english/template/debian/countries.wml:183
msgid "Ecuador"
msgstr "Equador"

#: ../../english/template/debian/countries.wml:186
msgid "Estonia"
msgstr "Estland"

#: ../../english/template/debian/countries.wml:189
msgid "Egypt"
msgstr "Egypte"

#: ../../english/template/debian/countries.wml:192
msgid "Spain"
msgstr "Spanje"

#: ../../english/template/debian/countries.wml:195
msgid "Ethiopia"
msgstr "Ethiopië"

#: ../../english/template/debian/countries.wml:198
msgid "Finland"
msgstr "Finland"

#: ../../english/template/debian/countries.wml:201
msgid "Faroe Islands"
msgstr "Faeröereilanden"

#: ../../english/template/debian/countries.wml:204
msgid "France"
msgstr "Frankrijk"

#: ../../english/template/debian/countries.wml:207
msgid "United Kingdom"
msgstr "Verenigd Koninkrijk"

#: ../../english/template/debian/countries.wml:210
msgid "Grenada"
msgstr "Grenada"

#: ../../english/template/debian/countries.wml:213
msgid "Georgia"
msgstr "Georgië"

#: ../../english/template/debian/countries.wml:216
msgid "Greenland"
msgstr "Groenland"

#: ../../english/template/debian/countries.wml:219
msgid "Greece"
msgstr "Griekenland"

#: ../../english/template/debian/countries.wml:222
msgid "Guatemala"
msgstr "Guatemala"

#: ../../english/template/debian/countries.wml:225
msgid "Hong Kong"
msgstr "Hong Kong"

#: ../../english/template/debian/countries.wml:228
msgid "Honduras"
msgstr "Honduras"

#: ../../english/template/debian/countries.wml:231
msgid "Croatia"
msgstr "Kroatië"

#: ../../english/template/debian/countries.wml:234
msgid "Hungary"
msgstr "Hongarije"

#: ../../english/template/debian/countries.wml:237
msgid "Indonesia"
msgstr "Indonesië"

#: ../../english/template/debian/countries.wml:240
msgid "Iran"
msgstr "Iran"

#: ../../english/template/debian/countries.wml:243
msgid "Ireland"
msgstr "Ierland"

#: ../../english/template/debian/countries.wml:246
msgid "Israel"
msgstr "Israël"

#: ../../english/template/debian/countries.wml:249
msgid "India"
msgstr "India"

#: ../../english/template/debian/countries.wml:252
msgid "Iceland"
msgstr "IJsland"

#: ../../english/template/debian/countries.wml:255
msgid "Italy"
msgstr "Italië"

#: ../../english/template/debian/countries.wml:258
msgid "Jordan"
msgstr "Jordanië"

#: ../../english/template/debian/countries.wml:261
msgid "Japan"
msgstr "Japan"

#: ../../english/template/debian/countries.wml:264
msgid "Kenya"
msgstr "Kenia"

#: ../../english/template/debian/countries.wml:267
msgid "Kyrgyzstan"
msgstr "Kirgizië"

#: ../../english/template/debian/countries.wml:270
msgid "Cambodia"
msgstr "Cambodja"

#: ../../english/template/debian/countries.wml:273
msgid "Korea"
msgstr "Korea"

#: ../../english/template/debian/countries.wml:276
msgid "Kuwait"
msgstr "Koeweit"

#: ../../english/template/debian/countries.wml:279
msgid "Kazakhstan"
msgstr "Kazachstan"

#: ../../english/template/debian/countries.wml:282
msgid "Sri Lanka"
msgstr "Sri Lanka"

#: ../../english/template/debian/countries.wml:285
msgid "Lithuania"
msgstr "Litouwen"

#: ../../english/template/debian/countries.wml:288
msgid "Luxembourg"
msgstr "Luxemburg"

#: ../../english/template/debian/countries.wml:291
msgid "Latvia"
msgstr "Letland"

#: ../../english/template/debian/countries.wml:294
msgid "Morocco"
msgstr "Marokko"

#: ../../english/template/debian/countries.wml:297
msgid "Monaco"
msgstr "Monaco"

#: ../../english/template/debian/countries.wml:300
msgid "Moldova"
msgstr "Moldavië"

#: ../../english/template/debian/countries.wml:303
msgid "Montenegro"
msgstr "Montenegro"

#: ../../english/template/debian/countries.wml:306
msgid "Madagascar"
msgstr "Madagaskar"

#: ../../english/template/debian/countries.wml:309
msgid "Macedonia, Republic of"
msgstr "Macedonië"

#: ../../english/template/debian/countries.wml:312
msgid "Mongolia"
msgstr "Mongolië"

#: ../../english/template/debian/countries.wml:315
msgid "Malta"
msgstr "Malta"

#: ../../english/template/debian/countries.wml:318
msgid "Mexico"
msgstr "Mexico"

#: ../../english/template/debian/countries.wml:321
msgid "Malaysia"
msgstr "Maleisië"

#: ../../english/template/debian/countries.wml:324
msgid "New Caledonia"
msgstr "Nieuw Caledonië"

#: ../../english/template/debian/countries.wml:327
msgid "Nicaragua"
msgstr "Nicaragua"

#: ../../english/template/debian/countries.wml:330
msgid "Netherlands"
msgstr "Nederland"

#: ../../english/template/debian/countries.wml:333
msgid "Norway"
msgstr "Noorwegen"

#: ../../english/template/debian/countries.wml:336
msgid "New Zealand"
msgstr "Nieuw-Zeeland"

#: ../../english/template/debian/countries.wml:339
msgid "Panama"
msgstr "Panama"

#: ../../english/template/debian/countries.wml:342
msgid "Peru"
msgstr "Peru"

#: ../../english/template/debian/countries.wml:345
msgid "French Polynesia"
msgstr "Frans Polynesië"

#: ../../english/template/debian/countries.wml:348
msgid "Philippines"
msgstr "Filipijnen"

#: ../../english/template/debian/countries.wml:351
msgid "Pakistan"
msgstr "Pakistan"

#: ../../english/template/debian/countries.wml:354
msgid "Poland"
msgstr "Polen"

#: ../../english/template/debian/countries.wml:357
msgid "Portugal"
msgstr "Portugal"

#: ../../english/template/debian/countries.wml:360
msgid "Réunion"
msgstr "Réunion"

#: ../../english/template/debian/countries.wml:363
msgid "Romania"
msgstr "Roemenië"

#: ../../english/template/debian/countries.wml:366
msgid "Serbia"
msgstr "Servië"

#: ../../english/template/debian/countries.wml:369
msgid "Russia"
msgstr "Rusland"

#: ../../english/template/debian/countries.wml:372
msgid "Saudi Arabia"
msgstr "Saoedi-Arabië"

#: ../../english/template/debian/countries.wml:375
msgid "Sweden"
msgstr "Zweden"

#: ../../english/template/debian/countries.wml:378
msgid "Singapore"
msgstr "Singapore"

#: ../../english/template/debian/countries.wml:381
msgid "Slovenia"
msgstr "Slovenië"

#: ../../english/template/debian/countries.wml:384
msgid "Slovakia"
msgstr "Slovakije"

#: ../../english/template/debian/countries.wml:387
msgid "El Salvador"
msgstr "El Salvador"

#: ../../english/template/debian/countries.wml:390
msgid "Thailand"
msgstr "Thailand"

#: ../../english/template/debian/countries.wml:393
msgid "Tajikistan"
msgstr "Tadzjikistan"

#: ../../english/template/debian/countries.wml:396
msgid "Tunisia"
msgstr "Tunesië"

#: ../../english/template/debian/countries.wml:399
msgid "Turkey"
msgstr "Turkije"

#: ../../english/template/debian/countries.wml:402
msgid "Taiwan"
msgstr "Taiwan"

#: ../../english/template/debian/countries.wml:405
msgid "Ukraine"
msgstr "Oekraïne"

#: ../../english/template/debian/countries.wml:408
msgid "United States"
msgstr "Verenigde Staten"

#: ../../english/template/debian/countries.wml:411
msgid "Uruguay"
msgstr "Uruguay"

#: ../../english/template/debian/countries.wml:414
msgid "Uzbekistan"
msgstr "Oezbekistan"

#: ../../english/template/debian/countries.wml:417
msgid "Venezuela"
msgstr "Venezuela"

#: ../../english/template/debian/countries.wml:420
msgid "Vietnam"
msgstr "Vietnam"

#: ../../english/template/debian/countries.wml:423
msgid "Vanuatu"
msgstr "Vanuatu"

#: ../../english/template/debian/countries.wml:426
msgid "South Africa"
msgstr "Zuid-Afrika"

#: ../../english/template/debian/countries.wml:429
msgid "Zimbabwe"
msgstr "Zimbabwe"
