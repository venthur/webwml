msgid ""
msgstr ""
"Project-Id-Version: templates.po\n"
"PO-Revision-Date: 2013-05-08 11:30+0900\n"
"Last-Translator: Nobuhiro IMAI <nov@yo.rim.or.jp>\n"
"Language-Team: Japanese <debian-doc@debian.or.jp>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:13
msgid "      Key fingerprint"
msgstr "                 指紋"

#: ../../english/devel/debian-installer/images.data:87
msgid "ISO images"
msgstr "ISO イメージ"

#: ../../english/devel/debian-installer/images.data:88
msgid "Jigdo files"
msgstr "Jigdo ファイル"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "・"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />FAQ"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Jigdo を使ったダウンロード"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "HTTP/FTP 経由のダウンロード"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "CD/DVD を購入"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "ネットワークインストール"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />ダウンロード"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />その他"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />アートワーク"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />ミラーリング"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />rsync ミラー"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />検証"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Torrent でダウンロード"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "Debian CD チーム"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr "Debian の CD"

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />FAQ"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr "http/ftp"

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "購入"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr "ネットインストール"

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "<void id=\"misc-bottom\" />その他"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"英語での CD/DVD 用<a href=\"/MailingLists/disclaimer\">公開メーリングリスト</"
"a>:"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />イメージのリリース情報"
