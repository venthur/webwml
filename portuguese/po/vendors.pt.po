# Brazilian Portuguese translation for Debian website vendors.pot
# Copyright (C) 2003-2015 Software in the Public Interest, Inc.
#
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007.
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-01-31 17:10-0300\n"
"Last-Translator: Thiago Pezzo (tico) <pezzo@protonmail.com>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Fornecedor(a)"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Permitem contribuições"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arquiteturas"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Envio internacional"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Contato"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Página web do(a) fornecedor(a)"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "página"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "e-mail"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "na Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "Para algumas áreas"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "fonte"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "e"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Permitem Contribuição para o Debian:"

#~ msgid "Architectures:"
#~ msgstr "Arquiteturas:"

#~ msgid "BD Type:"
#~ msgstr "Tipo de BD:"

#~ msgid "CD Type:"
#~ msgstr "Tipo de CD:"

#~ msgid "Country:"
#~ msgstr "País:"

#~ msgid "Custom Release"
#~ msgstr "Versão customizada"

#~ msgid "DVD Type:"
#~ msgstr "Tipo de DVD:"

#~ msgid "Development Snapshot"
#~ msgstr "Versão de desenvolvimento"

#~ msgid "Multiple Distribution"
#~ msgstr "Distribuição múltipla"

#~ msgid "Official CD"
#~ msgstr "CD Oficial"

#~ msgid "Official DVD"
#~ msgstr "DVD Oficial"

#~ msgid "Ship International:"
#~ msgstr "Entrega internacional:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL da Página Debian:"

#~ msgid "USB Type:"
#~ msgstr "Tipo de USB:"

#~ msgid "Vendor Release"
#~ msgstr "Versão do Vendedor"

#~ msgid "Vendor:"
#~ msgstr "Vendedor:"

#~ msgid "contrib included"
#~ msgstr "contrib incluído"

#~ msgid "email:"
#~ msgstr "e-mail:"

#~ msgid "non-US included"
#~ msgstr "non-US incluído"

#~ msgid "non-free included"
#~ msgstr "non-free incluído"

#~ msgid "reseller"
#~ msgstr "revendedor"

#~ msgid "reseller of $var"
#~ msgstr "revendedor de $var"

#~ msgid "updated monthly"
#~ msgstr "atualizado mensalmente"

#~ msgid "updated twice weekly"
#~ msgstr "atualizado duas vezes por semana"

#~ msgid "updated weekly"
#~ msgstr "atualização semanal"

#~ msgid "vendor additions"
#~ msgstr "adições do vendedor"
