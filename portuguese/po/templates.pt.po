# Brazilian Portuguese translation for Debian website templates.pot
# Copyright (C) 2002-2022 Software in the Public Interest, Inc.
#
# Philipe Gaspar <philipegaspar@terra.com.br>, 2002
# Michelle Ribeiro <michelle@cipsga.org.br>, 2003
# Gustavo R. Montesino <grmontesino@ig.com.br>, 2004
# Felipe Augusto van de Wiel (faw) <faw@debian.org>, 2006-2007
# Marcelo Gomes de Santana <marcelo@msantana.eng.br>, 2011-2017.
# Thiago Pezzo (tico) <pezzo@protonmail.com>, 2022
# Paulo Henrique de Lima Santana <phls@debian.org>, 2022
#
msgid ""
msgstr ""
"Project-Id-Version: Debian Webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-05-13 17:35-0300\n"
"Last-Translator: Paulo Henrique de Lima Santana <phls@debian.org>\n"
"Language-Team: Brazilian Portuguese <debian-l10n-portuguese@lists.debian."
"org>\n"
"Language: pt_BR\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Site web do Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Pesquisar o site web do Debian."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Pesquisa do site web do Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Sim"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Não"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Projeto Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian é um sistema operacional e uma distribuição de Software Livre. É "
"mantida e atualizada através do trabalho de muitos(as) usuários(as) que doam "
"seu tempo e esforço voluntariamente."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, código aberto, livre, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr ""
"Voltar para a <a href=\"m4_HOME/\">página principal do Projeto Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Página Principal"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Pular \"Quicknav\""

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Sobre"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Sobre o Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Contate-nos"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "Informações legais"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "Privacidade de dados"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Doações"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Eventos"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Notícias"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Distribuição"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Suporte"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Pure Blends"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Canto dos(as) desenvolvedores(as)"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Documentação"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Informações sobre Segurança"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Busca"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "nenhum"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Ir"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "mundialmente"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Mapa do site"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Miscelânea"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Obtendo o Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "O Blog do Debian"

#: ../../english/template/debian/common_translation.wml:94
msgid "Debian Micronews"
msgstr "Debian Micronews"

#: ../../english/template/debian/common_translation.wml:97
msgid "Debian Planet"
msgstr "Debian Planet"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "Última modificação"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Por favor, envie seus comentários, críticas e sugestões sobre estas páginas "
"para a nossa <a href=\"mailto:debian-doc@lists.debian.org\">lista de "
"discussão</a>."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "não necessário"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "não disponível"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/A"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "na versão 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "na versão 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "na versão 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "na versão 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "na versão 2.2"

#: ../../english/template/debian/footer.wml:84
msgid ""
"See our <a href=\"m4_HOME/contact\">contact page</a> to get in touch. Web "
"site source code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">available</a>."
msgstr ""
"Veja nossa <a href=\"m4_HOME/contact\">página de contato</a> para "
"falar conosco. O código-fonte do site web está <a href=\"https://salsa."
"debian.org/webmaster-team/webwml\">disponível</a>."

#: ../../english/template/debian/footer.wml:87
msgid "Last Modified"
msgstr "Última Modificação"

#: ../../english/template/debian/footer.wml:90
msgid "Last Built"
msgstr "Última compilação"

#: ../../english/template/debian/footer.wml:93
msgid "Copyright"
msgstr "Copyright"

#: ../../english/template/debian/footer.wml:96
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> e outros;"

#: ../../english/template/debian/footer.wml:99
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Veja os <a href=\"m4_HOME/license\" rel=\"copyright\">termos de licença</a>"

#: ../../english/template/debian/footer.wml:102
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian é uma <a href=\"m4_HOME/trademark\">marca registrada</a> da Software "
"in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Esta página também está disponível nos seguintes idiomas:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr ""
"Como configurar <a href=m4_HOME/intro/cn>o idioma padrão dos documentos</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr "Navegador padrão"

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr "Cancelar a definição do cookie de substituição de idioma"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian Internacional"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Parcerias"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Notícias Semanais Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Notícias Semanais"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Notícias do Projeto Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Notícias do Projeto"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Informações sobre Versões"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Pacotes do Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Baixar"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;em&nbsp;CDs"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Livros sobre Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Wiki do Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Arquivos das Listas de Discussão"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Listas de Discussão"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Contrato Social"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Código de Conduta"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - O sistema operacional universal"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Mapa do site para as páginas web do Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Banco de dados dos(as) desenvolvedores(as)"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "FAQ do Debian"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Manual de Políticas Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Referência dos(as) desenvolvedores(as)"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Guia para Novos(as) Mantenedores(as)"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Bugs Críticos para o Lançamento"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Relatórios do Lintian"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Arquivos de listas de discussão de usuários(as)"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Arquivos das listas de discussão dos(as) desenvolvedores(as)"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Arquivos das listas de discussão de i18n/l10n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Arquivos das listas de discussão de portes"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Arquivos das listas de discussão do sistema de acompanhamento de bugs"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Arquivos das listas de discussão variadas"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Software Livre"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Desenvolvimento"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Ajude o Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Relatórios de Bug"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Portes/Arquiteturas"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Manual de Instalação"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Vendedores(as) de CDs"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Imagens de CD/USB ISO"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Instalação via Rede"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Pré-instalado"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Projeto Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Salsa &ndash; Debian Gitlab"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Garantia de Qualidade"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Sistema de Acompanhamento de Pacotes"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Visão geral dos pacotes do(a) desenvolvedor(a) Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Página principal do Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Sem itens neste ano."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "proposto"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "em discussão"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "votação aberta"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "terminado"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "retirado"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Eventos futuros"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Eventos passados"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(nova revisão)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Relatório"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "Página redirecionada para <newpage/>"

#: ../../english/template/debian/redirect.wml:14
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""
"Esta página foi renomeada para <url <newpage/>>, por favor atualize seus "
"links."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s para %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Nota:</em> O <a href=\"$link\">documento original</a> é mais novo que "
"esta tradução."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Alerta! Esta tradução está muito desatualizada, por favor leia a <a href="
"\"$link\">original</a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Nota:</em> O documento original desta tradução não existe mais."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "Versão errada da tradução!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Voltar para <a href=\"../\">Quem está usando o Debian?</a>."

#~ msgid "%s  &ndash; %s, Version %s: %s"
#~ msgstr "%s &ndash; %s, Versão %s: %s"

#~ msgid "%s  &ndash; %s: %s"
#~ msgstr "%s &ndash; %s: %s"

#~ msgid "%s days in adoption."
#~ msgstr "%s dias em adoção."

#~ msgid "%s days in preparation."
#~ msgstr "%s dias em preparação."

#~ msgid "&middot;"
#~ msgstr "&middot;"

#~ msgid "<a href=\"../../\">Back issues</a> of this newsletter are available."
#~ msgstr ""
#~ "<a href=\"../../\">Edições anteriores</a> deste boletim estão disponíveis."

#~ msgid "<get-var url /> (dead link)"
#~ msgstr "<get-var url /> (link quebrado)"

#~ msgid "<th>Project</th><th>Coordinator</th>"
#~ msgstr "<th>Coordenador do</th><th>Projeto</th>"

#~ msgid "<void id=\"dc_artwork\" />Artwork"
#~ msgstr "<void id=\"dc_artwork\" />Arte gráfica"

#~ msgid "<void id=\"dc_download\" />Download"
#~ msgstr "<void id=\"dc_download\" />Baixar"

#~ msgid "<void id=\"dc_mirroring\" />Mirroring"
#~ msgstr "<void id=\"dc_mirroring\" />Espelhando"

#~ msgid "<void id=\"dc_misc\" />Misc"
#~ msgstr "<void id=\"dc_misc\" />Misc"

#~ msgid "<void id=\"dc_pik\" />Pseudo Image Kit"
#~ msgstr "<void id=\"dc_pik\" />Kit de Pseudo Imagens"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Informações sobre o Lançamento da Imagem"

#~ msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
#~ msgstr "<void id=\"dc_rsyncmirrors\" />Espelhos Rsync"

#~ msgid "<void id=\"dc_torrent\" />Download with Torrent"
#~ msgstr "<void id=\"dc_torrent\" />Baixar com Torrent"

#~ msgid "<void id=\"faq-bottom\" />faq"
#~ msgstr "<void id=\"faq-bottom\" />faq"

#~ msgid "<void id=\"misc-bottom\" />misc"
#~ msgstr "<void id=\"misc-bottom\" />misc"

#~ msgid ""
#~ "<void id=\"plural\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />As Notícias do Projeto Debian são editadas por <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />As Notícias Semanais Debian são editadas por <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"plural\" />It was translated by %s."
#~ msgstr "<void id=\"plural\" />Ela foi traduzida por %s."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Project News was edited by <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Esta edição das Notícias do Projeto Debian foi "
#~ "editada por <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"plural\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"plural\" />Esta edição das Notícias Semanais Debian foi "
#~ "editada por <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"pluralfemale\" />It was translated by %s."
#~ msgstr "<void id=\"pluralfemale\" />Ela foi traduzida por %s."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Project News is edited by <a href=\"mailto:"
#~ "debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />As Notícias do Projeto Debian são editadas por <a "
#~ "href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />Debian Weekly News is edited by <a href=\"mailto:"
#~ "dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />As Notícias Semanais Debian são editadas por <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singular\" />It was translated by %s."
#~ msgstr "<void id=\"singular\" />Ela foi traduzida por %s."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Project News was edited by "
#~ "<a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Esta edição das Notícias do Projeto Debian foi "
#~ "editada por <a href=\"mailto:debian-publicity@lists.debian.org\">%s</a>."

#~ msgid ""
#~ "<void id=\"singular\" />This issue of Debian Weekly News was edited by <a "
#~ "href=\"mailto:dwn@debian.org\">%s</a>."
#~ msgstr ""
#~ "<void id=\"singular\" />Esta edição das Notícias Semanais Debian foi "
#~ "editada por <a href=\"mailto:dwn@debian.org\">%s</a>."

#~ msgid "<void id=\"singularfemale\" />It was translated by %s."
#~ msgstr "<void id=\"singularfemale\" />Ela foi traduzida por %s."

#~ msgid "Amend&nbsp;a&nbsp;Proposal"
#~ msgstr "Corrigir&nbsp;uma&nbsp;Proposta"

#~ msgid "Amendment Proposer"
#~ msgstr "Proponente da Emenda"

#~ msgid "Amendment Proposer A"
#~ msgstr "Proponente da Emenda A"

#~ msgid "Amendment Proposer B"
#~ msgstr "Proponente da Emenda B"

#~ msgid "Amendment Seconds"
#~ msgstr "Padrinhos da Emenda"

#~ msgid "Amendment Seconds A"
#~ msgstr "Padrinhos da Emenda A"

#~ msgid "Amendment Seconds B"
#~ msgstr "Padrinhos da Emenda B"

#~ msgid "Amendment Text"
#~ msgstr "Texto da Emenda"

#~ msgid "Amendment Text A"
#~ msgstr "Texto da Emenda A"

#~ msgid "Amendment Text B"
#~ msgstr "Texto da Emenda B"

#~ msgid "Amendments"
#~ msgstr "Emendas"

#~ msgid "Back to other <a href=\"./\">Debian news</a>."
#~ msgstr "Voltar para outras <a href=\"./\">notícias Debian</a>."

#~ msgid "Back to the <a href=\"./\">Debian consultants page</a>."
#~ msgstr "Voltar para a <a href=\"./\">página de consultores do Debian</a>."

#~ msgid "Back to the <a href=\"./\">Debian speakers page</a>."
#~ msgstr "Voltar para a <a href=\"./\">página de palestrantes do Debian</a>."

#~ msgid ""
#~ "Back to: other <a href=\"./\">Debian news</a> || <a href=\"m4_HOME/"
#~ "\">Debian Project homepage</a>."
#~ msgstr ""
#~ "Voltar para: outras <a href=\"./\">notícias Debian</a> || <a href="
#~ "\"m4_HOME/\">página inicial do Projeto Debian</a>."

#~ msgid "Ballot"
#~ msgstr "Cédula"

#~ msgid "Buy CDs or DVDs"
#~ msgstr "Comprar CDs ou DVDs"

#~ msgid "Choices"
#~ msgstr "Opções"

#~ msgid "DFSG"
#~ msgstr "DFSG"

#~ msgid "DFSG FAQ"
#~ msgstr "DFSG FAQ"

#~ msgid "DLS Index"
#~ msgstr "Índice DLS"

#~ msgid "Data and Statistics"
#~ msgstr "Dados e Estatísticas"

#~ msgid "Date"
#~ msgstr "Data"

#~ msgid "Date published"
#~ msgstr "Data de publicação"

#~ msgid "Debate"
#~ msgstr "Debate"

#~ msgid "Debian CD team"
#~ msgstr "Equipe Debian CD"

#~ msgid "Debian Involvement"
#~ msgstr "Envolvimento do Debian"

#~ msgid "Debian-Legal Archive"
#~ msgstr "Arquivo Debian-Legal"

#~ msgid "Decided"
#~ msgstr "Decidido"

#~ msgid "Discussion"
#~ msgstr "Discussão"

#~ msgid "Download calendar entry"
#~ msgstr "Baixar entrada no calendário"

#~ msgid "Download via HTTP/FTP"
#~ msgstr "Baixar via HTTP/FTP"

#~ msgid "Download with Jigdo"
#~ msgstr "Baixar com Jigdo"

#~ msgid ""
#~ "English-language <a href=\"/MailingLists/disclaimer\">public mailing "
#~ "list</a> for CDs/DVDs:"
#~ msgstr ""
#~ "<a href=\"/MailingLists/disclaimer\">Lista de discussão pública</a> em "
#~ "inglês para CDs/DVDs:"

#~ msgid "Follow&nbsp;a&nbsp;Proposal"
#~ msgstr "Apoiar&nbsp;uma&nbsp;Proposta"

#~ msgid "Forum"
#~ msgstr "Fórum"

#~ msgid "Free"
#~ msgstr "Livre"

#~ msgid "Home&nbsp;Vote&nbsp;Page"
#~ msgstr "Página&nbsp;principal&nbsp;de&nbsp;Votação"

#~ msgid "How&nbsp;To"
#~ msgstr "Como Fazer"

#~ msgid "In&nbsp;Discussion"
#~ msgstr "Em&nbsp;Discussão"

#~ msgid "Justification"
#~ msgstr "Justificativa"

#~ msgid "Latest News"
#~ msgstr "Últimas Notícias"

#~ msgid "License"
#~ msgstr "Licença"

#~ msgid "License Information"
#~ msgstr "Informações de Licença"

#~ msgid "License text"
#~ msgstr "Texto da Licença"

#~ msgid "License text (translated)"
#~ msgstr "Texto da Licença (traduzido)"

#~ msgid "List of Consultants"
#~ msgstr "Lista de Consultores"

#~ msgid "List of Speakers"
#~ msgstr "Lista de Palestrantes"

#~ msgid "Main Coordinator"
#~ msgstr "Coordenador Principal"

#~ msgid "Majority Requirement"
#~ msgstr "Maioria Requerida"

#~ msgid "Minimum Discussion"
#~ msgstr "Discussão Mínima"

#~ msgid "More Info"
#~ msgstr "Mais Informações"

#~ msgid "More information"
#~ msgstr "Informações adicionais"

#~ msgid "More information:"
#~ msgstr "Mais informações:"

#~ msgid "Network Install"
#~ msgstr "Instalação via Rede"

#~ msgid "No Requested packages"
#~ msgstr "Nenhum pacote requisitado"

#~ msgid "No help requested"
#~ msgstr "Nenhuma ajuda requisitada"

#~ msgid "No orphaned packages"
#~ msgstr "Nenhum pacote órfão"

#~ msgid "No packages waiting to be adopted"
#~ msgstr "Nenhum pacote esperando para ser adotado"

#~ msgid "No packages waiting to be packaged"
#~ msgstr "Nenhum pacote esperando para ser empacotado"

#~ msgid "No requests for adoption"
#~ msgstr "Nenhuma requisição para adoção"

#~ msgid "Nobody"
#~ msgstr "Ninguém"

#~ msgid "Nominations"
#~ msgstr "Candidaturas"

#~ msgid "Non-Free"
#~ msgstr "Não-Livre "

#~ msgid "Not Redistributable"
#~ msgstr "Não Redistribuível"

#~ msgid "Opposition"
#~ msgstr "Oposição"

#~ msgid "Original Summary"
#~ msgstr "Sumário Original"

#~ msgid "Other"
#~ msgstr "Outro"

#~ msgid "Outcome"
#~ msgstr "Resultado"

#~ msgid "Platforms"
#~ msgstr "Plataformas"

#~ msgid "Proceedings"
#~ msgstr "Encaminhamentos"

#~ msgid "Project&nbsp;News"
#~ msgstr "Notícias&nbsp;do&nbsp;Projeto"

#~ msgid "Proposal A"
#~ msgstr "Proposta A"

#~ msgid "Proposal A Proposer"
#~ msgstr "Proponente da Proposta A"

#~ msgid "Proposal A Seconds"
#~ msgstr "Padrinhos da Proposta A"

#~ msgid "Proposal B"
#~ msgstr "Proposta B"

#~ msgid "Proposal B Proposer"
#~ msgstr "Proponente da Proposta B"

#~ msgid "Proposal B Seconds"
#~ msgstr "Padrinhos da Proposta B"

#~ msgid "Proposal C"
#~ msgstr "Proposta C"

#~ msgid "Proposal C Proposer"
#~ msgstr "Proponente da Proposta C"

#~ msgid "Proposal C Seconds"
#~ msgstr "Padrinhos da Proposta C"

#~ msgid "Proposal D"
#~ msgstr "Proposta D"

#~ msgid "Proposal D Proposer"
#~ msgstr "Proponente da Proposta D"

#~ msgid "Proposal D Seconds"
#~ msgstr "Padrinhos da Proposta D"

#~ msgid "Proposal E"
#~ msgstr "Proposta E"

#~ msgid "Proposal E Proposer"
#~ msgstr "Proponente da Proposta E"

#~ msgid "Proposal E Seconds"
#~ msgstr "Padrinhos da Proposta E"

#~ msgid "Proposal F"
#~ msgstr "Proposta F"

#~ msgid "Proposal F Proposer"
#~ msgstr "Proponente da Proposta F"

#~ msgid "Proposal F Seconds"
#~ msgstr "Padrinhos da Proposta F"

#~ msgid "Proposer"
#~ msgstr "Proponente"

#~ msgid "Quorum"
#~ msgstr "Quorum"

#~ msgid "Rating:"
#~ msgstr "Classificação:"

#~ msgid "Read&nbsp;a&nbsp;Result"
#~ msgstr "Ler&nbsp;um&nbsp;Resultado"

#~ msgid "Related Links"
#~ msgstr "Links Relacionados"

#~ msgid "Seconds"
#~ msgstr "Padrinhos"

#~ msgid ""
#~ "See the <a href=\"./\">license information</a> page for an overview of "
#~ "the Debian License Summaries (DLS)."
#~ msgstr ""
#~ "Veja a página de <a href=\"./\">informação sobre licenças</a> para uma "
#~ "visão geral dos Sumários de Licença Debian (DLS)."

#~ msgid "Select a server near you"
#~ msgstr "Escolha um servidor próximo de você"

#~ msgid "Submit&nbsp;a&nbsp;Proposal"
#~ msgstr "Enviar&nbsp;Proposta"

#~ msgid "Summary"
#~ msgstr "Sumário"

#~ msgid "Taken by:"
#~ msgstr "Tirado por:"

#~ msgid "Text"
#~ msgstr "Texto"

#~ msgid ""
#~ "The original summary by <summary-author/> can be found in the <a href="
#~ "\"<summary-url/>\">list archives</a>."
#~ msgstr ""
#~ "O sumário original por <summary-author/> pode ser encontrado nos <a href="
#~ "\"<summary-url/>\">arquivos da lista</a>."

#~ msgid "This summary was prepared by <summary-author/>."
#~ msgstr "Este sumário foi preparado por <summary-author/>."

#~ msgid "Time Line"
#~ msgstr "Linha do Tempo"

#~ msgid ""
#~ "To receive this newsletter bi-weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Para receber este boletim quinzenalmente em sua caixa postal, <a href="
#~ "\"https://lists.debian.org/debian-news-portuguese/\">inscreva-se na lista "
#~ "debian-news-portuguese</a> (versão em Português Brasileiro). Para receber "
#~ "o boletim em inglês, <a href=\"https://lists.debian.org/debian-news/"
#~ "\">inscreva-se na lista debian-news</a>."

#~ msgid ""
#~ "To receive this newsletter weekly in your mailbox, <a href=\"https://"
#~ "lists.debian.org/debian-news/\">subscribe to the debian-news mailing "
#~ "list</a>."
#~ msgstr ""
#~ "Para receber este boletim semanalmente em sua caixa postal, <a href="
#~ "\"https://lists.debian.org/debian-news-portuguese/\">inscreva-se na lista "
#~ "debian-news-portuguese</a> (versão em Português Brasileiro). Para receber "
#~ "o boletim em inglês, <a href=\"https://lists.debian.org/debian-news/"
#~ "\">inscreva-se na lista debian-news</a>."

#~ msgid ""
#~ "To report a problem with the web site, please e-mail our publicly "
#~ "archived mailing list <a href=\"mailto:debian-www@lists.debian.org"
#~ "\">debian-www@lists.debian.org</a> in English.  For other contact "
#~ "information, see the Debian <a href=\"m4_HOME/contact\">contact page</a>. "
#~ "Web site source code is <a href=\"https://salsa.debian.org/webmaster-team/"
#~ "webwml\">available</a>."
#~ msgstr ""
#~ "Para relatar um problema com o site web, por favor envie uma mensagem em "
#~ "inglês para nossa lista de discussão arquivada publicamente <a href="
#~ "\"mailto:debian-www@lists.debian.org\">debian-www@lists.debian.org</a> ou "
#~ "em português para <a href=\"mailto:debian-l10n-portuguese@lists.debian.org"
#~ "\">debian-l10n-portuguese@lists.debian.org</a>. Para outras informações "
#~ "de contato, veja a <a href=\"m4_HOME/contact\">página de contato</a> do "
#~ "Debian. O código-fonte do site web está <a href=\"m4_HOME/devel/website/"
#~ "using_cvs\">disponível</a>."

#~ msgid "Upcoming Attractions"
#~ msgstr "Próximas Atrações"

#~ msgid "Version"
#~ msgstr "Versão"

#~ msgid "Visit the site sponsor"
#~ msgstr "Visite o patrocinador do site"

#~ msgid "Vote"
#~ msgstr "Votar"

#~ msgid "Voting&nbsp;Open"
#~ msgstr "Votação&nbsp;Aberta"

#~ msgid "Waiting&nbsp;for&nbsp;Sponsors"
#~ msgstr "Aguardando&nbsp;Patrocinadores"

#~ msgid "Weekly&nbsp;News"
#~ msgstr "Notícias&nbsp;semanais"

#~ msgid "When"
#~ msgstr "Quando"

#~ msgid "Where"
#~ msgstr "Onde"

#~ msgid "Withdrawn"
#~ msgstr "Retirado"

#~ msgid "buy"
#~ msgstr "compre"

#~ msgid "buy pre-made images"
#~ msgstr "compre imagens pré-feitas"

#~ msgid "debian_on_cd"
#~ msgstr "debian_em_cd"

#~ msgid "discussed"
#~ msgstr "discutido"

#~ msgid "free"
#~ msgstr "livre"

#~ msgid "http_ftp"
#~ msgstr "http_ftp"

#~ msgid "in adoption since today."
#~ msgstr "em adoção desde hoje."

#~ msgid "in adoption since yesterday."
#~ msgstr "em adoção desde ontem."

#~ msgid "in preparation since today."
#~ msgstr "em preparação desde hoje."

#~ msgid "in preparation since yesterday."
#~ msgstr "em preparação desde ontem."

#~ msgid "jigdo"
#~ msgstr "jigdo"

#~ msgid "link may no longer be valid"
#~ msgstr "o link pode não ser mais válido"

#~ msgid "net_install"
#~ msgstr "net_install"

#~ msgid "network install"
#~ msgstr "instalação via rede"

#~ msgid "non-free"
#~ msgstr "não livre"

#~ msgid "not redistributable"
#~ msgstr "não-redistribuível"

#~ msgid "package info"
#~ msgstr "informações do pacote"

#~ msgid "requested %s days ago."
#~ msgstr "requisitado a %s dias."

#~ msgid "requested today."
#~ msgstr "requisitado hoje."

#~ msgid "requested yesterday."
#~ msgstr "requisitado ontem."
