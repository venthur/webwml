# translation of organization.ru.po to Russian
# Nick Toris <nicktoris@gmail.com>, 2007.
# Yuri Kozlov <kozlov.y@gmail.com>, 2008.
# Yuri Kozlov <yuray@komyakino.ru>, 2009, 2011, 2013.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2020-12-28 22:29+0500\n"
"Last-Translator: Lev Lamberov <dogsleg@debian.org>\n"
"Language-Team: Russian <>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n"
"%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "сообщение о делегировании"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "сообщение о назначении"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\\\"male\\\"/>делегат"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\\\"female\\\"/>делегат"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"he_him\"/>он/его"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"she_her\"/>она/её"

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"gender_neutral\"/>делегат"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"they_them\"/>они/их"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "действующий"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "член"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "менеджер"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Управление выпусками"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "мастер"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr "председатель"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "помощник"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "секретарь"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr "представитель"

#: ../../english/intro/organization.data:54
msgid "role"
msgstr "роль"

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""
"В списке ниже обозначение <q>текущий</q> используется для позиций, "
"являющихся\n"
"временными (выборными или назначаемыми на определённый срок)."

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Руководство"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Дистрибутив"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Информационное взаимодействие и социально-ориентированные программы"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr "Команда защиты данных"

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Связи с общественностью"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr "Членство в других организациях"

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Поддержка и инфраструктура"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Лидер"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Технический Комитет"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Секретарь"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Проекты"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP-архив"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "Менеджер FTP-архива"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "Помощники менеджера FTP-архива"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "Мастера настройки FTP"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Backports"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Команда Backports"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Управление выпусками"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Команда подготовки выпуска"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Контроль качества"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Команда создания системы установки"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr "Команда Debian Live"

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Информация о выпусках"

#: ../../english/intro/organization.data:152
msgid "CD/DVD/USB Images"
msgstr "Образы CD/DVD/USB"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Производство"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Тестирование"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr "Поддержка облачных технологий"

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Инфраструктура автоматической сборки"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Команда поддержки wanna-build"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Управление buildd"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Документация"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Список нуждающихся в доработке и будущих пакетов"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Связи с прессой"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Web-страницы"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Планета Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "Агитационная компания"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Женщины в проекте Debian"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr "Сообщество"

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""
"Для отправки приватного сообщения всем участникам команды сообщества, "
"используйте GPG-ключ <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "События"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "Комитет DebConf"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Партнёрская программа"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Координация пожертвования аппаратным обеспечением"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr "GNOME Foundation"

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr "Linux Professional Institute"

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr "Linux Magazine"

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr "Linux Standards Base"

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr "Free Standards Group"

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr "SchoolForge"

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr "Open Source Initiative"

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Система отслеживания ошибок"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Администрирование и архивы списков рассылки"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Регистрационный стол новых членов"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Менеджеры учётных записей Debian"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Чтобы отправить личное сообщение всем менеджерам учётных записей, "
"используйте GPG-ключ 57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Сопровождающие сети ключей (PGP и GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Команда безопасности"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Политика"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Администрирование систем"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Этот адрес нужно использовать при возникновении проблем с доступом к машинам "
"Debian, в том числе проблем с паролями или при необходимости установки "
"пакета."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Если у вас возникли проблемы с аппаратным обеспечением машин Debian, см. "
"страницу <a href=\"https://db.debian.org/machines.cgi\">Машины Debian</a>. "
"Там должна быть информация об администраторе каждой конкретной машины."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Администратор каталога разработчиков LDAP"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Зеркала"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Сопровождающий DNS"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Система отслеживания пакетов"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr "Казначей"

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Запросы по использованию <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">торговой марки</a>"

#: ../../english/intro/organization.data:392
msgid "Salsa administrators"
msgstr "Администраторы Salsa"

#~ msgid "Anti-harassment"
#~ msgstr "Борьба с притеснениями"

#~ msgid "Debian Pure Blends"
#~ msgstr "Чистые смеси Debian"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian для детей от 1 до 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian в медицинской практике и исследованиях"

#~ msgid "Debian for education"
#~ msgstr "Debian в образовании"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian в юридической конторе"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian для людей с ограниченными возможностями"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian в науке и связанных исследованиях"

#~ msgid "Debian for astronomy"
#~ msgstr "Debian в астрономии"

#~ msgid "Individual Packages"
#~ msgstr "Отдельные пакеты"

#~ msgid "Bits from Debian"
#~ msgstr "Bits from Debian"

#~ msgid "Testing Security Team"
#~ msgstr "Команда безопасности тестируемого дистрибутива"

#~ msgid "Security Audit Project"
#~ msgstr "Проект всесторонней проверки безопасности"

#~ msgid "Volatile Team"
#~ msgstr "Команда поддержки часто меняющихся частей дистрибутива (volatile)"

#~ msgid "Vendors"
#~ msgstr "Поставщики"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Пользовательские дистрибутивы Debian"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Команда подготовки стабильного выпуска"

#~ msgid "Handhelds"
#~ msgstr "Карманные компьютеры"

#~ msgid "Marketing Team"
#~ msgstr "Команда работы с рынком"

#~ msgid "Key Signing Coordination"
#~ msgstr "Координация подписи ключей"

#~ msgid "Accountant"
#~ msgstr "Учётные записи"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Универсальная операционная система на настольном компьютере"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian в некоммерческих организациях"

#~ msgid "Summer of Code 2013 Administrators"
#~ msgstr "Администраторы Summer of Code 2013"

#~ msgid "current Debian Project Leader"
#~ msgstr "текущий Лидер Проекта Debian"

#~ msgid "DebConf chairs"
#~ msgstr "Председатели DebConf"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Сопровождающие связки ключей Сопровождающих Debian (DM)"

#~ msgid "Publicity"
#~ msgstr "Связи с общественностью"

#~ msgid "Live System Team"
#~ msgstr "Команда подготовки Live-выпуска"

#~ msgid "Auditor"
#~ msgstr "Контролёр"

#~ msgid "Alioth administrators"
#~ msgstr "Администраторы Alioth"

#~ msgid "User support"
#~ msgstr "Поддержка пользователей"

#~ msgid "Embedded systems"
#~ msgstr "Встроенные системы"

#~ msgid "Firewalls"
#~ msgstr "Брандмауэры"

#~ msgid "Laptops"
#~ msgstr "Переносные компьютеры"

#~ msgid "Special Configurations"
#~ msgstr "Особые варианты"

#~ msgid "Ports"
#~ msgstr "Переносы"

#~ msgid "CD Vendors Page"
#~ msgstr "Поставщики CD"

#~ msgid "Consultants Page"
#~ msgstr "Консультанты"
