#use wml::debian::translation-check translation="28bc87857803972597b697f1aafdfc05773ea8db" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В почтовом сервере Dovecot было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24386">CVE-2020-24386</a>

    <p>Если включен спящий режим для imap, злоумышленник (с корректными данными учётной
    записи для доступа к почтовому серверу) может использовать Dovecot для обнаружения структуры
    каталогов файловой системы и получать доступ к почтовым сообщениям других пользователей
    с помощью специально сформированных команд.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25275">CVE-2020-25275</a>

    <p>Иннокентий Сенновский сообщил, что доставка почты и её грамматический разбор
    в Dovecot могут завершаться аварийно, если 10000-ая MIME-часть сообщения имеет формат
    message/rfc822 (или если её родительское сообщение имеет формат multipart/digest). Эта уязвимость
    появилась из-за более ранних изменений, добавленных для исправления
    <a href="https://security-tracker.debian.org/tracker/CVE-2020-12100">\
    CVE-2020-12100</a>.</p></li>

</ul>

<p>В стабильном выпуске (buster) эти проблемы были исправлены в
версии 1:2.3.4.1-5+deb10u5.</p>

<p>Рекомендуется обновить пакеты dovecot.</p>

<p>С подробным статусом поддержки безопасности dovecot можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4825.data"
