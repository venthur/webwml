# Translation of organization.sk.po
# Copyright (C)
# This file is distributed under the same license as the webwml package.
# Ivan Masár <helix84@centrum.sk>, 2009, 2011, 2013, 2014, 2017.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"Report-Msgid-Bugs-To: debian-www@lists.debian.org\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2017-07-12 16:39+0200\n"
"Last-Translator: Ivan Masár <helix84@centrum.sk>\n"
"Language-Team: x\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"
"X-Generator: Virtaal 0.7.1\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr "delegácia pošty"

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr "email schôdzky"

#. One male delegate
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:18
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr "<void id=\"male\"/>delegát"

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr "<void id=\"male\"/>delegátka"

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"he_him\"/>he/him"
msgstr "<void id=\"male\"/>delegátka"

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"she_her\"/>she/her"
msgstr "<void id=\"male\"/>delegátka"

#: ../../english/intro/organization.data:24
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr "<void id=\"male\"/>delegátka"

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
#, fuzzy
#| msgid "<void id=\"female\"/>delegate"
msgid "<void id=\"they_them\"/>they/them"
msgstr "<void id=\"male\"/>delegátka"

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr "aktuálne"

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr "člen"

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr "manažér"

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr "SRM"

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr "Správca stabilného vydania"

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr "wizard"

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
#, fuzzy
msgid "chair"
msgstr "predseda"

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr "asistent"

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr "sekretár"

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid ""
"In the following list, <q>current</q> is used for positions that are\n"
"transitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr "Predstavenstvo"

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:108
msgid "Distribution"
msgstr "Distribúcia"

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:203
msgid "Communication and Outreach"
msgstr "Komunikácia a PR"

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:206
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:211
msgid "Publicity team"
msgstr "Marketingový tím"

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:284
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:312
msgid "Support and Infrastructure"
msgstr "Podpora a infraštruktúra"

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr "Vedúci"

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr "Technická komisia"

#: ../../english/intro/organization.data:103
msgid "Secretary"
msgstr "Sekretár"

#: ../../english/intro/organization.data:111
msgid "Development Projects"
msgstr "Vývojové projekty"

#: ../../english/intro/organization.data:112
msgid "FTP Archives"
msgstr "FTP archívy"

#: ../../english/intro/organization.data:114
msgid "FTP Masters"
msgstr "FTP Masters"

#: ../../english/intro/organization.data:120
msgid "FTP Assistants"
msgstr "FTP asistenti"

#: ../../english/intro/organization.data:126
msgid "FTP Wizards"
msgstr "FTP Wizards"

#: ../../english/intro/organization.data:130
msgid "Backports"
msgstr "Spätné porty"

#: ../../english/intro/organization.data:132
msgid "Backports Team"
msgstr "Tím spätných portov"

#: ../../english/intro/organization.data:136
msgid "Release Management"
msgstr "Správa vydaní"

#: ../../english/intro/organization.data:138
msgid "Release Team"
msgstr "Tím vydania"

#: ../../english/intro/organization.data:147
msgid "Quality Assurance"
msgstr "Zabezpečenie kvality"

#: ../../english/intro/organization.data:148
msgid "Installation System Team"
msgstr "Tím inštalátora"

#: ../../english/intro/organization.data:149
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:150
msgid "Release Notes"
msgstr "Poznámky k vydaniu"

#: ../../english/intro/organization.data:152
#, fuzzy
#| msgid "CD Images"
msgid "CD/DVD/USB Images"
msgstr "Obrazy CD"

#: ../../english/intro/organization.data:154
msgid "Production"
msgstr "Produkcia"

#: ../../english/intro/organization.data:161
msgid "Testing"
msgstr "Testovanie"

#: ../../english/intro/organization.data:163
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:167
msgid "Autobuilding infrastructure"
msgstr "Infraštruktúra automatického zostavovania"

#: ../../english/intro/organization.data:169
msgid "Wanna-build team"
msgstr "Tím wanna-build"

#: ../../english/intro/organization.data:176
msgid "Buildd administration"
msgstr "Správa buildd"

#: ../../english/intro/organization.data:193
msgid "Documentation"
msgstr "Dokumentácia"

#: ../../english/intro/organization.data:198
msgid "Work-Needing and Prospective Packages list"
msgstr "Balíky, ktoré potrebujú pomoc a perspektívne balíky"

#: ../../english/intro/organization.data:214
msgid "Press Contact"
msgstr "Tlačový kontakt"

#: ../../english/intro/organization.data:216
msgid "Web Pages"
msgstr "Webstránky"

#: ../../english/intro/organization.data:228
msgid "Planet Debian"
msgstr "Planet Debian"

#: ../../english/intro/organization.data:233
msgid "Outreach"
msgstr "PR"

#: ../../english/intro/organization.data:238
msgid "Debian Women Project"
msgstr "Projekt Ženy Debianu"

#: ../../english/intro/organization.data:246
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:255
msgid ""
"To send a private message to all the members of the Community Team, use the "
"GPG key <a href=\"community-team-pubkey.txt"
"\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Events"
msgstr "Udalosti"

#: ../../english/intro/organization.data:264
msgid "DebConf Committee"
msgstr "Komisia DebConf"

#: ../../english/intro/organization.data:271
msgid "Partner Program"
msgstr "Partnerský program"

#: ../../english/intro/organization.data:275
msgid "Hardware Donations Coordination"
msgstr "Koordinácia darov hardvéru"

#: ../../english/intro/organization.data:290
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:292
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:294
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:298
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:299
msgid "SchoolForge"
msgstr ""

#: ../../english/intro/organization.data:302
msgid ""
"OASIS: Organization\n"
"      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:305
msgid ""
"OVAL: Open Vulnerability\n"
"      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:308
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:315
msgid "Bug Tracking System"
msgstr "Systém sledovania chýb"

#: ../../english/intro/organization.data:320
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr "Správa konferencií a archívov konferencií"

#: ../../english/intro/organization.data:329
msgid "New Members Front Desk"
msgstr "Recepcia nových členov"

#: ../../english/intro/organization.data:335
msgid "Debian Account Managers"
msgstr "Správcovia účtov Debianu"

#: ../../english/intro/organization.data:339
msgid ""
"To send a private message to all DAMs, use the GPG key "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""
"Ak chcete poslať súkromnú správu všetkým DAM, použite kľúč GPG "
"57731224A9762EA155AB2A530CA8D15BB24D96F2."

#: ../../english/intro/organization.data:340
msgid "Keyring Maintainers (PGP and GPG)"
msgstr "Správcovia kľúčov (PGP a GPG)"

#: ../../english/intro/organization.data:344
msgid "Security Team"
msgstr "Bezpečnostný tím"

#: ../../english/intro/organization.data:355
msgid "Policy"
msgstr "Politika"

#: ../../english/intro/organization.data:358
msgid "System Administration"
msgstr "Správa systému"

#: ../../english/intro/organization.data:359
msgid ""
"This is the address to use when encountering problems on one of Debian's "
"machines, including password problems or you need a package installed."
msgstr ""
"Túto adresu použite, ak sa stretnete s problémami na niektorom so strojov "
"Debianu, aj v prípade že na nich potrebujete nainštalovať balík alebo máte "
"problém s heslom."

#: ../../english/intro/organization.data:369
msgid ""
"If you have hardware problems with Debian machines, please see <a href="
"\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should "
"contain per-machine administrator information."
msgstr ""
"Ak máte hardvérový problém so strojom Debianu, pozrite stránku <a href="
"\"https://db.debian.org/machines.cgi\">Strojov Debianu</a>, mala by "
"obsahovať informácie o správcovi každého zo strojov."

#: ../../english/intro/organization.data:370
msgid "LDAP Developer Directory Administrator"
msgstr "Správca LDAP adresára vývojárov"

#: ../../english/intro/organization.data:371
msgid "Mirrors"
msgstr "Zrkadlá"

#: ../../english/intro/organization.data:378
msgid "DNS Maintainer"
msgstr "Správca DNS"

#: ../../english/intro/organization.data:379
msgid "Package Tracking System"
msgstr "Systém sledovania balíkov"

#: ../../english/intro/organization.data:381
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:388
msgid ""
"<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""
"Žiadosti o použitie <a name=\"trademark\" href=\"m4_HOME/trademark"
"\">ochranných známok</a>"

#: ../../english/intro/organization.data:392
#, fuzzy
msgid "Salsa administrators"
msgstr "Správcovia Alioth"

#~ msgid "Anti-harassment"
#~ msgstr "Boj proti obťažovaniu"

#~ msgid "Debian Pure Blends"
#~ msgstr "Debian Pure Blends"

#~ msgid "Individual Packages"
#~ msgstr "Jednotlivé balíky"

#~ msgid "Debian for children from 1 to 99"
#~ msgstr "Debian pre deti od 1 do 99"

#~ msgid "Debian for medical practice and research"
#~ msgstr "Debian pre medicínsku prax a výskum"

#~ msgid "Debian for education"
#~ msgstr "Debian pre vzdelávanie"

#~ msgid "Debian in legal offices"
#~ msgstr "Debian v právnických kanceláriách"

#~ msgid "Debian for people with disabilities"
#~ msgstr "Debian pre ľudí s postihnutiami"

#~ msgid "Debian for science and related research"
#~ msgstr "Debian pre vedu a súvisiaci výskum"

#, fuzzy
#~| msgid "Debian for education"
#~ msgid "Debian for astronomy"
#~ msgstr "Debian pre vzdelávanie"

#~ msgid "Live System Team"
#~ msgstr "Tím Live systému"

#~ msgid "Auditor"
#~ msgstr "Auditor"

#~ msgid "Publicity"
#~ msgstr "Publicita"

#~ msgid "Bits from Debian"
#~ msgstr "Čriepky z Debianu"

#~ msgid "Debian Maintainer (DM) Keyring Maintainers"
#~ msgstr "Tím správcov zväzku kľúčov správcov Debianu (DM)"

#~ msgid "DebConf chairs"
#~ msgstr "Predsedovia DebConf"

#~ msgid "Volatile Team"
#~ msgstr "Tím volatile"

#~ msgid "Vendors"
#~ msgstr "Dodávatelia"

#~ msgid "Handhelds"
#~ msgstr "Handheldy"

#~ msgid "Marketing Team"
#~ msgstr "Marketingový tím"

#~ msgid "Key Signing Coordination"
#~ msgstr "Koordinácia podpisovania kľúčov"

#~ msgid "Custom Debian Distributions"
#~ msgstr "Prispôsobené distribúcie Debianu"

#~ msgid "Release Team for ``stable''"
#~ msgstr "Tím vydania „stable“"

#~ msgid "Accountant"
#~ msgstr "Účtovník"

#~ msgid "The Universal Operating System as your Desktop"
#~ msgstr "Univerzálny operačný systém pre pracovnú stanicu"

#~ msgid "Debian for non-profit organisations"
#~ msgstr "Debian pre neziskové organizácie"

#~ msgid ""
#~ "The admins responsible for buildd's for a particular arch can be reached "
#~ "at <genericemail arch@buildd.debian.org>, for example <genericemail "
#~ "i386@buildd.debian.org>."
#~ msgstr ""
#~ "Správcov zodpovedajúcich za buildd konkrétnej architekúry môžete "
#~ "zastihnúť na <genericemail arch@buildd.debian.org>, napr. <genericemail "
#~ "i386@buildd.debian.org>."

#~ msgid ""
#~ "Names of individual buildd's admins can also be found on <a href=\"http://"
#~ "www.buildd.net\">http://www.buildd.net</a>.  Choose an architecture and a "
#~ "distribution to see the available buildd's and their admins."
#~ msgstr ""
#~ "Mená správcov jednotlivých buildd nájdete tiež na <a href=\"http://www."
#~ "buildd.net\">http://www.buildd.net</a>.  Vyberte si architektúru a "
#~ "distribúciu pre ktorú chcete zobraziť správcov buildd."

#~ msgid "current Debian Project Leader"
#~ msgstr "súčasný líder projektu Debian"

#~ msgid "Security Audit Project"
#~ msgstr "Projekt Bezpečnostný audit"

#~ msgid "Testing Security Team"
#~ msgstr "Bezpečnostný tím pre „testing“"

#~ msgid "Alioth administrators"
#~ msgstr "Správcovia Alioth"

#~ msgid "User support"
#~ msgstr "Používateľská podpora"

#~ msgid "Embedded systems"
#~ msgstr "Vnorené systémy"

#~ msgid "Firewalls"
#~ msgstr "Firewally"

#~ msgid "Laptops"
#~ msgstr "Notebooky"

#~ msgid "Special Configurations"
#~ msgstr "Špeciálne konfigurácie"

#~ msgid "Ports"
#~ msgstr "Porty"

#~ msgid "CD Vendors Page"
#~ msgstr "Stránka dodávateľov CD"

#~ msgid "Consultants Page"
#~ msgstr "Stránka konzultantov"
