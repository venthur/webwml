#use wml::debian::template title="Forældet dokumentation"
#use wml::debian::translation-check translation="4792632f7a20682a368627c66663b57ff1b3fa8b"
#include "$(ENGLISHDIR)/doc/manuals.defs"
#include "$(ENGLISHDIR)/doc/obsolete.defs"

<h1 id="historical">Historiske dokumenter</h1>

<p>De nedenfor anførte dokumenter blev enten skrevet for længe siden og er ikke
ført ajour, eller blev skrevet til tidligere udgaver af Debian og er ikke blevet
opdateret til de aktuelle udgaver.  Oplysningerne i dokumenterne er forældet,
men kan stadig være af interesse for nogle læsere.</p>

<p>De dokumener som ikke længere er relevante og ikke længere tjener noget 
formål, har fået fjernet deres referencer, men kildekoden til mange af de 
forældede dokumenter kan findes på 
<a href="https://salsa.debian.org/ddp-team/attic">DDP's loft</a>.</p>


<h2 id="user">Brugerorienteret dokumentation</h2>

<document "dselect-dokumentation til begyndere" "dselect">

<div class="centerblock">
<p>
  Denne fil dokumenterer deselect for førstegangsbrugere, og har det mål at 
  hjælpe med at foretage en vellykket installering af Debian. Der forsøges 
  ikke på at forklare alt, derfor skal du også læse hjælpe-skærmbillederne, 
  første gang du anvender dselect.
</p>
<doctable>
  <authors "Stéphane Bortzmeyer">
  <maintainer "(?)">
  <status>
  gået i stå: <a href="https://packages.debian.org/aptitude">aptitude</a> har
  erstattet dselect som Debians standardprogram til pakkehåndteringsgrænseflade.  
  </status>
  <availability>
  <inddpvcs name="dselect-beginner" formats="html txt pdf ps"  
    langs="ca da en fr it ja hr pl pt ru sk es cs de" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Brugervejledning" "users-guide">

<div class="centerblock">
<p>
Denne <q>brugervejledning</q> er en omformatteret udgave af <q>Progenys brugervejledning</q>.
Indholdet er tilpasset til Debians-standardsystem.</p>

<p>Over 300 sider med en god vejledning i at bruge Debians styresystem fra 
den grafiske brugerflade og kommandolinien.
</p>
<doctable>
  <authors "Progeny Linux Systems, Inc.">
  <maintainer "Osamu Aoki (&#38738;&#26408; &#20462;)">
  <status>
  I sig selv nyttig som en vejledning.  Skrevet til udgivelsen "woody", er ved
  at blive overflødig.
  </status>
  <availability>
# langs="en" isn't redundant, it adds the needed language suffix to the link  
  <inddpvcs name="users-guide" index="users-guide" langs="en" formats="html txt pdf" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debians lærebog" "tutorial">

<div class="centerblock">
<p>
Denne håndbog er beregnet til nye Linux-brugere, som en hjælp til at lære 
Linux at kende, når systemet er blevet installeret, eller til nye Linux-brugere 
på systemer som administreres af andre.
</p>
<doctable>
  <authors "Havoc Pennington, Oliver Elphick, Ole Tetlie, James Treacy,
  Craig Sawyer, Ivan E. Moore II">
  <editors "Havoc Pennington">
  <maintainer "(?)">
  <status>
  gået i stå; ikke færdig; muligvis forældet på grund af
  <a href="user-manuals#quick-reference">Debian-reference</a>
  </status>
  <availability>
  ikke klar endnu
  <inddpvcs name="debian-tutorial" vcsname="tutorial" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debian GNU/Linux: Vejledning i installering og anvendelse" "guide">

<div class="centerblock">
<p>
  En håndbog, rettet mod slutbrugere.
</p>
<doctable>
  <authors "John Goerzen, Ossama Othman">
  <editors "John Goerzen">
  <status>
  færdig (men handler om potato)
  </status>
  <availability>
  <inoldpackage "debian-guide">
  </availability>
</doctable>
</div>

<hr />

<document "Debians brugeropslagsbog" "userref">

<div class="centerblock">
<p>
  Denne håndbog giver om ikke andet en oversigt over alt hvad en bruger bør 
  vide om sit Debian GNU/Linux-system (f.eks. opsætning af X, hvordan netværket 
  sættes op, tilgang til disketter, osv.). Det er hensigten at slå bro mellem 
  Debian-vejledningen og de detaljerede håndbøger og info-sider, der følger med 
  alle pakker.</p>

  <p>Det er også hensigten at give en vis indsigt i hvordan man kombinerer 
  kommandoer, efter det genrelle Unix-princip, at der <em>altid er mere end en 
  måde at gøre det på</em>.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Jason D. Waterman, Havoc Pennington,
      Oliver Elphick, Bruce Evry, Karl-Heinz Zimmer">
  <editors "Thalia L. Hooker, Oliver Elphick">
  <maintainer "(?)">
  <status>
  gået i stå og ganske mangelfuld; muligvis forældet på grund af
  <a href="user-manuals#quick-reference">Debian-reference</a>
  </status>
  <availability>
  <inddpvcs name="user" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debians system-administrationshåndbog" "system">

<div class="centerblock">
<p>
  Dette dokument nævnes i indledningen til fremgangsmådehåndbogen.
  Det dækker alle aspekter af systemadministrering af et Debian-system.
</p>
<doctable>
  <authors "Tapio Lehtonen">
  <maintainer "Tapio Lehtonen">
  <status>
  gået i stå; ufuldstændig; muligvis forældet på grund af 
  <a href="user-manuals#quick-reference">Debian-reference</a> 
  </status>
  <availability>
  ikke tilgængelig endnu
  <inddpvcs name="system-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Debians netværkadministrationshåndbog" "network">

<div class="centerblock">
<p>
  Denne håndbog dækker alle aspekter af netværksadministrering af et
  Debian-system.
</p>
<doctable>
  <authors "Ardo van Rangelrooij, Oliver Elphick, Duncan C. Thomson, Ivan E. Moore II">
  <maintainer "(?)">
  <status>
  gået i stå; ufuldstændig; muligvis forældet på grund af 
  <a href="user-manuals#quick-reference">Debian-reference</a>
  </status>
  <availability>
  ikke tilgængelig endnu
  <inddpvcs name="network-administrator" vcstype="attic">
  </availability>
</doctable>
</div>

<hr />

<document "Linux-kogebog" "linuxcookbook">

<div class="centerblock">
<p>
  En opslagsbog om Debian GNU/Linux-systemet, med over 1500 <q>opskrifter</q> på 
  hvordan man bruger systemet i dagligdagen &ndash; fra arbejde med tekst, billeder 
  og lyd, til problemer med produktivitet og netværk.  Som den software bogen
  beskriver, er den udgivet under "copyleft" og kildedataene er tilgængelige.
</p>
<doctable>
  <authors "Michael Stutz">
  <status>
  udgivet, skrevet til woody, er ved at blive forældet
  </status>
  <availability>
  <inoldpackage "linuxcookbook">
  <p><a href="http://dsl.org/cookbook/">fra forfatteren</a>
  </availability>
</doctable>
</div>

<hr />

<document "APT HOWTO" "apt-howto">

<div class="centerblock">
<p>
  Denne håndbog er en hurtig, men komplet, informationskilde om APT-systemet 
  og dets muligheder. Der er mange oplysninger om APT's primære 
  anvendelsesområder og mange eksempler.
  </p>
<doctable>
  <authors "Gustavo Noronha Silva">
  <maintainer "Gustavo Noronha Silva">
  <status>
  forældet siden 2009
  </status>
  <availability>
  <inoldpackage "apt-howto">
  <br>
  <inddpvcs name="apt-howto" langs="ca en fr el it ja zh-cn zh-tw ko pl pt-br ru es cs tr de uk"
	formats="html txt pdf ps" naming="locale" vcstype="attic" />
  </availability>
</doctable>
</div>


<h2 id="devel">Udviklerdokumentation</h2>

<document "Introduktion: Fremstil en Debian-pakke" "makeadeb">

<div class="centerblock">
<p>
  Introduktion til hvordan man laver en <code>.deb</code> ved hjælp af
  <strong>debmake</strong>.
</p>
<doctable>
  <authors "Jaldhar H. Vyas">
  <status>
  stoppet, overflødiggjort af <a href="devel-manuals#maint-guide">vejledning til nye udviklere</a>
  </status>
  <availability>
  <a href="https://people.debian.org/~jaldhar/">HTML online</a>
  </availability>
</doctable>
</div>

<hr />

<document "Debians håndbog til programmører" "programmers">

<div class="centerblock">
<p>
  Vejleder nye udviklere i at oprette en pakke til Debian GNU/Linux-systemet.
</p>
<doctable>
  <authors "Igor Grobman">
  <status>
  forældet pga. <a href="devel-manuals#maint-guide">vejledning til nye udviklere</a>
  </status>
  <availability>
  ikke færdig
  <inddpvcs name="programmer" vcstype="attic">
  </availability>
</doctable>
</div>

<document "Debians pakningshåndbog" "packman">

<div class="centerblock">
<p>
  Denne håndbog beskriver de tekniske aspekter ved fremstillingen af
  Debians binære- og kildekodepakker. Den dokumenterer også grænsefladen
  mellem dselect og dettes adgangsscripts. Håndbogen behandler ikke
  Debian-projekets retningslinier, og den forudsætter kendskab til dpkg's
  funktionalitet fra en systemadministrators perspektiv.
</p>
<doctable>
  <authors "Ian Jackson, Klee Dienes, David A. Morris, Christian Schwarz">
  <status>
  Delene, som var de facto-retningslinier blev flettet ind i
  <a href="devel-manuals#policy">debian-policy</a>.
  </status>
  <availability>
  <inoldpackage "packaging-manual">
  </availability>
</doctable>
</div>

<document "Hvordan software-producenter kan distribuere deres produkter direkte i .deb-formatet" "swprod">

<div class="centerblock">
<p>
  Dette dokument er tænkt som udgangspunkt til at forklare hvordan
  softwareproducenter kan integrere deres produkter med Debian, hvilke
  forskellige situationer der kan opstå, afhængigt af produkternes licens
  og producentens valg, samt hvilke muligheder der er.  Det forklarer ikke
  hvordan man fremstiller pakker, men har henvisninger til dokumenter som
  forklarer lige netop dette.
</p>
<p>
  Læs dette dokument hvis du ikke har indblik i alt hvad det indebærer, at
  fremstille og distribuere Debian-pakker, og hvordan disse eventuelt føjes til
  Debians distribution.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  forældet
  </status>
  <availability>
  <inddpvcs-distribute-deb>
  </availability>
</doctable>
</div>

<hr />

<document "Introduktion til i18n" "i18n">

<div class="centerblock">
<p>
  Dette dokument beskriver de grundlæggende idéer og hvordan man udfører 
  l10n (lokaltilpasning/localization),
  i18n (internationalisering/internationalization), og
  m17n (flersprogstilpasning/multilingualization) for programmører og
  pakkevedligeholdere.
</p>
<p>
  Formålet med dette dokument er at få flere pakker til at understøtte
  i18n og gøre Debian til en mere internationaliseret distribution. Bidrag fra
  hele verden er velkomne, da den oprindelige forfatter er japansktalende og
  dette dokument dermed vil handle om "japanisering" hvis der ikke kommer
  bidrag.
</p>
<doctable>
  <authors "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <maintainer "Tomohiro KUBOTA (&#20037;&#20445;&#30000;&#26234;&#24195;)">
  <status>
  forældet, gået i stå
  </status>
  <availability>
  ikke klar endnu
  <inddpvcs-intro-i18n>
  </availability>
</doctable>
</div>

<hr>

<document "Debian SGML/XML HOWTO" "sgml-howto">

<div class="centerblock">
<p>
  Denne HOWTO indeholder praktiske oplysninger om anvendelse af SGML og XML på 
  et Debian-styresystem.
</p>
<doctable>
  <authors "Stephane Bortzmeyer">
  <maintainer "Stephane Bortzmeyer">
  <status>
  gået i stå, forældet
  </status>
  <availability>

# English only using index.html, so langs set.
  <inddpvcs name="sgml-howto" formats="html" srctype="SGML" vcstype="attic" />
</availability>
</doctable>
</div>

<hr>

<document "Debian XML/SGML Policy" "xml-sgml-policy">

<div class="centerblock">
<p>
  Tillæg til Debians fremgangsmåder (subpolicy) beskrivende Debian-pakker
  der anvender og/eller stiller XML/SGML-ressourcer til rådighed.
</p>
<doctable>
  <authors "Mark Johnson, Ardo van Rangelrooij, Adam Di Carlo">
  <status>
  dødt
  <tt>sgml-base-doc</tt> samt nyt materiale vedrørende XML-kataloghåndtering
  </status>
  <availability>
  <inddpvcs-xml-sgml-policy>
  </availability>
</doctable>
</div>

<hr>

<document "DebianDoc-SGML Markup-håndbog" "markup">

<div class="centerblock">
<p>
  Dokumentation af <strong>debiandoc-sgml</strong>-systemet,
  indeholdende de bedste fremgangsmåder og tips til vedligeholdere. 
  Efterfølgende udgaver bør indeholde tips til nemmere vedligeholdelse og 
  opbygning af dokumentation i Debian-pakker, retningslinier for organisering 
  af oversættelse af dokumentationen, og andre nyttige oplysninger.
  Se også <a href="https://bugs.debian.org/43718">fejl #43718</a>.

<doctable>
  <authors "Ian Jackson, Ardo van Rangelrooij">
  <maintainer "Ardo van Rangelrooij">
  <status>
  færdig
  </status>
  <availability>
  <inpackage "debiandoc-sgml-doc">
  <inddpvcs-debiandoc-sgml-doc>
  </availability>
</doctable>
</div>

<h2 id="misc">Forskellig dokumentation</h2>

<document "Debian Repository HOWTO" "repo">

<div class="centerblock">
<p>
  Dette dokument forklarer hvordan Debians arkiver (repositories) fungerer, 
  hvordan man opretter dem og hvordan man føjer dem til <tt>sources.list</tt> 
  på den rette måde.
</p>
<doctable>
  <authors "Aaron Isotton">
  <maintainer "Aaron Isotton">
  <status>
  klar (?)
  </status>
  <availability>
  <inddpvcs name="repository-howto" index="repository-howto"
    formats="html" langs="en fr ta de uk" vcstype="attic">
  </availability>
</doctable>
</div>
