#use wml::debian::translation-check translation="57142714136b016072b02516b529006538983a39" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Flere sårbarheder er opdaget i Linux-kernen, hvilke kunne føre til en 
rettighedsforøgelse, lammelsesangreb eller informationslækager.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-36310">CVE-2020-36310</a>

    <p>En fejl blev opdaget i KVM-implementeringen til AMD-processorer, hvilken 
    kunne føre til en uendelig løkke.  En ondsindet VM-gæst kunne udnytte fejlen 
    til at forårsage et lammelsesangreb.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">CVE-2022-0001</a> (INTEL-SA-00598)

    <p>Efterforskere ved VUSec opdagede at Branch History Buffer i 
    Intel-processorer kunne udnyttes til at iværksætte informationssidekanaler 
    med spekulativ udførelse.  Problemet svarer til Spectre variant 2, men 
    kræver yderligere afhjælpningsforanstaltninger på nogle processorer.</p>

    <p>Det kan udnyttes til at få fat i følsomme oplysninger fra en anden 
    sikkerhedskontekt, eksempelvis fra brugerrummet til kernen, eller fra en 
    KVM-gæst til kernen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0002">CVE-2022-0002</a> (INTEL-SA-00598)

    <p>Et problem svarende til 
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-0001">\
    CVE-2022-0001</a>, men dækker udnyttelse indenfor en sikkerhedskontekst, 
    eksempelvis fra JIT-kompileret kode i en sandkasse til værtskode i den 
    samme proces.</p>

    <p>Det er delvist afhjulpet ved at deaktivere eBPF for upriviligerede 
    brugere med sysctl: kernel.unprivileged_bpf_disabled=2.  Det er allerede 
    standarden i Debian 11 <q>bullseye</q>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0487">CVE-2022-0487</a>

    <p>En anvendelse efter frigivelse blev opdaget i MOXART SD/MMC Host 
    Controller-supportdriveren.  Fejlen påvirker ikke Debians binære pakker, da 
    CONFIG_MMC_MOXART ikke er opsat.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0492">CVE-2022-0492</a>

    <p>Yiqi Sun og Kevin Wang rapporterede at undersystemet cgroup-v1 ikke på 
    korrekt vis begrænsede adgang til release-agent-funktionaliteten.  En lokal 
    bruger kunne drage nytte af fejlen til rettighedsforøgelse og omgåelse af 
    navnerumsisolation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0617">CVE-2022-0617</a>

    <p>butt3rflyh4ck opdagede en NULL-pointerdereference i UDF-filsystemet.  En 
    lokal bruger, der kan mounte et særligt fremstillet UDF-filaftryk, kunne 
    udnytte fejlen til at få systemet til at gå ned.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-25636">CVE-2022-25636</a>

    <p>Nick Gregory rapporterede om en heapfejl i forbindelse med skrivning 
    udenfor grænserne i netfilter-undersystemet.  En bruger med kapabiliteten 
    CAP_NET_ADMIN, kunne udnytte fejlen til lammelsesangreb eller muligvis 
    rettighedsforøgelse.</p></li>

</ul>

<p>I den stabile distribution (bullseye), er disse problemer rettet
i version 5.10.103-1.  Opdateringen indeholder desuden mange flere fejlrettelser 
fra de stabile opdateringer fra 5.10.93 til 5.10.103, begge inklusive.</p>

<p>Vi anbefaler at du opgraderer dine linux-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende linux, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5095.data"
