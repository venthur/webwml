#use wml::debian::translation-check translation="47c9da48bb2e6ee7f5d51bb1bfb0e169808fcbf0" mindelta="1"
<define-tag description>sikkerhedsopdatering</define-tag>
<define-tag moreinfo>
<p>Følgende sårbarheder er opdaget i webmotoren webkit2gtk:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9802">CVE-2020-9802</a>

    <p>Samuel Gross opdagede at behandling af ondsindet fremstillet webindhold 
    kunne føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9803">CVE-2020-9803</a>

    <p>Wen Xu opdagede at behandling af ondsindet fremstillet webindhold kunne 
    føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9805">CVE-2020-9805</a>

    <p>En anonym efterforsker opdagede at behandling af ondsindet fremstillet 
    webindhold kunne føre til univerel udførelse af skripter på tværs af 
    servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9806">CVE-2020-9806</a>

    <p>Wen Xu opdagede at behandling af ondsindet fremstillet webindhold kunne 
    føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9807">CVE-2020-9807</a>

    <p>Wen Xu opdagede at behandling af ondsindet fremstillet webindhold kunne 
    føre til udførelse af vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9843">CVE-2020-9843</a>

    <p>Ryan Pickren opdagede at behandling af ondsindet fremstillet webindhold 
    kunne føre til et angreb med udførelse af skripter på tværs af 
    servere.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9850">CVE-2020-9850</a>

    <p>@jinmo123, @setuid0x0_ og @insu_yun_en opdagede at en fjernangriber kunne 
    være i stand til at udføre vilkårlig kode.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13753">CVE-2020-13753</a>

    <p>Milan Crha opdagede at en angriber kunne være i stand til at udføre 
    kommandoer udenfor bubblewrap-sandkassen.</p></li>

</ul>

<p>I den stabile distribution (buster), er disse problemer rettet i
version 2.28.3-2~deb10u1.</p>

<p>Vi anbefaler at du opgraderer dine webkit2gtk-pakker.</p>

<p>For detaljeret sikkerhedsstatus vedrørende webkit2gtk, se
dens sikkerhedssporingsside på:
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4724.data"
