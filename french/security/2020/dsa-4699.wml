#use wml::debian::translation-check translation="d948bd9589feece12bc709aa1b03a99f72526fc6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3016">CVE-2019-3016</a>

<p>L'implémentation de KVM pour x86 ne réalisait pas toujours la purge des
TLB si la fonctionnalité de purge des TLB paravirtualisés était activée.
Cela pourrait conduire à la divulgation d'informations sensibles dans une
VM cliente.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19462">CVE-2019-19462</a>

<p>L'outil syzkaller a découvert une vérification d’erreur manquante dans
la bibliothèque <q>relay</q> utilisée pour implémenter divers fichiers sous
debugfs. Un utilisateur local autorisé à accéder à debugfs pourrait utiliser
cela pour provoquer un déni de service (plantage) ou éventuellement pour
une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-0543">CVE-2020-0543</a>

<p>Des chercheurs de l'Université libre d'Amsterdam ont découvert que, sur
certains processeurs Intel prenant en charge les instructions RDRAND et
RDSEED, une partie d'une valeur aléatoire générée par ces instructions
pouvait être utilisée dans une exécution spéculative ultérieure dans
n'importe quel cœur du même processeur physique. Selon la manière dont ces
instructions sont utilisées par les applications, un utilisateur local ou
une machine virtuelle pourrait utiliser cela pour obtenir des informations
sensibles telles que des clés de chiffrement d'autres utilisateurs ou
machines virtuelles.</p>

<p>Cette vulnérabilité peut être atténuée par une mise à jour du microcode,
soit dans le cadre des microprogrammes du système (BIOS) ou au moyen du
paquet intel-microcode de la section non-free de l'archive Debian. Cette
mise à jour du noyau ne fournit que le rapport de la vulnérabilité et
l'option de désactiver la mitigation si elle n'est pas nécessaire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10711">CVE-2020-10711</a>

<p>Matthew Sheets a signalé des problèmes de déréférencement de pointeur
NULL dans le sous-système SELinux lors de la réception de paquet CIPSO avec
une catégorie vide. Un attaquant distant peut tirer avantage de ce défaut
pour provoquer un déni de service (plantage). Notez que ce problème
n'affecte pas les paquets binaires distribués dans Debian dans la mesure où
CONFIG_NETLABEL n'est pas activé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10732">CVE-2020-10732</a>

<p>Une fuite d'informations de la mémoire privée du noyau vers l'espace
utilisateur a été découverte dans l'implémentation du noyau du vidage mémoire
des processus de l'espace utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10751">CVE-2020-10751</a>

<p>Dmitry Vyukov a signalé que le sous-système SELinux ne gérait pas
correctement la validation de messages multiples, ce qui pourrait permettre
à un attaquant privilégié de contourner les restrictions netlink de
SELinux.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10757">CVE-2020-10757</a>

<p>Fan Yang a signalé un défaut dans la manière dont mremap gérait les très
grandes pages DAX, permettant une élévation des privilèges d'un utilisateur
local.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12114">CVE-2020-12114</a>

<p>Piotr Krysiuk a découvert une situation de compétition entre les
opérations umount et pivot_root dans le noyau du système de fichiers (vfs).
Un utilisateur local doté de la capacité CAP_SYS_ADMIN dans un espace de
noms d'utilisateur pourrait éventuellement utiliser cela pour provoquer un
déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12464">CVE-2020-12464</a>

<p>Kyungtae Kim a signalé une situation de compétition dans le noyau USB
qui peut avoir pour conséquence dans une utilisation de mémoire après
libération. Le moyen de l'exploiter n'est pas clair, mais elle pourrait
avoir pour conséquence un déni de service (plantage ou corruption de
mémoire) ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12768">CVE-2020-12768</a>

<p>Un bogue a été découvert dans l'implémentation de KVM pour les
processeurs AMD qui pourrait avoir pour conséquence une fuite de mémoire.
L'impact de sécurité de cela n'est pas clair.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12770">CVE-2020-12770</a>

<p>Le pilote sg (SCSI générique) ne libérait pas correctement des
ressources internes dans un cas d'erreur particulier. Un utilisateur local
autorisé à accéder à un service sg pourrait éventuellement utiliser cela
pour provoquer un déni de service (épuisement de ressource).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13143">CVE-2020-13143</a>

<p>Kyungtae Kim a signalé une possible écriture de tas hors limites écrite
dans le sous-système gadget USB. Un utilisateur local autorisé à écrire
dans le système de fichiers de configuration de gadget pourrait utiliser
cela pour provoquer un déni de service (plantage ou corruption de mémoire)
ou éventuellement pour une élévation de privilèges.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 4.19.118-2+deb10u1. Cette version corrige aussi certains
bogues liés qui n'ont pas leur propre identifiant CVE et une régression
dans l'en-tête UAPI dde &lt;linux/swab.h&gt; introduite dans la version
intermédiaire précédente (bogue nº 960271).</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4699.data"
# $Id: $
