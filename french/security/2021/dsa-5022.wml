#use wml::debian::translation-check translation="9c44c7905cd7bbe6df67ff227e8eaec739c984bb" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>

<p>Il a été découvert que le correctif pour traiter le <a
href="https://security-tracker.debian.org/tracker/CVE-2021-44228">\
CVE-2021-44228</a> dans Apache Log4j, une infrastructure de journalisation
pour Java, était incomplet dans certaines configurations autres que celle
par défaut. Cela pourrait permettre à des attaquants ayant le contrôle sur
des données d'entrée de carte de contextes de thread (MDC), quand la
configuration de journalisation utilise un motif autre que celui par défaut
avec soit une recherche de contexte (par exemple, $${ctx:loginId}) ou un
motif de carte de contextes de thread (%X, %mdc, ou %MDC) de contrefaire
des données d'entrée en utilisant un patron de recherche de JNDI avec pour
conséquence une attaque par déni de service (DOS).</p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 2.16.0-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2.16.0-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets apache-log4j2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de apache-log4j2,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/apache-log4j2">\
https://security-tracker.debian.org/tracker/apache-log4j2</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5022.data"
# $Id: $
