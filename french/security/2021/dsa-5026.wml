#use wml::debian::translation-check translation="d8db68fcc4a4ff36a9319a063d683c9b9c88f413" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla, qui pourraient éventuellement avoir pour
conséquences l'exécution de code arbitraire, la divulgation d'informations
ou une usurpation.</p>

<p>Debian suit les éditions longue durée (ESR, « Extended Support
Release ») de Firefox. Le suivi des séries 78.x est terminé, aussi, à
partir de cette mise à jour, Debian suit les versions 91.x.</p>

<p>Entre les versions 78.x et 91.x, Firefox a vu nombre de ses fonctions
mises à jour. Pour davantage d'informations, veuillez consulter la page
<a href="https://www.mozilla.org/en-US/firefox/91.0esr/releasenotes/">\
https://www.mozilla.org/en-US/firefox/91.0esr/releasenotes/</a>.</p>

<p>Pour la distribution oldstable (Buster) une mise à jour plus achevée
de la chaîne de compilation est nécessaire, des paquets mis à jour seront
bientôt disponibles avec la version 91.4.1esr-1~deb10u1.</p>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 91.4.1esr-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5026.data"
# $Id: $
