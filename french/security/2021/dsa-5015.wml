#use wml::debian::translation-check translation="65a9c27818db1c1e093dd5651ca3c518685e3224" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Andrew Bartlett a découvert que Samba, un serveur de fichier SMB/CIFS,
d'impression et de connexion pour UNIX, peut mapper des utilisateurs de
domaine sur des utilisateurs locaux d'une façon indésirable. Cela pourrait
permettre à un utilisateur dans un domaine AD de devenir éventuellement
superutilisateur sur les membres du domaine.</p>

<p>Un nouveau paramètre <q>min domain uid</q> (par défaut 1000) a été
ajouté pour définir l'UID minimal permis lors du mappage d'un compte local
sur un compte de domaine.</p>

<p>Davantage de détails et de solutions peuvent être trouvés dans
l'avertissement amont
<a href="https://www.samba.org/samba/security/CVE-2020-25717.html">\
https://www.samba.org/samba/security/CVE-2020-25717.html</a></p>

<p>Pour la distribution oldstable (Buster), ce problème a été corrigé dans
la version 2:4.9.5+dfsg-5+deb10u2. En outre, la mise à jour atténue le
<a href="https://security-tracker.debian.org/tracker/CVE-2020-25722">CVE-2020-25722</a>.
Malheureusement les modifications exigées pour corriger des CVE
supplémentaires affectant Samba en tant que contrôleur de domaine
compatible avec AD sont trop invasives pour être rétroportées. Aussi, les
utilisateurs se servant de Samba comme contrôleur de domaine compatible
avec AD sont encouragés à migrer vers Debian Bullseye. Dorénavant, les
installations de contrôleur de domaine AD ne sont plus prises en charge
dans Debian oldstable.</p>

<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de samba, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-5015.data"
# $Id: $
