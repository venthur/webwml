#use wml::debian::translation-check translation="1d182b3abd199000857dae0786cf70a840908dd1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Joe Vennix a découvert que sudo, un programme conçu pour donner des
droits limités de superutilisateur à des utilisateurs particuliers, quand
il est configuré pour permettre à un utilisateur d'exécuter des commandes
comme un utilisateur arbitraire au moyen du mot clé ALL dans une
spécification RunAs, permet d'exécuter des commandes en tant que
superutilisateur en spécifiant -1 ou 4294967295 comme ID utilisateur. Cela
pourrait permettre à un utilisateur, doté de privilèges sudo appropriés,
d'exécuter des commandes en tant que superutilisateur même si la
spécification RunAs lui interdit de façon explicite un accès en tant que
superutilisateur.</p>

<p>Pour plus de détails, consultez l'avertissement amont à l'adresse
<a href="https://www.sudo.ws/alerts/minus_1_uid.html">https://www.sudo.ws/alerts/minus_1_uid.html</a> .</p>

<p>Pour la distribution oldstable (Stretch), ce problème a été corrigé dans
la version 1.8.19p1-2.1+deb9u1.</p>

<p>Pour la distribution stable (Buster), ce problème a été corrigé dans la
version 1.8.27-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sudo.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sudo, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sudo">\
https://security-tracker.debian.org/tracker/sudo</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4543.data"
# $Id: $
