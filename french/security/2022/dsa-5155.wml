#use wml::debian::translation-check translation="4d331211e7fad899902d668f3ffed6e3138553a6" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
WPE WebKit :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26700">CVE-2022-26700</a>

<p>ryuzaki a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26709">CVE-2022-26709</a>

<p>Chijin Zhou a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26716">CVE-2022-26716</a>

<p>SorryMybad a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26717">CVE-2022-26717</a>

<p>Jeonghoon Shin a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26719">CVE-2022-26719</a>

<p>Dongzhuo Zhao a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30293">CVE-2022-30293</a>

<p>Chijin Zhou a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire ou à un déni de service
(plantage d'application).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30294">CVE-2022-30294</a>

<p>Chijin Zhou a découvert que le traitement d'un contenu web contrefait
pouvait conduire à l'exécution de code arbitraire ou à un déni de service
(plantage d'application).</p></li>

</ul>


<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 2.36.3-1~deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets wpewebkit.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de wpewebkit, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/wpewebkit">\
https://security-tracker.debian.org/tracker/wpewebkit</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5155.data"
# $Id: $
