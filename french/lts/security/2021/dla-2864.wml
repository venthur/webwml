#use wml::debian::translation-check translation="3d6d970361d1892a6d5caa934f2591a402dfa4c3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans ruby-haml, un moteur de modèles XHTML/XML structurés élégant, lors
de l'utilisation d'une entrée utilisateur pour réaliser des tâches sur le
serveur, les caractères tels que < > " ' doivent être protégés
correctement. Dans cette utilisation, le caractère ' était absent. Un
attaquant pouvait manipuler l’entrée pour introduire des attributs
supplémentaires, avec une exécution de code potentielle.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
4.0.7-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-haml.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-haml, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-haml">\
https://security-tracker.debian.org/tracker/ruby-haml</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2864.data"
# $Id: $

