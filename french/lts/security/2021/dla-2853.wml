#use wml::debian::translation-check translation="30cffc5191007996ed0593f734bb9cea8fee5a01" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité d'usurpation de préfixe de cookie dans
CGI::Cookie.parse et une vulnérabilité de déni de service à l'aide d'une
expression rationnelle (ReDoS) sur les méthodes d'analyse de date ont été
découvertes dans src:ruby2.1, l'interpréteur Ruby.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.3.3-1+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby2.3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby2.3">\
https://security-tracker.debian.org/tracker/ruby2.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2853.data"
# $Id: $
