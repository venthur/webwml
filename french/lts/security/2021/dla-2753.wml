#use wml::debian::translation-check translation="5eb55d58ea9441458b6404b577acb0a5af82b261" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans Qemu, un
émulateur rapide de processeur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3713">CVE-2021-3713</a>

<p>Un défaut d’écriture hors limites a été découvert dans l’émulation du
périphérique UAS (USB Attached SCSI) par QEMU. Le périphérique utilise le
numéro de flux fourni par le client sans vérification. Cela peut conduire à un
accès hors limites aux champs UASDevice-&gt;data3 et UASDevice-&gt;status3.
Un utilisateur client malveillant pourrait utiliser ce défaut pour planter
QEMU ou éventuellement réaliser une exécution de code avec les privilèges du
processus QEMU dans l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3682">CVE-2021-3682</a>

<p>Un défaut a été découvert dans l’émulation de périphérique redirecteur de
QEMU. Il apparaît lors de transmissions de paquets lors de transferts en bloc à
partir d’un client SPICE à cause de la file d’attente de paquets pleine. Un
client SPICE malveillant pourrait utiliser ce défaut pour que QEMU
appelle free() avec des métadonnées de parties de tas falsifiées, aboutissant à
un plantage de QEMU ou à une exécution potentielle de code avec les privilèges
du processus QEMU dans l’hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3527">CVE-2021-3527</a>

<p>Un défaut a été découvert dans le périphérique redirecteur USB (usb-redir)
de QEMU. Des petits paquets USB sont combinés en une seule et grande requête
de transfert pour réduire la surcharge et améliorer les performances. La taille
combinée du transfert en bloc est utilisée pour allouer dynamiquement un
tableau de longueur variable (VLA) dans la pile sans validation appropriée.
Puisque la taille totale n’est pas limitée, un client malveillant pourrait
utiliser ce défaut pour affecter la longueur du tableau et faire que le
processus QEMU réalise une allocation excessive dans la pile, aboutissant à un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3594">CVE-2021-3594</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseau SLiRP par QEMU. Ce défaut existe dans la fonction
udp_input() et pourrait apparaitre lors du traitement d’un paquet UDP qui est
plus petit que la taille de la structure <q>udphdr</q>. Ce défaut pourrait
conduire à un accès en lecture hors limites ou à la divulgation indirecte de la
mémoire de l’hôte au client. Le plus grand risque de cette vulnérabilité
concerne la confidentialité des données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3592">CVE-2021-3592</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseau SLiRP par QEMU. Ce défaut existe dans la fonction
bootp_input() et pourrait apparaitre lors du traitement d’un paquet UDP qui est
plus petit que la taille de la structure <q>bootp_t</q>. Un client malveillant
pourrait utiliser ce défaut pour divulguer 10 octets de la mémoire de tas non
initialisés de l’hôte. Le plus grand risque de cette vulnérabilité
concerne la confidentialité des données.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3595">CVE-2021-3595</a>

<p>Un problème d’initialisation non valable de pointeur a été découvert dans
l’implémentation de réseau SLiRP par QEMU. Ce défaut existe dans la fonction
tftp_input() et pourrait apparaitre lors du traitement d’un paquet UDP qui est
plus petit que la taille de la structure <q>tftp_t</q>. Ce défaut pourrait
conduire à un accès en lecture hors limites ou à la divulgation indirecte de la
mémoire de l’hôte au client. Le plus grand risque de cette vulnérabilité
concerne la confidentialité des données.</p></li>
</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 1:2.8+dfsg-6+deb9u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets qemu.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de qemu,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/qemu">\
https://security-tracker.debian.org/tracker/qemu</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2753.data"
# $Id: $
