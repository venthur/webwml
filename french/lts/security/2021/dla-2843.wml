#use wml::debian::translation-check translation="be2ed4058252467df960ca5671e98db88288fb90" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
 <p><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3655">CVE-2021-3655</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a> 
 <a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a> 
 <a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-22543">CVE-2021-22543</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-37159">CVE-2021-37159</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a> 
 <a href="https://security-tracker.debian.org/tracker/CVE-2021-38198">CVE-2021-38198</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38204">CVE-2021-38204</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-38205">CVE-2021-38205</a> 
 <a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-42008">CVE-2021-42008</a> <a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a> 
 <a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a></p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou à
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-3702">CVE-2020-3702</a>

<p>Un défaut a été découvert dans le pilote pour la famille de puces
Atheros IEEE 802.11n (ath9k) permettant une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16119">CVE-2020-16119</a>

<p>Hadar Manor a signalé une utilisation de mémoire après libération dans
l'implémentation du protocole DCCP dans le noyau Linux. Un attaquant local
peut tirer avantage de ce défaut pour provoquer un déni de service ou
éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-0920">CVE-2021-0920</a>

<p>Une situation de compétition a été découverte dans le sous-système local
de sockets (AF_UNIX) qui pourrait conduire à une utilisation de mémoire
après libération. Un utilisateur local pourrait exploiter cela pour un déni
de service (corruption de mémoire ou plantage), ou éventuellement pour une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3612">CVE-2021-3612</a>

<p>Murray McAllister a signalé un défaut dans le sous-système d'entrée de
manette de jeu. Un utilisateur local autorisé à accéder à un périphérique
de manette de jeu pourrait exploiter cela pour une lecture et écriture hors
limites dans le noyau, ce qui pourrait être utilisé pour une élévation de
privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3653">CVE-2021-3653</a>

<p>Maxim Levitsky a découvert une vulnérabilité dans l'implémentation de
l'hyperviseur KVM pour les processeurs AMD dans le noyau Linux : l'absence
de validation du champ « int_ctl » de VMCB pourrait permettre à un client
L1 malveillant d'activer la prise en charge d'AVIC (Advanced Virtual
Interrupt Controller) pour le client L2. Le client L2 peut tirer avantage
de ce défaut pour écrire dans un sous-ensemble limité mais néanmoins
relativement grand de la mémoire physique de l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3655">CVE-2021-3655</a>

<p>Ilja Van Sprundel et Marcelo Ricardo Leitner ont découvert plusieurs
défauts dans l'implémentation de SCTP où l'absence de validation pourrait
conduire à une lecture hors limites. Sur un système utilisant SCTP, un
attaquant réseau pourrait les exploiter pour provoquer un déni de service
(plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3679">CVE-2021-3679</a>

<p>Un défaut dans la fonctionnalité du module de traçage du noyau Linux
pourrait permettre à un utilisateur local privilégié (doté de la capacité
CAP_SYS_ADMIN) de provoquer un déni de service (saturation des ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3732">CVE-2021-3732</a>

<p>Alois Wohlschlager a signalé un défaut dans l'implémentation du
sous-système overlayfs, permettant à un attaquant local, doté des
privilèges pour monter un système de fichiers, de révéler des fichiers
cachés dans le montage d'origine.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3753">CVE-2021-3753</a>

<p>Minh Yuan a signalé une situation de compétition dans vt_k_ioctl dans
rivers/tty/vt/vt_ioctl.c, qui peut provoquer une lecture hors limites dans
le terminal virtuel.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3760">CVE-2021-3760</a>

<p>Lin Horse a signalé un défaut dans le pilote NCI (NFC Controller
Interface) qui pourrait conduire à une utilisation de mémoire après
libération.</p>

<p>Néanmoins, ce pilote n'est pas inclus dans les paquets binaires fournis
par Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20317">CVE-2021-20317</a>

<p>La structure de la file d'attente du minuteur pourrait être corrompue
menant les tâches en attente à n'être jamais réveillées. Un utilisateur
local avec certains privilèges pourrait exploiter cela pour provoquer un
déni de service (blocage système).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20321">CVE-2021-20321</a>

<p>Une situation de compétition a été découverte dans le pilote du système
de fichiers overlayfs. Un utilisateur local doté d'un accès à un montage
overlayfs et à son répertoire racine sous-jacent pourrait exploiter cela
pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20322">CVE-2021-20322</a>

<p>Une fuite d'informations a été découverte dans l'implémentation d'IPv4.
Un attaquant distant pourrait exploiter cela pour découvrir rapidement quels
ports UDP sont utilisés par un système, facilitant son attaque par
empoisonnement de DNS à l'encontre de ce système.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-22543">CVE-2021-22543</a>

<p>David Stevens a découvert un défaut dans la manière dont l'hyperviseur
KVM met en correspondance la mémoire de l'hôte dans un client. Un
utilisateur local autorisé à accéder à /dev/kvm pourrait utiliser cela pour
provoquer la libération de certaines pages alors qu'elles ne devraient pas
l'être, menant à une utilisation de mémoire après libération. Cela pourrait
être utilisé pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-37159">CVE-2021-37159</a>

<p>Un défaut a été découvert dans le pilote hso pour les modems mobiles
haut débit Option. Une erreur pendant l'initialisation pourrait conduire à
une double libération de zone de mémoire ou à une utilisation de mémoire
après libération. Un attaquant capable de se connecter à des périphériques
USB pourrait utiliser cela pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement pour exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38160">CVE-2021-38160</a>

<p>Un défaut a été découvert dans virtio_console permettant la corruption
ou la perte de données par un périphérique non fiable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38198">CVE-2021-38198</a>

<p>Un défaut a été découvert dans l'implémentation de KVM pour les
processeurs x86, ce qui pourrait avoir pour conséquence que la protection
de la mémoire virtuelle dans un client n'est pas correctement appliquée.
Lorsque des tables de page « shadow » sont utilisées, par exemple pour la
virtualisation imbriquée ou sur les processeurs qui ne disposent pas des
fonctions EPT ou NPT, un utilisateur du système exploitation client
pourrait exploiter cela pour un déni de service ou une élévation de
privilèges dans le client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38199">CVE-2021-38199</a>

<p>Michael Wakabayashi a signalé un défaut dans l'implémentation du client
NFSv4, où l'ordre incorrect des réglages de connexion permet aux opérations
d'un serveur NFSv4 distant de provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38204">CVE-2021-38204</a>

<p>Un défaut a été découvert dans le pilote de contrôleur USB hôte
max4321-hcd qui pourrait conduire à une utilisation de mémoire après
libération.</p>

<p>Néanmoins, ce pilote n'est pas inclus dans les paquets binaires fournis
par Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38205">CVE-2021-38205</a>

<p>Une fuite d'informations a été découverte dans le pilote réseau
xilinx_emaclite. Sur un noyau personnalisé où ce pilote est activé et
utilisé, cela pourrait faciliter l'exploitation d'autres bogues du noyau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40490">CVE-2021-40490</a>

<p>Une situation de compétition a été découverte dans le sous-système ext4
lors de l'écriture dans un fichier inline_data alors que ses attributs
étendus sont modifiés. Cela pourrait avoir pour conséquence un déni de
service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-41864">CVE-2021-41864</a>

<p>Un dépassement d'entier a été découvert dans le sous-système Extended
BPF (eBPF). Un utilisateur local pourrait exploiter cela pour un déni de
service (corruption de mémoire ou plantage) ou éventuellement pour une
élévation de privilèges.</p>

<p>Cela peut être atténué avec le réglage de sysctl
kernel.unprivileged_bpf_disabled=1 qui désactive l'utilisation d'eBPF par
les utilisateurs non privilégiés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42008">CVE-2021-42008</a>

<p>Un dépassement de tampon de tas a été découvert dans le pilote réseau
par port série 6pack. Un utilisateur local doté de la capacité
CAP_NET_ADMIN pourrait exploiter cela pour un déni de service (corruption
de mémoire ou plantage) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42739">CVE-2021-42739</a>

<p>Un dépassement de tampon de tas a été découvert dans le pilote firedtv
pour les receveurs DVB connectés par FireWire. Un utilisateur local ayant
l'accès à un périphérique firedtv pourrait exploiter cela pour un déni de
service (corruption de mémoire ou plantage) ou éventuellement pour une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43389">CVE-2021-43389</a>

<p>Le laboratoire Active Defense de Venustech a découvert un défaut dans le
sous-système CMTP tel qu'utilisé par Bluetooth, qui pourrait conduire à une
lecture hors limites et à une confusion de type d'objet. Un utilisateur
local doté de la capacité CAP_NET_ADMIN dans l'espace de noms utilisateur
initial pourrait exploiter cela pour un déni de service (corruption de
mémoire ou plantage) ou éventuellement pour une élévation de privilèges.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.9.290-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2843.data"
# $Id: $
