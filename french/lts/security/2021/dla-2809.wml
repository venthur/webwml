#use wml::debian::translation-check translation="03f245cb340128c47cebab3961bf32f5cf1d81af" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Stefan Walter a découvert qu'udisks2, un service pour accéder et
manipuler les périphériques de stockage, pourrait avoir pour conséquence un
déni de service au moyen d'un plantage du système si un périphérique ou une
image ext2/3/4 corrompus ou contrefaits pour l'occasion ont été montés, ce
qui pourrait se produire automatiquement dans certains environnements.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2.1.8-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets udisks2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de udisks2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/udisks2">\
https://security-tracker.debian.org/tracker/udisks2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2809.data"
# $Id: $
