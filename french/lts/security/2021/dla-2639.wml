#use wml::debian::translation-check translation="18337ab4fd5e82fa223e9d37e535cc311edacb58" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert qu’OpenDMARC, une implémentation de milter de DMARC,
avait un octet NULL final incorrect dans la fonction opendmarc_xml_parse qui
pouvait conduire à un dépassement de tas d’un octet dans opendmarc_xml lors de
l’analyse d’un rapport global DMARC contrefait pour l'occasion. Cela pouvait
provoquer une corruption de mémoire distante lorsqu’un octet <q>\0</q>
surchargeait les métadonnées de tas dans le fragment suivant et son indicateur
PREV_INUSE.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.3.2-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets opendmarc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de opendmarc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/opendmarc">\
https://security-tracker.debian.org/tracker/opendmarc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2639.data"
# $Id: $
