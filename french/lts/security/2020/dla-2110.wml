#use wml::debian::translation-check translation="99fa24791b9f11082d2e8a6ebcb0b3839793849e" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le serveur HTTP fourni par
Netty, un environnement de développement de sockets client/serveur NIO en Java.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-0193">CVE-2014-0193</a>

<p>WebSocket08FrameDecoder permet à des attaquants distants de provoquer un déni
de service (consommation de mémoire) à l'aide d'un TextWebSocketFrame suivi par
un long flux de ContinuationWebSocketFrames.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-3488">CVE-2014-3488</a>

<p>Le SslHandler permet à des attaquants distants de provoquer un déni de
service (boucle infinie et consommation de CPU) à l'aide d'un message SSLv2Hello
contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16869">CVE-2019-16869</a>

<p>Netty gère incorrectement un espace blanc avant un deux-points dans un
en-tête HTTP (telle une ligne « Transfer-Encoding : chunked »), conduisant à une
dissimulation de requête HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20444">CVE-2019-20444</a>

<p>HttpObjectDecoder.java permet à un en-tête HTTP ayant un deux-points en moins
d’être interprété comme un en-tête distinct avec une syntaxe incorrecte ou
d’être interprété comme un « fold » non valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20445">CVE-2019-20445</a>

<p>HttpObjectDecoder.java permet à un en-tête Content-Length d’être accompagné
par un second en-tête Content-Length ou par un en-tête Transfer-Encoding.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7238">CVE-2020-7238</a>

<p>Netty permet la dissimulation de requête HTTP à cause d’une gestion incorrecte
d’espace blanc Transfer-Encoding (telle une ligne [espace]Transfer-Encoding:chunked)
et un en-tête Content-Length après.</p></li>
</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.9.0.Final-1+deb8u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets netty-3.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2110.data"
# $Id: $
