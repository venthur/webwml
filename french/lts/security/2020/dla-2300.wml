#use wml::debian::translation-check translation="6bfd2adf31e16c63a7ab518518b753cfa8ccef5a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème si kdepim-runtime utilisait
par défaut une communication POP3 non chiffrée en dépit de l’indication par
l’interface utilisateur que le chiffrement était en usage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15954">CVE-2020-15954</a>

<p>KMail 19.12.3 de KDE (c'est-à-dire 5.13.3) s’engageait dans une communication
POP3 non chiffrée alors que l’interface indiquait que le chiffrement était
utilisé.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4:16.04.2-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets kdepim-runtime.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2300.data"
# $Id: $
