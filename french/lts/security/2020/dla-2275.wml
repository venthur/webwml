#use wml::debian::translation-check translation="b204cbef7418890a63b80f60c54b333b34fe8503" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les CVE suivants ont été signalés à l’encontre de src:ruby-rack.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8161">CVE-2020-8161</a>

<p>Une vulnérabilité de traversée de répertoires existait dans rack avant la
version 2.2.0 permettant à un attaquant de réaliser une traversée de répertoires
dans l’application Rack::Directory qui est fournie avec Rack. Cela pouvait
aboutir à une divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8184">CVE-2020-8184</a>

<p>Une vulnérabilité de sécurité dépendant de cookies sans vérification de
validité ou d’intégrité existait dans rack avant la version 2.2.3 et rack avant
la version 2.1.4 qui rendait possible la contrefaçon d’un préfixe de cookie
sécurisé ou d’hôte unique (host-only).</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.6.4-4+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rack.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rack, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rack">https://security-tracker.debian.org/tracker/ruby-rack</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2275.data"
# $Id: $
