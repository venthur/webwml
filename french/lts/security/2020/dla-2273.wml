#use wml::debian::translation-check translation="cab1770f19f9bf589e8252d475ce8edaf6008ca8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait deux problèmes dans shiro, un cadriciel de
sécurité pour les applications Java :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1957">CVE-2020-1957</a>

<p>Dans Apache Shiro avant la version 1.5.2, lors de son utilisation avec des
contrôleurs dynamiques Spring, une requête contrefaite pour l'occasion pouvait
provoquer un contournement d’authentification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11989">CVE-2020-11989</a>

<p>Dans Apache Shiro avant la version 1.5.3, lors de son utilisation avec des
contrôleurs dynamiques Spring, une requête contrefaite pour l'occasion pouvait
provoquer un contournement d’authentification.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 1.3.2-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets shiro.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2273.data"
# $Id: $
