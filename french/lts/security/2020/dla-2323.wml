#use wml::debian::translation-check translation="444aecf0c594e32e64aea1472f5deef356007e37" maintainer="Jean-Paul Guillonneau"
<define-tag description>Nouveau paquets LTS</define-tag>
<define-tag moreinfo>
<p>Linux 4.19 a été empaqueté pour Debian 9 sous le nom de linux-4.19. Ce paquet fournit
une méthode de gestion de mise à niveau pour les systèmes qui utilisent
actuellement des paquets de noyau et de micrologiciel de la suite
« Stretch-backports ».</p>

<p>Il n’est nul besoin de mettre à niveau les systèmes utilisant Linux 4.9,
car cette version de noyau sera prise en charge pendant toute la durée de la
période LTS.</p>

<p>Ce rétroportage n’inclut pas les paquets binaires suivants :</p>

<blockquote><div>hyperv-daemons, libbpf-dev, libbpf4.19, libcpupower-dev, libcpupower1,
liblockdep-dev, liblockdep4.19, linux-compiler-gcc-6-arm,
linux-compiler-gcc-6-x86, linux-cpupower, linux-libc-dev, lockdep,
usbip.</div></blockquote>

<p>Les anciennes versions de la plupart de ceux-ci sont construites à partir du
paquet source linux dans Debian 9.</p>

<p>Les images et modules de noyau ne seront pas signés pour une utilisation sur
les systèmes avec le Secure Boot activé, car aucune prise en charge n’existe
pour cela dans Debian 9.</p>

<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à un déni de service ou une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18814">CVE-2019-18814</a>

<p>Navid Emamdoost a signalé une utilisation potentielle de mémoire après
libération dans le module de sécurité AppArmor dans le cas où l’initialisation
de cette règle d’audit échouerait. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18885">CVE-2019-18885</a>

<p>L’équipe <q>bobfuzzer</q> a découvert que des volumes Btrfs contrefaits
pourraient déclencher un plantage (oops). Un attaquant capable de monter un tel
volume pourrait utiliser cela pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20810">CVE-2019-20810</a>

<p>Une fuite de mémoire potentielle a été découverte dans le pilote de média
go7007. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10766">CVE-2020-10766</a>

<p>Anthony Steinhauser a signalé un défaut dans la mitigation pour « Speculative
Store Bypass »
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-3639">CVE-2018-3639</a>)
sur les CPU x86. Un utilisateur local pourrait utiliser cela pour désactiver
temporairement la mitigation SSB dans d’autres tâches d’utilisateur. Si ces
autres tâches exécutent du code confiné dans un bac à sable, cela permet à ce
code de lire des informations sensibles dans le même processus mais en dehors du
bac à sable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10767">CVE-2020-10767</a>

<p>Anthony Steinhauser a signalé un défaut dans la mitigation pour la seconde
variante de Spectre
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>)
sur les CPU x86. Selon les autres mitigations gérées par le CPU, le noyau peut ne
pas utiliser IBPB pour mitiger cette variante dans l’espace utilisateur. Un
utilisateur local pourrait utiliser cela pour lire des informations sensibles
d’autres processus utilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10768">CVE-2020-10768</a>

<p>Anthony Steinhauser a signalé un défaut dans la mitigation pour la seconde
variante de Spectre
(<a href="https://security-tracker.debian.org/tracker/CVE-2017-5715">CVE-2017-5715</a>)
sur les CPU x86. Après qu’une tâche ait forcé la désactivation de spéculation
indirecte de branche à travers prctl(), elle pourrait encore la réactiver plus
tard, aussi il n’était pas possible d’ignorer un programme qui l’activait de
manière explicite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12655">CVE-2020-12655</a>

<p>Zheng Bin a signalé que des volumes XFS contrefaits pourraient déclencher un
plantage du système. Un attaquant capable de monter de tels volumes pourrait
utiliser cela pour provoquer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12771">CVE-2020-12771</a>

<p>Zhiqiang Liu a signalé un bogue dans le pilote de bloc bcache qui pourrait
conduire à un plantage du système. L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13974">CVE-2020-13974</a>

<p>Kyungtae Kim a signalé un dépassement d'entier potentiel dans le pilote vt
(virtual terminal). L’impact de sécurité est incertain.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15393">CVE-2020-15393</a>

<p>Kyungtae Kim a signalé une fuite de mémoire dans le pilote usbtest.
L’impact de sécurité est incertain.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.19.132-1~deb9u1. De plus, cette mise à jour corrige les bogues
de Debian <a href="https://bugs.debian.org/958300">n° 958300</a>,
<a href="https://bugs.debian.org/960493">n° 960493</a>,
<a href="https://bugs.debian.org/962254">n° 962254</a>,
<a href="https://bugs.debian.org/963493">n° 963493</a>,
<a href="https://bugs.debian.org/964153">n° 964153</a>,
<a href="https://bugs.debian.org/964480">n° 964480</a>,
<a href="https://bugs.debian.org/965365">n° 965365</a> et inclut beaucoup
d’autres corrections de bogue des mises à jour de stable, versions 4.19.119
à 4.19.132.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux-4.19.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux-4.19, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux-4.19">https://security-tracker.debian.org/tracker/linux-4.19</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2323.data"
# $Id: $
