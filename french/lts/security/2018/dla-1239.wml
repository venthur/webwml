#use wml::debian::translation-check translation="433a52f0981ad6e4aaae1b5faf25d2aa3337d088" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>La fonction ZipCommon::isValidPath() dans Zip/src/ZipCommon.cpp de
l'ensemble de bibliothèques C++ POCO, versions antérieures à 1.8, ne
restreint pas correctement la valeur de nom de fichier dans l'en-tête de
ZIP. Cela permet à des attaquants de conduire des attaques de traversée de
chemin absolu durant la décompression de ZIP, et éventuellement de créer ou
d'écraser des fichiers arbitraires, à l'aide d'un fichier contrefait ZIP,
lié à une <q>vulnérabilité d'injection de chemin de fichier</q>.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 1.3.6p1-4+deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poco.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1239.data"
# $Id: $
