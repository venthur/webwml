#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Divers problèmes de sécurité ont été découverts dans poppler, la bibliothèque
partagée de rendu de PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18267">CVE-2017-18267</a>

<p>La fonction FoFiType1C::cvtGlyph dans fofi/FoFiType1C.cc dans Poppler
jusqu’à 0.64.0 permet à des attaquants distants de provoquer un déni de service
(récursion infinie) à l'aide d'un fichier PDF contrefait, comme démontré par
pdftops.</p>

<p>Le correctif appliqué dans FoFiType1C::cvtGlyph empêche une récursion infinie
dans de tels documents malformés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10768">CVE-2018-10768</a>

<p>Un déréférencement de pointeur NULL dans la fonction
AnnotPath::getCoordsLength dans Annot.h dans Poppler 0.24.5 a été découvert. Une
entrée contrefaite pourrait conduire à attaque par déni de service distante. Les
versions suivantes de Poppler telles que 0.41.0 ne sont pas affectées.</p>

<p>Les rustines appliquées corrigent le plantage de AnnotInk::draw pour des
documents malformés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-13988">CVE-2018-13988</a>

<p>Poppler jusqu’à 0.62 contient une vulnérabilité de lecture hors limites
due à un accès incorrect à la mémoire qui n’est pas mappé à son espace mémoire,
comme démonstré par pdfunite. Cela pourrait aboutir à une corruption de mémoire
et à un déni de service. Cela peut être exploitable quand une victime ouvre un
fichier PDF spécialement contrefait.</p>

<p>La rustine appliquée corrige le plantage quand un Object possède un nombre
négatif (les spécifications disent que le nombre doit être > 0 et gen >= 0).</p>

<p>Pour Poppler dans Debian Jessie, le correctif original de l’amont a été
rétroporté vers l’ancienne API Object de Poppler.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16646">CVE-2018-16646</a>

<p>Dans Poppler 0.68.0, la fonction Parser::getObj() dans Parser.cc peut causer
une récursion infinie à l'aide d'un fichier contrefait. Un attaquant distant
peut exploiter cela pour une attaque par déni de service.</p>

<p>Une série de correctifs de l’amont a été appliquée à XRef.cc de Poppler dans
Debian Jessie pour parfaire la correction de ce problème.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la version 0.26.5-2+deb8u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets poppler.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1562.data"
# $Id: $
