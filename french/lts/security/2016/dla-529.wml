#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité de déni de service a été identifiée dans Commons
FileUpload qui se produisait quand la longueur des limites d'entité
fragmentée était juste inférieure à la taille du tampon (4096 octets)
utilisé pour lire le fichier envoyé. Cela faisait que le processus d'envoi
de fichier acceptait plusieurs commandes de taille plus grande que si les
limites avaient la longueur habituelle de dizaines d'octets de long.</p>

<p>Apache Tomcat utilise une copie renommée du paquet Apache Commons
FileUpload pour implémenter les besoins d'envoi de fichiers de la
spécification de la Servlet, et il est donc aussi sensible à une
vulnérabilité de déni de service.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 7.0.28-4+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-529.data"
# $Id: $
