#use wml::debian::translation-check translation="adc5cbd36ecf754028e80bbdee567a58bca03b81" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été signalé que Phpmyadmin, un outil d'administration web pour
MySQL, comportait plusieurs vulnérabilités de script intersite (XSS).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5731">CVE-2016-5731</a>

<p>Il est possible, avec une requête contrefaite pour l'occasion de
déclencher une attaque de script intersite (XSS) à travers le script
d'authentification OpenID.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5733">CVE-2016-5733</a>

<p>Plusieurs vulnérabilités de script intersite ont été découvertes dans la
fonction Transformation. Une vulnérabilité a aussi été signalée permettant
à un serveur MySQL configuré de façon particulière d'exécuter une attaque
de script intersite. Cette attaque particulière nécessite de configurer la
directive log_bin du serveur MySQL avec la charge utile.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5739">CVE-2016-5739</a>

<p>Une vulnérabilité a été signalée dans laquelle une Transformation
contrefaite pour l'occasion pourrait être utilisée pour divulguer des
informations y compris le jeton d'authentification. Cela pourrait être
utilisé pour diriger une attaque de contrefaçon de requête intersite (CSRF)
à l'encontre d'un utilisateur.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 4:3.4.11.1-2+deb7u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets phpmyadmin.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-551.data"
# $Id: $
