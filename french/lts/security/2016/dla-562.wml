#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>GOsa² est le regroupement d'une interface web commune d’administration
système et d’utilisateur final pour gérer les configurations basées sur
LDAP.</p>

<p>Une vulnérabilité d'injection de code dans le code du greffon Samba de
GOsa a été découverte. Durant la modification de mots de passe de Samba, il
était possible d'injecter du code Perl malveillant.</p>

<p>Si vous mettez à niveau vers cette révision corrigée du paquet, veuillez
noter que la modification des mots de passe de Samba ne fonctionnera plus
jusqu'à ce que le paramètre sambaHashHook dans gosa.conf ait été mis à jour
pour accepter des chaînes de mot de passe codées en base 64.</p>

<p>Veuillez lire la page de manuel gosa.conf(5) après la mise à niveau vers
cette révision du paquet et adaptez gosa.conf comme cela est décrit.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 2.7.4-4.3~deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gosa.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-562.data"
# $Id: $
