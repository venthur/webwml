#use wml::debian::translation-check translation="6de0e532bc23ed329ae22b373037e94a734c5d83" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14821">CVE-2019-14821</a>

<p>Matt Delco a signalé une situation de compétition dans la fonction Coalesced
MMIO de KVM qui pourrait conduire à un accès hors limites dans le noyau. Un
attaquant local autorisé à accéder à /dev/kvm pourrait utiliser cela pour
provoquer un déni de service (corruption de mémoire ou plantage) ou
éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14835">CVE-2019-14835</a>

<p>Peter Pi de Tencent Blade Team a découvert une absence de vérification de
limites dans vhost_net, le pilote de dorsal de réseau pour les hôtes KVM,
menant à un dépassement de tampon quand l'hôte débute la migration en direct
d’une machine virtuelle. Un attaquant qui contrôle une machine virtuelle
pourrait utiliser cela pour provoquer un déni de service (corruption de mémoire
ou plantage) ou éventuellement pour une élévation de privilèges sur l'hôte.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15117">CVE-2019-15117</a>

<p>Hui Peng et Mathias Payer ont signalé une absence de vérification
de limites dans dans le code d'analyse du descripteur du pilote usb-audio,
menant à une lecture hors limites de tampon. Un attaquant capable d'ajouter
des périphériques USB pourrait éventuellement utiliser cela pour provoquer
un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15118">CVE-2019-15118</a>

<p>Hui Peng et Mathias Payer ont signalé une récursion illimitée dans le
code d'analyse du descripteur du pilote usb-audio, menant à un dépassement
de pile. Un attaquant capable d'ajouter des périphériques USB pourrait
utiliser cela pour provoquer un déni de service (corruption de mémoire ou
plantage) ou éventuellement une élévation de privilèges. Sur l'architecture
amd64 ainsi que sur l'architecture arm64 dans Buster, cela est atténué par
une page de protection sur la pile du noyau, ainsi, il est uniquement
possible de provoquer un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15902">CVE-2019-15902</a>

<p>Brad Spengler a signalé qu'une erreur de rétroportage réintroduisait une
vulnérabilité « Spectre v1 » dans la fonction ptrace_get_debugreg() du
sous-système ptrace.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.9.189-3+deb9u1~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets linux-4.9.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1940.data"
# $Id: $
