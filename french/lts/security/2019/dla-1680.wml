#use wml::debian::translation-check translation="244380ed6414a14f265795cff6ac8dab1d04e3a3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Brève introduction</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17000">CVE-2018-17000</a>

<p>Un déréférencement de pointeur NULL dans la fonction _TIFFmemcmp dans tif_unix.c
(appelée à partir de TIFFWriteDirectoryTagTransferfonction) permet à un
attaquant de provoquer un déni de service à l’aide d’un fichier tiff contrefait.
Cette vulnérabilité peut être déclenchée par l’exécutable tiffcp.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19210">CVE-2018-19210</a>

<p>Il existait un déréférencement de pointeur NULL dans la fonction
TIFFWriteDirectorySec dans tif_dirwrite.c pouvant conduire à une attaque par
déni de service, comme démontré par tiffset.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-7663">CVE-2019-7663</a>

<p>Un déréférencement d’adresse non valable a été découvert dans
TIFFWriteDirectoryTagTransferfonction dans libtiff/tif_dirwrite.c,
affectant la fonction cpSeparateBufToContigBuf dans tiffcp.c. Des attaquants
distants pourraient exploiter cette vulnérabilité pour provoquer un déni de service
à l'aide d'un fichier tiff contrefait.</p>

<p>Nous pensons que cela est la même chose que
<a href="https://security-tracker.debian.org/tracker/CVE-2018-17000">CVE-2018-17000</a> (ci-dessus).</p>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans la
version 4.0.3-12.3+deb8u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1680.data"
# $Id: $
