#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un attaquant qui est enregistré dans OTRS, un système de requête d’assistance,
comme un agent avec permission d’écriture pour les statistiques peut injecter
du code arbitraire dans le système. Cela peut conduire à des problèmes sérieux
tels qu’une élévation des privilèges, une perte de données ou un déni de
service. Ce problème est aussi répertorié dans
<a href="https://security-tracker.debian.org/tracker/CVE-2017-14635">CVE-2017-14635</a>
et est résolu en mettant à niveau vers la dernière publication de l’amont
d’OTRS3.</p>

<p><strong>REMARQUES IMPORTANTES DE MISE À NIVEAU</strong></p>

<p>Cette mise à jour nécessite une intervention manuelle. Nous vous recommandons
de sauvegarder tous vos fichiers et base de données avant la mise à niveau. Si vous
utilisez MySQL comme dorsal, vous devriez lire le rapport de bogue n° 707075 et
le fichier README.Debian inclus fournissant des informations supplémentaires.</p>

<p>Si vous découvrez que le mode maintenance est toujours activé après la mise
à jour, nous recommandons de supprimer /etc/otrs/maintenance.html et
/var/lib/otrs/httpd/htdocs/maintenance.html ce qui résoudra ce problème.</p>

<p>De plus, les vulnérabilités suivantes ont été aussi corrigées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-1695">CVE-2014-1695</a>

<p>Vulnérabilité de script intersite (XSS) dans OTRS permettant à des attaquants
distants d’injecter un script web arbitraire ou de l’HTML à l'aide d'un
courriel HTML contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2553">CVE-2014-2553</a>

<p>Vulnérabilité de script intersite (XSS) dans OTRS permettant à un utilisateur
distant authentifié d’injecter un script web arbitraire ou de l’HTML à l’aide de
vecteurs relatifs aux champs dynamiques.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2014-2554">CVE-2014-2554</a>

<p>OTRS permet à des attaquants distants de mener des attaques de détournement
de clic à l'aide d'un élément IFRAME.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 3.3.18-1~deb7u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1119.data"
# $Id: $
