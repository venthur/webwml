#use wml::debian::translation-check translation="7436af5280d67e5eaa0947427aa1dd537c1cdd07" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Un dépassement d'entier (avec un dépassement de tampon de tas
consécutif) a été découvert dans ndb, le serveur de protocole « Network
Block Device ». Une valeur de 0xffffffff dans le champ de longueur du nom
pouvait provoquer l'allocation d'un tampon de taille nulle pour le nom avec
pour conséquence une écriture vers un pointeur bancal.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26495">CVE-2022-26495</a>

<p>Dans nbd-server pour les versions de nbd antérieures à 3.24, il y a un
dépassement d'entier avec un dépassement de tampon de tas consécutif. Une
valeur de 0xffffffff dans le champ de longueur du nom provoque l'allocation
d'un tampon de taille nulle pour le nom avec pour conséquence une écriture
vers un pointeur bancal. Ce problème existe pour les messages NBD_OPT_INFO,
NBD_OPT_GO et NBD_OPT_EXPORT_NAME.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:3.15.2-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets nbd.</p>

<p>Merci à Wouter Verhelst pour son aide à la préparation de cette mise à
jour.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2944.data"
# $Id: $
