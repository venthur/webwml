#use wml::debian::translation-check translation="f50a0bc733e82a8ad2d1fa9d924d8487bcce7274" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que PgBouncer, un groupeur de connexions (pooler)
pour PostgreSQL, était vulnérable à une attaque par injection de code SQL
arbitraire si un homme du milieu pouvait injecter des données quand une
connexion utilisant une authentification de certificats est établie.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
1.7.2-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pgbouncer.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pgbouncer, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pgbouncer">\
https://security-tracker.debian.org/tracker/pgbouncer</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2922.data"
# $Id: $
