#use wml::debian::translation-check translation="9949f12dd9ce9f2e20aed9274fe6356021e625b1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans OpenVPN, un serveur et
client de réseau privé virtuel (VPN) qui pouvaient conduire à un
contournement d'authentification lors de l'utilisation de greffons
d'authentification différée.</p>

<p>Notez que cette version désactive la prise en charge de plusieurs
greffons d'authentification différée, suivant la correction amont pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-0547">CVE-2022-0547</a>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.4.0-6+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets openvpn.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de openvpn, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/openvpn">\
https://security-tracker.debian.org/tracker/openvpn</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2992.data"
# $Id: $
