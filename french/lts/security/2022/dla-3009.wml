#use wml::debian::translation-check translation="565564cb31279948eb9233941d3ff8657df1f8ba" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les deux vulnérabilités suivantes ont été découvertes dans
src:cifs-utils, des utilitaires du système de fichier CIFS (Common Internet
File System) :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27239">CVE-2022-27239</a>

<p>Dans cifs-utils, un dépassement de pile lors de l'analyse de l'argument
en ligne de commande <q>ip=</q> de mount.cifs pouvait permettre à des
attaquants locaux d'obtenir les privilèges du superutilisateur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29869">CVE-2022-29869</a>

<p>cifs-utils, avec une journalisation détaillée, peut provoquer une fuite
d'informations quand un fichier contient des caractères = (signe égal),
mais n'est pas un fichier d'identifiants valable.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2:6.7-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets cifs-utils.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de cifs-utils, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/cifs-utils">\
https://security-tracker.debian.org/tracker/cifs-utils</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3009.data"
# $Id: $
