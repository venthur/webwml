#use wml::debian::translation-check translation="f40d2bc9bb6160275a07a82faddd0ec659d1395d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de sécurité a été découverte dans Ghostscript,
l'interpréteur GPL de PostScript et de PDF. Certains opérateurs Postscript
privilégiés demeuraient accessibles à partir de divers emplacements. Par
exemple, un fichier PostScript contrefait pour l'occasion pouvait utiliser
ce défaut afin d'accéder au système de fichiers en dehors des contraintes
imposées par -dSAFER.</p>

<p>Ce problème existe à cause d'une correction incomplète pour la
<a href="https://security-tracker.debian.org/tracker/CVE-2019-3839">CVE-2019-3839</a>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
9.26a~dfsg-0+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ghostscript.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ghostscript, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ghostscript">\
https://security-tracker.debian.org/tracker/ghostscript</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2989.data"
# $Id: $
