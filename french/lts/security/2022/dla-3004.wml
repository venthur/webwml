#use wml::debian::translation-check translation="9b1bbed9d9d097a893206015c4441205b0266daf" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il y avait une vulnérabilité de dépassement d'entier dans htmldoc, un
processeur qui génère des fichiers HTML indexés, PS et PDF. Cela était
causé par une erreur de programmation dans la fonction image_load_jpeg
due à un mélange ou une confusion entre les dimensions d'image déclarées,
attendues et observées.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-27114">CVE-2022-27114</a>

<p>Il y a une vulnérabilité dans htmldoc 1.9.16. Dans la fonction
image_load_jpeg image.cxx, quand il appelle malloc, « img-&gt;width » et
« img-&gt;height » sont suffisamment grands pour provoquer un dépassement
d'entier. Aussi, la fonction malloc peut renvoyer un bloc de tas plus petit
que la taille attendue, et elle provoquera un dépassement de tampon ou une
erreur de limites d'adresse dans la fonction jpeg_read_scanlines.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.8.27-8+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets htmldoc.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3004.data"
# $Id: $
