#use wml::debian::translation-check translation="4bc8c6886b51e51da0ff36819bc9817fbbb78c43" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans rsyslog, démon de
journalisation système et noyau. Quand un serveur de journalisation est
configuré pour accepter des journaux à partir de clients distants à travers
des modules particuliers tels que <q>imptcp</q>, un attaquant peut
provoquer un déni de service (DoS) et éventuellement exécuter du code sur
le serveur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16881">CVE-2018-16881</a>

<p>Une vulnérabilité de déni de service a été découverte dans le module
imptcp de rsyslog. Un attaquant pouvait envoyer un message contrefait pour
l'occasion au socket d'imptcp, ce qui pouvait provoquer le plantage de
rsyslog.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24903">CVE-2022-24903</a>

<p>Les modules pour la réception de syslog par TCP ont un potentiel
dépassement de tampon de tas quand le traitement de trames à comptage
d'octets est utilisé. Cela peut avoir pour conséquences une erreur de
segmentation ou un autre dysfonctionnement.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 8.24.0-1+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rsyslog.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rsyslog, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rsyslog">\
https://security-tracker.debian.org/tracker/rsyslog</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3016.data"
# $Id: $
