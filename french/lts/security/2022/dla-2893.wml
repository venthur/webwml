#use wml::debian::translation-check translation="6b48d772918892e1e5639e5f5bd1b1706e320685" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Pillow, une
bibliothèque d'imagerie Python, qui pourraient avoir pour conséquences un
déni de service et éventuellement l'exécution de code arbitraire lors du
traitement d'images mal formées.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 4.0.0-4+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets pillow.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de pillow, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/pillow">\
https://security-tracker.debian.org/tracker/pillow</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2893.data"
# $Id: $
