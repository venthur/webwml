#use wml::debian::translation-check translation="0a4d682a00c2a503c66d32921f111ee2930b1f3d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans htmldoc, un processeur qui
génère des fichiers HTML indexés, PS et PDF.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0534">CVE-2022-0534</a>

<p>Un fichier GIF contrefait pouvait conduire à une lecture de pile hors
limites, qui pouvait avoir pour conséquence un plantage (erreur de
segmentation).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43579">CVE-2021-43579</a>

<p>La conversion d'un document HTML, qui est lié à un fichier BMP
contrefait, pouvait conduire à un dépassement de pile, ce qui pouvait
avoir pour conséquence l'exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-40985">CVE-2021-40985</a>

<p>Une image BMP contrefaite pouvait conduire à un dépassement de tampon,
qui pouvait provoquer un déni de service.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1.8.27-8+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets htmldoc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de htmldoc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/htmldoc">\
https://security-tracker.debian.org/tracker/htmldoc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2928.data"
# $Id: $
